import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as path from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestContextModule } from '@medibloc/nestjs-request-context';

import { PrimaryModule } from './settings/primary/primary.module';
import { PersonalsModule } from './personals/personals.module';
import { AccountModule } from './microservices/account/account.module';
import { RegionModule } from './microservices/region/region.module';
import { MediaModule } from './microservices/media/media.module';
import { TeamModule } from './microservices/team/team.module';
import { StadiumModule } from './microservices/stadium/stadium.module';
import { PlayerHistoryModule } from './microservices/player-history/player-history.module';
import { TeamSocialContactModule } from './microservices/team-social-contact/team-social-contact.module';
import { StaffHistoryModule } from './microservices/staff-history/staff-history.module';
import { UniformModule } from './microservices/uniform/uniform.module';
import { AccountTeamHistoryModule } from './microservices/account-team-history/account-team-history.module';
import { StadiumSocialContactModule } from './microservices/stadium-social-contact/stadium-social-contact.module';
import { TeamMediaModule } from './microservices/team-media/team-media.module';
import { AssociateModule } from './microservices/associate/associate.module';
import { AssociateSocialContactModule } from './microservices/associate-social-contact/associate-social-contact.module';
import { CompetitionsModule } from './microservices/competitions/competitions.module';
import { AuthModule } from './auth/auth.module';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import { LoggingInterceptor } from './filters/logging.interceptor';
import { ApiRequestContext } from './shared/api-request-context';
import { UserPanelsModule } from './microservices/user-panels/user-panels.module';
import { DashboardModule } from './microservices/dashboard/dashboard.module';
import { TeamVersusMatchModule } from './microservices/team-versus-match/team-versus-match.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const baseDir = __dirname;
        const entitiesPath = path.join(baseDir, '**/**.entity{.ts,.js}');
        const migrationPath = path.join(baseDir, '/../db/seed/*.{.ts,.js}');
        return {
          type: 'postgres',
          host: configService.get('DATABASE_HOST', 'localhost'),
          port: configService.get<number>('DATABASE_PORT', 5432),
          username: configService.get('DATABASE_USERNAME', 'postgres'),
          password: configService.get('DATABASE_PASSWORD', 'postgres'),
          database: configService.get<string>('DATABASE_SCHEMA', 'local'),
          synchronize: configService.get<boolean>(
            'DATABASE_SYNCHRONIZE',
            false,
          ),
          ssl:
            configService.get('DATABASE_HOST') === 'localhost'
              ? false
              : {
                  rejectUnauthorized: false,
                },
          entities: [entitiesPath],
          migrations: [migrationPath],
        };
      },
    }),
    RequestContextModule.forRoot({
      contextClass: ApiRequestContext,
      isGlobal: true,
    }),
    UserPanelsModule,
    PrimaryModule,
    PersonalsModule,
    AccountModule,
    RegionModule,
    MediaModule,
    TeamModule,
    StadiumModule,
    PlayerHistoryModule,
    TeamSocialContactModule,
    StaffHistoryModule,
    UniformModule,
    AccountTeamHistoryModule,
    StadiumSocialContactModule,
    TeamMediaModule,
    AssociateModule,
    AssociateSocialContactModule,
    CompetitionsModule,
    AuthModule,
    DashboardModule,
    TeamVersusMatchModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
  ],
  exports: [],
})
export class AppModule {}
