import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { EPermissionCodes } from '../shared/enum';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: `${process.env.JWT_SECRET_KEY}`,
    });
  }

  async validate(payload: any) {
    if(
        [
          EPermissionCodes.B_ADMIN, 
          EPermissionCodes.B_ATHLETE, 
          EPermissionCodes.B_REPORTER, 
          EPermissionCodes.B_STAFF, 
          EPermissionCodes.B_SUPER_ADMIN, 
          EPermissionCodes.B_TEAM_ADMIN, 
          EPermissionCodes.B_VIEWER,
          EPermissionCodes.B_MEMBER,
          EPermissionCodes.B_OPERATION
        ].includes(payload.account_role.permission_code)
      ) 
    {
      return payload;
    }else{
      this.throw();
    }
  }

  throw() {
    throw new UnauthorizedException();
  }
}
