import { RequestContext } from '@medibloc/nestjs-request-context';
import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ApiRequestContext } from 'src/shared/api-request-context';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    const message = (exception.getResponse() as any).message;

    const errorResp = {
      statusCode: status,
      timestamp: new Date().toLocaleString(),
      path: request.url,
      method: request.method,
      message: message || exception.message || null,
      success: false,
    };

    Logger.error(
      `${request.method} ${request.url}`,
      JSON.stringify(errorResp),
      'ExceptionFilter',
    );

    const requestCtx: ApiRequestContext = RequestContext.get();

    requestCtx.addEventData(errorResp);

    requestCtx.submit();

    response.status(status).json(errorResp);
  }
}
