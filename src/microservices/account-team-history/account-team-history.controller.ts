import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SearchAccountTeamHistoryDto } from './account-team-history.dto';
import { AccountTeamHistoryService } from './account-team-history.service';

@ApiTags('Account Team History')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account')
export class AccountTeamHistoryController {
  constructor(
    private teamHistoryService: AccountTeamHistoryService
  ) {}

  @Get(':accountId/players/team-histories')
  async getPlayerTeamHistory(
    @Req() req: Request, 
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountTeamHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamHistoryService.getPlayerTeamHistories(accountId, query, token));
  }

  @Get(':accountId/staffs/team-histories')
  async getStaffTeamHistory(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: SearchAccountTeamHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamHistoryService.getStaffTeamHistories(accountId, query, token));
  }
}
