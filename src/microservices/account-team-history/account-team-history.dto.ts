import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional } from "class-validator";
import { SearchPaginateDto } from "../microservices.dto";
import { ISearchAccountTeamHistory } from "./account-team-history.interface";

export class SearchAccountTeamHistoryDto extends SearchPaginateDto implements ISearchAccountTeamHistory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  standard_types?: number;
}