import { ISearchPaginate } from "../microservices.interface";

export interface ISearchAccountTeamHistory extends ISearchPaginate {
  standard_types?: number;
}