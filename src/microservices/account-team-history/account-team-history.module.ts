import { forwardRef, Module } from '@nestjs/common';
import { AccountTeamHistoryService } from './account-team-history.service';
import { AccountTeamHistoryController } from './account-team-history.controller';
import { AccountPositionsModule } from 'src/personals/account-positions/account-positions.module';

@Module({
  imports: [
    forwardRef(() => AccountPositionsModule)
  ],
  providers: [AccountTeamHistoryService],
  controllers: [AccountTeamHistoryController],
  exports:[AccountTeamHistoryService]
})
export class AccountTeamHistoryModule {}
