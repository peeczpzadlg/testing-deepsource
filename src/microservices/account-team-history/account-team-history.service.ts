import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { AccountPositionsService } from 'src/personals/account-positions/account-positions.service';
import { EPersonnelPositionType } from 'src/shared/enum/position-type';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISearchAccountTeamHistory } from './account-team-history.interface';

@Injectable()
export class AccountTeamHistoryService {
  private microservice: AxiosInstance;

  constructor(private readonly positionService: AccountPositionsService) {
    this.microservice = microserviceAxios();
  }

  async getPlayerTeamHistories(
    accountId: number,
    params: ISearchAccountTeamHistory,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`account/${accountId}/players/teams-histories`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    ).then(async res => {
      try {
        const mapTeamPlayer = res.data?.player_team_history?.data?.map(
          async ele => {
            if (!ele.global_config_positions) return ele;
            const accountPosition = await this.positionService.findPositionType(
              EPersonnelPositionType.Player,
              ele.global_config_positions,
            );
            return {
              ...ele,
              accountPosition,
            };
          },
        );
        const data = await Promise.all(mapTeamPlayer);
        const result = {
          ...res,
          data: {
            player_team_history: {
              ...res.data.player_team_history,
              data,
            },
          },
        };
        return result;
      } catch (err) {
        throw err;
      }
    });
  }

  async getStaffTeamHistories(
    accountId: number,
    params: ISearchAccountTeamHistory,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`account/${accountId}/staffs/teams-histories`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    ).then(async res => {
      try {
        const mapTeamPlayer = res.data?.player_team_history?.data?.map(
          async ele => {
            if (!ele.global_config_positions) return ele;
            const accountPosition = await this.positionService.findPositionType(
              EPersonnelPositionType.Staff,
              ele.global_config_positions,
            );
            return {
              ...ele,
              accountPosition,
            };
          },
        );
        const data = await Promise.all(mapTeamPlayer);
        const result = {
          ...res,
          data: {
            player_team_history: {
              ...res.data.player_team_history,
              data,
            },
          },
        };
        return result;
      } catch (err) {
        throw err;
      }
    });
  }

  async checkPositionUsed(positionId: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`check/global-config-positions/${positionId}`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async checkDepartmentUsed(departmentId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`check/global-config-department/${departmentId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }


}
