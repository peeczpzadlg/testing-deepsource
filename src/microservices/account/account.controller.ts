import {
  Body,
  Controller,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request, Response, response } from 'express';
import { AccountSpecificationsService } from '../../personals/account-specifications/account-specifications.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../shared/utils/helper.utils';
import { EAcountType } from './account.enum';
import { AccountService } from './account.service';
import {
  GlobalConfigEducationDto,
  GlobalConfigRelationDto,
  SearchEducationDto,
  SearchRelationDto,
  UpdateEducationDto,
  UpdateRelationsDto,
  UpdateSocialContactDto,
} from './dto';
import { CreateUpdateEmailAuthenticationDto } from './dto/account-email-authentication.dto';
import { SearchAccountDto, SearchAccountReportDto } from './dto/account.dto';
import { UpdateMedicalConditionDto } from './dto/medical-condition.dto';
import { CreatePersonelDto } from './dto/personel.dto';
import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import { UpdatePublishedDto } from './dto/published.dto';
import { ApiFile } from 'src/shared/utils/file.utils';
import { FileInterceptor } from '@nestjs/platform-express';
import FormData from 'form-data';
import { createReadStream } from 'fs';
@ApiTags('Microservice Account')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class AccountController {
  constructor(
    private accountService: AccountService,
    private accountSpecificationsService: AccountSpecificationsService,
  ) {}

  // @Post('signup')
  // async createInformation(@Body() body: SignupDto) {
  //   return await getMicroserviceResponse(this.accountService.signup(body));
  // }

  @Get('account/:accountId/roles-permissions')
  async getAccountPermission(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAccountPermission(accountId, token),
    );
  }

  @Get('account/:accountId/roles-permissions/big-data')
  async getAccountPermissionBigdata(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAccountPermissionBigdata(accountId, token),
    );
  }

  @Post('account/:accountId/delete')
  async deleteAccount(
    @Param('accountId') accountId: number,
    @Req() req: Request,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteAccount(accountId, token),
    );
  }

  @Get('account/:accountId/player/position')
  async getPlayerPosition(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const account = await this.accountSpecificationsService.findAcountSpecification(
      accountId,
      token,
    );
    const response = await this.accountService.getPlayerPosition(account);
    if (response.error_code) {
      throw new ResponseFailed('Position has not found', HttpStatus.NOT_FOUND);
    } else {
      return new ResponseSuccess('successful', response);
    }
  }

  @Get('account/:accountId/staff/position')
  async getStaffPosition(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const account = await this.accountSpecificationsService.findAcountSpecification(
      accountId,
      token,
    );
    const response = await this.accountService.getStaffPosition(account);
    if (response.error_code) {
      throw new ResponseFailed('Position has not found', HttpStatus.NOT_FOUND);
    } else {
      return new ResponseSuccess('successful', response);
    }
  }

  @Get('account/personels/players')
  async getPlayers(@Req() req: Request, @Query() query: SearchAccountDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAllAccounts(EAcountType.PLAYER, query, token),
    );
  }

  @Get('account/personels/staff')
  async getStaffs(@Req() req: Request, @Query() query: SearchAccountDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAllAccounts(EAcountType.STAFF, query, token),
    );
  }

  @Get('account/:accountId')
  async getAccount(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAccount(accountId, token),
    );
  }

  @Post('account/personnels')
  async createPersonel(@Req() req: Request, @Body() body: CreatePersonelDto) {
    const token = await getHeaderAccessToken(req.headers);
    body.created_by = 'admin';
    if (body.account_region_citizen) {
      body.account_region_citizen.created_by = 'admin';
    }
    if (body.account_region_current) {
      body.account_region_current.created_by = 'admin';
    }
    return await getMicroserviceResponse(
      this.accountService.createPersonel(body, token),
    );
  }

  @Post('account/personels/:accountId/update')
  async updatePersonels(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: CreatePersonelDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.updatePersonels(accountId, body, token),
    );
  }

  @Post('account/:accountId/create/authentication')
  async createOrUpdateEmailAuthentication(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: CreateUpdateEmailAuthenticationDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.createOrUpdateEmailAuthentication(
        accountId,
        body,
        token,
      ),
    );
  }

  @Get('account/:accountId/relations')
  async getRelations(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getRelations(accountId, token),
    );
  }

  @Post('account/:accountId/relations')
  @ApiBody({ type: [UpdateRelationsDto] })
  async updateRelations(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: UpdateRelationsDto[],
  ) {
    const token = await getHeaderAccessToken(req.headers);
    body = body.map(ele => ({
      ...ele,
      created_by: 'admin',
    }));
    return await getMicroserviceResponse(
      this.accountService.updateAccountRelations(accountId, body, token),
    );
  }

  @Post('account/:accountId/relations/:relationId/delete')
  async deleteRelations(
    @Param('accountId') accountId: number,
    @Param('relationId') relationId: number,
    @Req() req: Request,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteAccountRelation(accountId, relationId, token),
    );
  }

  @Get('account/:accountId/social-contacts')
  async getSocialContact(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getSocialContact(accountId, token),
    );
  }

  @Post('account/:accountId/social-contacts')
  async updateSocialContact(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: UpdateSocialContactDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.updateSocialContact(accountId, body, token),
    );
  }

  @Get('account/:accountId/educations')
  async getEducation(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getEducation(accountId, token),
    );
  }

  @Post('account/:accountId/educations')
  @ApiBody({ type: [UpdateEducationDto] })
  async updateEducation(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: UpdateEducationDto[],
  ) {
    const token = await getHeaderAccessToken(req.headers);
    body = body.map(ele => {
      return {
        ...ele,
        created_by: 'admin',
      };
    });
    return await getMicroserviceResponse(
      this.accountService.updateAccountEducation(accountId, body, token),
    );
  }

  @Post('account/:accountId/educations/:educationId/delete')
  async deleteAccountEducation(
    @Param('accountId') accountId: number,
    @Param('educationId') educationId: number,
    @Req() req: Request,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteAccountEducation(accountId, educationId, token),
    );
  }

  @Get('account/:accountId/medical-conditions')
  async getMedicalCondition(
    @Req() req: Request,
    @Param('accountId') accountId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getMedicalCondition(accountId, token),
    );
  }

  @Post('account/:accountId/medical-conditions')
  @ApiBody({ required: false, type: [UpdateMedicalConditionDto] })
  async updateMedicalConditions(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: UpdateMedicalConditionDto[],
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.updateMedicalCondition(accountId, body, token),
    );
  }

  @Post('account/:accountId/medical-conditions/:medicalConditionId/delete')
  async deleteMedicalCondition(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Param('medicalConditionId') medicalConditionId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteMedicalCondition(
        accountId,
        medicalConditionId,
        token,
      ),
    );
  }

  @Get('setting/global/education')
  async getConfigEducations(
    @Req() req: Request,
    @Query() query: SearchEducationDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getConfigEducations(query, token),
    );
  }

  @Get('setting/global/education/:educationId')
  async getConfigEducation(
    @Req() req: Request,
    @Param('educationId') educationId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getConfigEducation(educationId, token),
    );
  }

  @Post('setting/global/education')
  async postConfigEducation(
    @Req() req: Request,
    @Body() body: GlobalConfigEducationDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      body.id
        ? this.accountService.updateEducation(body, token)
        : this.accountService.createEducation(body, token),
    );
  }

  @Post('setting/global/education/:educationId/delete')
  async deleteEducation(
    @Req() req: Request,
    @Param('educationId') educationId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteConfigEducation(educationId, token),
    );
  }

  @Get('setting/global/relation')
  async getConfigRelations(
    @Req() req: Request,
    @Query() query: SearchRelationDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getConfigRelations(query, token),
    );
  }

  @Get('setting/global/relation/:relationId')
  async getConfigRelation(
    @Req() req: Request,
    @Param('relationId') relationId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getConfigRelation(relationId, token),
    );
  }

  @Post('setting/global/relation')
  async postConfigRelation(
    @Req() req: Request,
    @Body() body: GlobalConfigRelationDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    body.created_by = 'admin';
    return await getMicroserviceResponse(
      body.id
        ? this.accountService.updateRelation(body, token)
        : this.accountService.createRelation(body, token),
    );
  }

  @Post('setting/global/relation/:relationId/delete')
  async deleteGlobalconfigRelation(
    @Req() req: Request,
    @Param('relationId') relationId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.deleteConfigRelation(relationId, token),
    );
  }


  @Post('account/:accountId/published/update')
  @ApiBody({ required: true, type: UpdatePublishedDto })
  async updatePublished(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Body() body: UpdatePublishedDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.updatePublished(accountId, body, token),
    );
  }

  @Post('account/import/excel/players')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async importAccountExcelPlayer(
      @Req() req: Request, 
      @UploadedFile() file: Express.Multer.File
  ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.accountService.importExcelPlayer(body, token));
  }

  @Post('account/import/excel/staffs')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async importAccountExcelStaff(
      @Req() req: Request, 
      @UploadedFile() file: Express.Multer.File
  ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.accountService.importExcelStaff(body, token));
  }

  @Get('account/:accountId/player/report')
  async getAccountReport(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAccountReport(accountId, token, query),
    );
  }

  @Get('account/:accountId/player/report/pdf/th')
  async getAccountReportPDF(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto,
    @Res() response:Response
  )  {
    const token = await getHeaderAccessToken(req.headers);
    const pdf = await this.accountService.getAccountReportPdf(accountId, token, query);

    pdf.pipe(response);

  }

  @Get('account/:accountId/player/report/pdf/en')
  async getAccountReportPDFEn(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto,
    @Res() response:Response
  )  {
    const token = await getHeaderAccessToken(req.headers);
    const pdf = await this.accountService.getAccountReportPdfEn(accountId, token, query);

    pdf.pipe(response);

  }

  @Get('account/count/players')
  async getCountPlayer(
    @Req() req: Request
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getCountPlayers(token),
    );
  }

  @Get('account/count/staffs')
  async getCountStaff(
    @Req() req: Request
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getCountStaffs(token),
    );
  }

  @Get('account/:accountId/staff/report')
  async getAccountStaffReport(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.accountService.getAccountStaffReport(accountId, token, query),
    );
  }

  @Get('account/:accountId/staff/report/pdf/th')
  async getAccountStaffReportPDF(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto,
    @Res() response:Response
  )  {
    const token = await getHeaderAccessToken(req.headers);
    const pdf = await this.accountService.getAccountStaffReportPdf(accountId, token, query);
    pdf.pipe(response);

  }

  @Get('account/:accountId/staff/report/pdf/en')
  async getAccountStaffReportPDFEn(
    @Req() req: Request,
    @Param('accountId') accountId: number,
    @Query() query: SearchAccountReportDto,
    @Res() response:Response
  )  {
    const token = await getHeaderAccessToken(req.headers);
    const pdf = await this.accountService.getAccountStaffReportPdfEn(accountId, token, query);
    pdf.pipe(response);

  }

}


