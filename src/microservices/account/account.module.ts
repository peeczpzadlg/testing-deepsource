import { forwardRef, Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { AccountSpecificationsModule } from '../../personals/account-specifications/account-specifications.module';
import { AccountPositionsModule } from '../../personals/account-positions/account-positions.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from '../../personals/account-specifications/account-speciafications.entity';

@Module({
  imports: [
    forwardRef(() => AccountSpecificationsModule),
    forwardRef(() => AccountPositionsModule),
    TypeOrmModule.forFeature([AccountSpecifications])
  ],
  providers: [AccountService],
  controllers: [AccountController],
  exports: [AccountService]
})
export class AccountModule {}
