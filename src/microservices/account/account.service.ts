import { HttpStatus, Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { AccountSpecifications } from '../../personals/account-specifications/account-speciafications.entity';
import { AccountPositionsService } from '../../personals/account-positions/account-positions.service';
import { EPersonnelPositionType } from '../../shared/enum/position-type';
import { ExpectError } from '../../shared/interfaces/global-config.interface';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { EAcountType } from './account.enum';
import { IGlobalConfigEducation, IGlobalConfigRelation, IPersonel, ISearchEdudation, ISearchRelation, ISignUp, IUpdateEducations, IUpdateInformations, IUpdateMedicalCondition, IUpdateRelations, IUpdateSocialContact } from './inteface';
import { ICreateUpdateEmailAuthentication } from './inteface/account-email-authentication';
import { SearchAccount, SearchAccountReport } from './inteface/account.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IUpdatePublished } from './inteface/published.interface';
import FormData from 'form-data';

@Injectable()
export class AccountService {
  private microservice: AxiosInstance;

  constructor(
    private readonly accountPositionService: AccountPositionsService,
    @InjectRepository(AccountSpecifications)
    private specificationRepository: Repository<AccountSpecifications>,
    ) {
    this.microservice = microserviceAxios();
  }

  private get accountNotFoundError() {
    return new ExpectError(HttpStatus.NOT_FOUND, 'account not found', {});
  }

  async signup(body: ISignUp) {
    return await getResponse(this.microservice.post('authentication/signup', body))
  }

  async updateAccountRelations(accountId: number, body: IUpdateRelations[], accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/relations`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async deleteAccount(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/delete`, null, {
      headers: {
        Authorization: accessToken
      }
    }).then(async (res) => {
      const specificationId = res.data.data?.account?.account_specifications;
      if (!specificationId) return res;
      try {
        const specification = await this.specificationRepository.findOne(specificationId);
        specification.deleted = true;
        specification.active = false;
        await this.specificationRepository.save(specification);
        return res; 
      } catch(err) {
        throw err;
      }
    }))
  }

  async deleteAccountRelation(accountId: number, relationId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/relations/${relationId}/delete`, null, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getRelations(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/relations`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getSocialContact(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/social-contacts`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateSocialContact(accountId: number, body: IUpdateSocialContact, accessToken: string) {
    if (body.id) {
      return await getResponse(this.microservice.post(`account/${accountId}/social-contacts/${body.id}/update`, body,
        {
          headers: {
            Authorization: accessToken
        }
      }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
    }
    return await getResponse(this.microservice.post(`account/${accountId}/social-contacts`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async getEducation(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/educations`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateAccountEducation(accountId: number, body: IUpdateEducations[], accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/educations`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  // async updateRegions(accountId: number, body: IRegion) {
  //   return await getResponse(this.microservice.post(`account/${accountId}/regions`, body));
  // }

  async getMedicalCondition(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/medical-conditions`, 
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateMedicalCondition(accountId: number, body: IUpdateMedicalCondition[], accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medical-conditions`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async deleteMedicalCondition(accountId: number, medicalConditionId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medical-conditions/${medicalConditionId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateSpecification(accountId: number, body, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/specifications`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async getAccountPermission(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/roles-permissions`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getAccountPermissionBigdata(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/roles-permissions/big-data`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getPlayerPosition(dataPlayer: any) {
    return await this.accountPositionService
      .findPositionType(EPersonnelPositionType.Player, dataPlayer.global_config_positions[0])
      .then(res => {
        const obj = {
          global_config_position_name_th: res.global_config_position_name_th, 
          global_config_position_name_en: res.global_config_position_name_en,
          global_config_position_abbreviation: res.global_config_position_abbreviation
        }
        return obj;
      }
    ).catch(err => {
      if(err) return err;
    })
  }

  async getStaffPosition(dataPlayer: any) {
    return await this.accountPositionService
      .findPositionType(EPersonnelPositionType.Staff, dataPlayer.global_config_positions[0])
      .then(res => {
        const obj = {
          global_config_position_name_th: res.global_config_position_name_th, 
          global_config_position_name_en: res.global_config_position_name_en,
          global_config_position_abbreviation: res.global_config_position_abbreviation
        }
        return obj;
      }
    ).catch(err => {
      if(err) return err;
    })
  }


  async getAccount(accountId: number, accessToken: string) {

    const responseFromMS =  await getResponse(this.microservice.get(`account/${accountId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
    // console.log('aa',responseFromMS.data.account.account_specifications);
    const { account:{account_specifications} } = responseFromMS.data;
    
    if(!account_specifications){
      // console.log(account_specifications);
      
      
      // console.log(body)
       try{

        const accountSpecification = await this.specificationRepository.save({
          account_physical_fitness:null,
          global_config_departments:null,
          account_honors:null,
          global_config_sport_categories:[],
          global_config_positions:[]
        });
        // console.log(accountSpecification)
        if (!accountSpecification.id) {
          throw 'Cant Create Specification';
        }
       
        const UpdateSpecification = await getResponse(this.microservice.post(`account/${accountId}/specification`,{
          "specificationId":accountSpecification.id
        },
          {
            headers: {
              Authorization: accessToken
          },
          
        }));
        // console.log('update',UpdateSpecification);
        if(UpdateSpecification && UpdateSpecification.success && UpdateSpecification.error_code!=406){
          // console.log(UpdateSpecification);
          responseFromMS.data.account.account_specifications = accountSpecification.id;
        }
        else{          
          const ReverseSpecification = await this.specificationRepository.save({
            id:accountSpecification.id,
            active:false,
            deleted:true,
          });

        }
      }catch(err){
        return  err;
      }
      
    }
    // console.log(account);
    // if(responseFromMS.data?.account
    return responseFromMS;
  }

  async getAllAccounts(type: EAcountType, query?: SearchAccount, accessToken?: string) {
    const params = new URLSearchParams(query as Record<string, string>);
    const data = await getResponse(this.microservice.get(`account/personnels/${type}`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
    let positions: any;
    const accId = await Promise.all(
      data.data.account.data.map(async (el) => {
        el.account_positions = null;
        if(el.account_specifications){
          const specifications = await this.specificationRepository.createQueryBuilder('specification')
          .where('specification.id = :id', { id: el.account_specifications })
          .getOne();
          if(typeof specifications != 'undefined' && specifications.global_config_positions.length > 0) {
            if(type == EAcountType.PLAYER) {
              positions = await this.accountPositionService.findPositionType(1, specifications.global_config_positions[0])
            }else{
              positions = await this.accountPositionService.findPositionType(2, specifications.global_config_positions[0])
            }
            el.account_positions = {
              global_config_position_name_th: positions.global_config_position_name_th,
              global_config_position_name_en: positions.global_config_position_name_en,
              global_config_position_abbreviation: positions.global_config_position_abbreviation,
            };
          }
        }
        return el;
      })
    )

    data.data.account.data = accId;
    
    return data;
  }

  async getAllAccountEdication(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/educations`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getAccountEducation(accountId: number, educationId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/educations/${educationId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteAccountEducation(accountId: number, educationId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/educations/${educationId}/delete`, null, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async createPersonel(body: IPersonel, accessToken: string) {
    return await getResponse(this.microservice.post(`account/personnels`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }
  
  async updatePersonels(accountId: number, body: IPersonel, accessToken: string) {
    return await getResponse(this.microservice.post(`account/personnels/${accountId}/update`, body,
      {
        headers: {
          Authorization: accessToken
        }
      }
    ));
  }

  async createOrUpdateEmailAuthentication(accountId: number, body: ICreateUpdateEmailAuthentication, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/create/authentication`, body,
      {
        headers: {
          Authorization: accessToken
        }
      }
    ));
  }

  async getConfigEducations(query: ISearchEdudation, accessToken: string) {
    const microserviceParams: any = query;
    if (query.perPage) {
      microserviceParams.size = query.perPage;
      delete microserviceParams.perPage;
    }
    const params = new URLSearchParams(microserviceParams as any);
    return await getResponse(this.microservice.get(`setting/global/educations`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async deleteConfigEducation(educationId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`setting/global/educations/${educationId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getConfigEducation(educationId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`setting/global/educations/${educationId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async createEducation(body: IGlobalConfigEducation, accessToken: string) {
    return await getResponse(this.microservice.post(`setting/global/educations`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateEducation(body: IGlobalConfigEducation, accessToken: string) {
    const educationId = body.id;
    delete body.id;
    return await getResponse(this.microservice.post(`setting/global/educations/${educationId}`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getConfigRelations(query: ISearchRelation, accessToken: string) {
    const microserviceParams: any = query;
    if (query.perPage) {
      microserviceParams.size = query.perPage;
      delete microserviceParams.perPage;
    }
    const params = new URLSearchParams(microserviceParams as Record<string, string>);
    return await getResponse(this.microservice.get(`setting/global/relations`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async deleteConfigRelation(relationId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`setting/global/relations/${relationId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getConfigRelation(relationId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`setting/global/relations/${relationId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async createRelation(body: IGlobalConfigRelation, accessToken: string) {
    return await getResponse(this.microservice.post(`setting/global/relations`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateRelation(body: IGlobalConfigRelation, accessToken: string) {
    const relationId = body.id;
    delete body.id;
    return await getResponse(this.microservice.post(`setting/global/relations/${relationId}`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async saveSpecificationId(accountId: number, specificationId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/specification`,
      { specificationId: specificationId },
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async updateInformation(accountId: number, body: IUpdateInformations, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/information/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async updatePublished(accountId: number, body: IUpdatePublished, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/published/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }), this.getAccount(accountId, accessToken), this.accountNotFoundError);
  }

  async importExcelPlayer(body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`account/import/excel/players`, body,
      {
        headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        },
        maxBodyLength: Infinity
      }
    ));
  }

  async importExcelStaff(body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`account/import/excel/staffs`, body,
      {
        headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        },
        maxBodyLength: Infinity
      }
    ));
  }

  async getAccountReport(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/player/report`, 
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getAccountReportPdf(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/player/report/pdf/th`, 
      {
        headers: {
          Authorization: accessToken
      },
      responseType:'stream',
      params
    }));
  }

  async getAccountReportPdfEn(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/player/report/pdf/en`, 
      {
        headers: {
          Authorization: accessToken
      },
      responseType:'stream',
      params
    }));
  }

  async getCountPlayers(accessToken: string) {
    return await getResponse(this.microservice.get(`account/count/players`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getCountStaffs(accessToken: string) {
    return await getResponse(this.microservice.get(`account/count/staffs`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getAccountStaffReport(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/staff/report`, 
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getAccountStaffReportPdf(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/staff/report/pdf/th`, 
      {
        headers: {
          Authorization: accessToken
      },
      responseType:'stream',
      params
    }));
  }

  async getAccountStaffReportPdfEn(accountId: number, accessToken: string, query?: SearchAccountReport) {
    const params = new URLSearchParams(query as Record<string, string>);
    return await getResponse(this.microservice.get(`account/${accountId}/staff/report/pdf/en`, 
      {
        headers: {
          Authorization: accessToken
      },
      responseType:'stream',
      params
    }));
  }

  
}
