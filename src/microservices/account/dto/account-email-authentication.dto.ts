import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString} from "class-validator";

export class CreateUpdateEmailAuthenticationDto {
  @ApiProperty()
  @IsString()
  @IsEmail()
  account_email: string;
}