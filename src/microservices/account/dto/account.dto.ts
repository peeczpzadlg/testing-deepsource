import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { ETypePersonnel } from "../../../shared/enum";
import { EGender } from "../../../shared/enum/gender.enum";
import { SearchAccount, SearchAccountReport } from "../inteface/account.interface";
import { ToBoolean } from "../../../shared/utils/transform"

export class SearchAccountDto implements SearchAccount {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;

  @ApiPropertyOptional({
    enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER]
  })
  @IsOptional()
  @IsEnum(EGender)
  gender?: EGender;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: 'enum',
    enum: ETypePersonnel
  })
  @IsOptional()
  @IsEnum(ETypePersonnel)
  type_personnel?: ETypePersonnel;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  city_name?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_min?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_max?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  national?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_club?: number;
}


export class SearchAccountReportDto implements SearchAccountReport {

  @ApiPropertyOptional()
  @ToBoolean()
  information?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  social_contact?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  social_region?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  education?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  team_national_history?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  team_club_history?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  honor_national?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  honor_club?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  honor_personal?: boolean;
}