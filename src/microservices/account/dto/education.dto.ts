import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsNumber, IsOptional, IsString } from "class-validator";
import { PaginateQueryDto } from "../../../shared/interfaces/api-interfaces";
import { IGlobalConfigEducation, ISearchEdudation, IUpdateEducations } from "../inteface";

export class UpdateEducationDto implements IUpdateEducations {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_educations: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_education_name_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_education_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_education_faculty?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_education_start_at: Date;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_education_end_at: Date;
  
  @ApiProperty()
  @IsBoolean()
  account_education_current: boolean;

  created_by?: string;
}

export class GlobalConfigEducationDto implements IGlobalConfigEducation {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_education_name_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_education_name_en?: string;

  created_by?: string;
}

export class SearchEducationDto extends PaginateQueryDto implements ISearchEdudation {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active?: string;
}