export * from './informations.dto';
export * from './sign-up.dto';
export * from './relations.dto';
export * from './social-contact.dto';
export * from './education.dto';
export * from './region.dto';
export * from './personel.dto';
