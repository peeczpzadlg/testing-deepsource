import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsDate, IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { ETypePersonnel } from "../../../shared/enum";
import { EGender } from "../../../shared/enum/gender.enum";
import { IAccountInformation } from "../inteface/informations.interface";

export class AccountInformationDto implements IAccountInformation {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_code_personnel: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_citizen_id: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_passport_id?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(ETypePersonnel)
  account_information_type_personnel: ETypePersonnel;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_firstname_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_lastname_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_firstname_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_lastname_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_address_citizen?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_address_current: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nickname?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_blood_type: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_information_dob: Date;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_religion: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EGender)
  account_information_gender: EGender;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_race: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nationality: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_phone: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_information_height: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_information_weight: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_biography_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_biography_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_hand_skill?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_feet_skill?: string
}