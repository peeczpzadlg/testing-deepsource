import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { IUpdateMedicalCondition } from "../inteface";

export class UpdateMedicalConditionDto implements IUpdateMedicalCondition {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_medical_condition_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_medical_condition_name_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_medical_condition_priority: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_medical_condition_type: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  created_by?: string;
}