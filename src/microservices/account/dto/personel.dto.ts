import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsDate, IsEnum, IsObject, IsOptional, IsString, Length, ValidateIf } from "class-validator";
import { EGender } from "../../../shared/enum/gender.enum";
import { EPositionType } from "../../../shared/enum/position-type";
import { IRegion } from "../inteface";
import { IPersonel } from "../inteface/personel.interface";
import { UpdateRegionDto } from "./region.dto";

export class CreatePersonelDto implements IPersonel {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_code_personnel: string
  
  @ApiPropertyOptional()
  @ValidateIf(o => o.account_information_code_personnel === 'THAI')
  @Length(13, 13)
  @IsString()
  account_information_citizen_id: string
  
  @ApiPropertyOptional()
  @ValidateIf(o => o.account_information_code_personnel === 'FOREIGN')
  @IsString()
  account_information_passport_id: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_type_personnel: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_firstname_th: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_lastname_th: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_firstname_en?: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_lastname_en?: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nickname: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nickname_en: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_blood_type: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_information_dob: Date
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EGender)
  account_information_gender: EGender;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_religion: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_race: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nationality: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_nationality_en: string
  
  @ApiPropertyOptional({
    type: UpdateRegionDto
  })
  @IsOptional()
  @IsObject()
  account_region_current: IRegion
  
  @ApiPropertyOptional({
    type: UpdateRegionDto
  })
  @IsOptional()
  @IsObject()
  account_region_citizen: IRegion
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EPositionType)
  account_type: EPositionType;
  
  @ApiPropertyOptional({ type: Boolean, required: false, default: false})
  active: boolean;

  created_by: string;
}
