import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { IUpdatePublished } from '../inteface/published.interface';

export class UpdatePublishedDto implements IUpdatePublished {
  @ApiPropertyOptional({ type: Boolean,default:false })
  @IsBoolean()
  account_published?: boolean;
}
