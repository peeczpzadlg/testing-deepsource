import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { IRegion } from "../inteface";

export class UpdateRegionDto implements IRegion {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_region_country_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_region_address_detail: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_region_address_detail_en: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_region_province_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_region_district_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_region_subdistrict_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_region_zipcode: string;
}