import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsNumber, IsOptional, IsString } from "class-validator";
import { PaginateQueryDto } from "../../../shared/interfaces/api-interfaces";
import { IGlobalConfigRelation, ISearchRelation, IUpdateRelations } from "../inteface";

export class UpdateRelationsDto implements IUpdateRelations {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_firstname_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_lastname_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_firstname_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_lastname_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_phone: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEmail()
  account_relation_email?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_address?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_relation_country_id: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_country_code?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_country_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_country_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_city_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_city_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_district_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_district_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_subdistrict_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_subdistrict_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_relation_zipcode?: string;

  created_by?: string;
}

export class GlobalConfigRelationDto implements IGlobalConfigRelation {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_relation_name_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_relation_name_en: string;

  created_by?: string;
}

export class SearchRelationDto extends PaginateQueryDto implements ISearchRelation {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active?: string;
}
