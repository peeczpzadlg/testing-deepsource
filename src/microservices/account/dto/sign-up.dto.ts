import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString, MinLength } from "class-validator";
import { Match } from "../../../shared/utils/match.decorator";
import { ISignUp } from "../inteface/sign-up.interface";

export class SignupDto implements ISignUp {
  @ApiProperty()
  @IsEmail()
  account_email: string;

  @ApiProperty()
  @MinLength(6)
  @IsString()
  account_password: string;

  @ApiProperty()
  @IsString()
  @MinLength(6)
  @Match('account_password')
  confirm_password: string;
}