import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { IUpdateSocialContact } from "../inteface";

export class UpdateSocialContactDto implements IUpdateSocialContact{
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;


  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_facebook?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_email?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_phone?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_line?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_twitter?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_instagram?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_youtube?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_website?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_social_contact_fax?: string;
}