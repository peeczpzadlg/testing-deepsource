import { ETypePersonnel } from "../../../shared/enum";
import { EGender } from "../../../shared/enum/gender.enum";

export interface SearchAccount {
  page?: number;
  size?: number;
  gender?: EGender;
  name?: string;
  type_personnel?: ETypePersonnel;
  city_name?: string;
  age_min?: number;
  age_max?: number;
  national?: number;
  team_club?: number;
}

export interface SearchAccountReport {
  information?: boolean;
  social_contact?: boolean;
  social_region?: boolean;
  education?: boolean;
  team_national_history?: boolean;
  team_club_history?: boolean;
  honor_national?: boolean;
  honor_club?: boolean;
  honor_personal?: boolean;
}