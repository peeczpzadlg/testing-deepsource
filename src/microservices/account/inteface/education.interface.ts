import { PrimaryData } from "../../../shared/entities/primary-data.entity";
import { IPaginateQuery } from "../../../shared/interfaces/api-interfaces";

export interface IUpdateEducations extends PrimaryData {
  global_config_educations: number;
  account_education_name_th: string;
  account_education_name_en?: string;
  account_education_faculty?: string;
  account_education_start_at: Date;
  account_education_end_at: Date;
  account_education_current: boolean;
}

export interface IGlobalConfigEducation extends PrimaryData {
  global_config_education_name_th: string;
  global_config_education_name_en?: string;
}

export interface ISearchEdudation extends IPaginateQuery {
  name?: string;
  active?: string;
}