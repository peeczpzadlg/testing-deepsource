export * from './informations.interface';
export * from './sign-up.interface';
export * from './relations.interface';
export * from './social-contact.interface';
export * from './education.interface';
export * from './region.interface';
export * from './medical-condition.interface';
export * from './personel.interface';
