import { PrimaryData } from "../../../shared/entities/primary-data.entity";
import { ETypePersonnel } from "../../../shared/enum";
import { EGender } from "../../../shared/enum/gender.enum";

export interface IAccountInformation extends PrimaryData {
  account_information_code_personnel: string;
  account_information_citizen_id: string;
  account_information_passport_id?: string;
  account_information_type_personnel: ETypePersonnel;
  account_information_firstname_th: string;
  account_information_lastname_th: string;
  account_information_firstname_en?: string;
  account_information_lastname_en?: string;
  account_information_address_citizen?: string;
  account_information_address_current: string;
  account_information_nickname?: string;
  account_information_blood_type: string;
  account_information_dob: Date;
  account_information_religion: string;
  account_information_race: string;
  account_information_gender: EGender;
  account_information_nationality: string;
  account_information_phone: string;
  account_information_height: number;
  account_information_weight: number;
  account_information_biography_th?: string;
  account_information_biography_en?: string;
  account_information_hand_skill?: string;
  account_information_feet_skill?: string
}

export interface IUpdateInformations {
  account_information_height: number;
  account_information_weight: number;
  account_information_biography_th: string;
  account_information_biography_en?: string;
  account_information_hand_skill: string;
  account_information_feet_skill: string;
  account_information_blood_type: string;
  updated_by: string;
} 
