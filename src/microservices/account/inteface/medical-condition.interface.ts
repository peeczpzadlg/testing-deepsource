import { PrimaryData } from "../../../shared/entities/primary-data.entity";

export interface IUpdateMedicalCondition extends PrimaryData {
  account_medical_condition_name_th: string;
  account_medical_condition_name_en?: string;
  account_medical_condition_priority: number;
  account_medical_condition_type: string;
}