import { PrimaryData } from "../../../shared/entities/primary-data.entity";
import { EGender } from "../../../shared/enum/gender.enum";
import { EPositionType } from "../../../shared/enum/position-type";
import { IRegion } from "./region.interface";

export interface IPersonel extends PrimaryData{
  account_information_code_personnel: string
  account_information_citizen_id?: string
  account_information_passport_id?: string
  account_information_type_personnel?: string
  account_information_firstname_th?: string
  account_information_lastname_th?: string
  account_information_firstname_en?: string
  account_information_lastname_en?: string
  account_information_nickname?: string
  account_information_blood_type?: string
  account_information_dob?: Date
  account_information_gender?: EGender
  account_information_religion?: string
  account_information_race?: string
  account_information_nationality?: string
  account_information_nationality_en?: string
  account_region_current?: IRegion
  account_region_citizen?: IRegion
  account_type?: EPositionType
}

export interface IUpdateAccount {
  account_name: string;
  account_tel: string;
  account_social_contact_id: number;
  account_social_contact_your_id: string;
}
