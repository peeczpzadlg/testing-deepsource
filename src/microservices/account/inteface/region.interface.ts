import { PrimaryData } from "../../../shared/entities/primary-data.entity";

export interface IRegion extends PrimaryData {
  account_region_country_id: number,
  account_region_address_detail: string,
  account_region_address_detail_en: string,
  account_region_province_id: number,
  account_region_district_id: number,
  account_region_subdistrict_id: number,
  account_region_zipcode: string
}
