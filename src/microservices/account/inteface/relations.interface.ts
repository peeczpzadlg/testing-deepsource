import { PrimaryData } from "../../../shared/entities/primary-data.entity";
import { IPaginateQuery } from "../../../shared/interfaces/api-interfaces";

export interface IUpdateRelations extends PrimaryData {
  account_relation_firstname_th: string;
  account_relation_lastname_th: string;
  account_relation_firstname_en?: string;
  account_relation_lastname_en?: string;
  account_relation_phone: string;
  account_relation_email?: string;
  account_relation_address?: string;
  account_relation_country_id: number;
  account_relation_country_code?: string;
  account_relation_country_name_th?: string;
  account_relation_country_name_en?: string;
  account_relation_city_name_th?: string;
  account_relation_city_name_en?: string;
  account_relation_district_name_th?: string;
  account_relation_district_name_en?: string;
  account_relation_subdistrict_name_th?: string;
  account_relation_subdistrict_name_en?: string;
  account_relation_zipcode?: string;
}

export interface IGlobalConfigRelation extends PrimaryData {
  global_config_relation_name_th: string;
  global_config_relation_name_en: string;
}

export interface ISearchRelation extends IPaginateQuery {
  name?: string;
  active?: string;
}