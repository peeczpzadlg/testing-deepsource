export interface ISignUp {
  account_email: string;
  account_password: string;
  confirm_password: string;
}