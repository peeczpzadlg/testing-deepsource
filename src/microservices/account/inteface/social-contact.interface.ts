import { PrimaryData } from "../../../shared/entities/primary-data.entity";

export interface IUpdateSocialContact extends PrimaryData{
  account_social_contact_facebook?: string;
  account_social_contact_line?: string;
  account_social_contact_twitter?: string;
  account_social_contact_instagram?: string;
  account_social_contact_youtube?: string;
  account_social_contact_website?: string;
  account_social_contact_fax?: string;
}