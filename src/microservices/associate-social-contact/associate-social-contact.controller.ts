import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SaveAssociateSocialContactDto } from './associate-social-contact.dto';
import { AssociateSocialContactService } from './associate-social-contact.service';

@ApiTags('Setting Association Social-Contacts')
@Controller('association-social-contact')
export class AssociateSocialContactController {
  constructor(
    private associateSocialContactService: AssociateSocialContactService
  ) {}

  @Get(':associationId/social-contact')
  async getSocialContact(@Param('associationId') associationId: number) {
    return await getMicroserviceResponse(this.associateSocialContactService.getSocialContact(associationId));
  }

  @Post(':associationId/social-contacts')
  @ApiBody({ required: false, type: SaveAssociateSocialContactDto })
  async saveSocialContact(@Param('associationId') associationId: number, @Body() body: SaveAssociateSocialContactDto) {
    return await getMicroserviceResponse(this.associateSocialContactService.saveSocialContact(associationId, body));
  }

  @Post(':associationId/social-contacts/:socialContactId/delete')
  async deleteSocialContact(@Param('associationId') associationId: number, @Param('socialContactId') socialContactId: number) {
    return await getMicroserviceResponse(this.associateSocialContactService.deleteSocialContact(associationId, socialContactId));
  }
}
