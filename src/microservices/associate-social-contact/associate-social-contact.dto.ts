import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { ISaveAssociateSocialContact } from './associate-social-contact.interface';

export class SaveAssociateSocialContactDto implements ISaveAssociateSocialContact {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_email: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_phone: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_facebook: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_line: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_twitter: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_instagram: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_youtube: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_website: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_social_contact_fax: string;
}
