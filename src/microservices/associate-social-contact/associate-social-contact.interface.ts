export interface ISaveAssociateSocialContact {
    global_config_association_social_contact_phone: string;
    global_config_association_social_contact_email: string;
    global_config_association_social_contact_facebook: string;
    global_config_association_social_contact_line: string;
    global_config_association_social_contact_twitter: string;
    global_config_association_social_contact_instagram: string;
    global_config_association_social_contact_youtube: string;
    global_config_association_social_contact_website: string;
    global_config_association_social_contact_fax: string;
  }
  