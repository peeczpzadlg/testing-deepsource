import { Module } from '@nestjs/common';
import { AssociateSocialContactController } from './associate-social-contact.controller';
import { AssociateSocialContactService } from './associate-social-contact.service';

@Module({
  controllers: [AssociateSocialContactController],
  providers: [AssociateSocialContactService]
})
export class AssociateSocialContactModule {}
