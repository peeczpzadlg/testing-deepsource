import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISaveAssociateSocialContact } from './associate-social-contact.interface';

@Injectable()
export class AssociateSocialContactService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getSocialContact(associationId: number) {
    return await getResponse(this.microservice(`association/${associationId}/social-contact`));
  }

  async saveSocialContact(associationId: number, body: ISaveAssociateSocialContact) {
    return await getResponse(this.microservice.post(`association/${associationId}/social-contact`, body));
  }

  async deleteSocialContact(associationId: number, socialConatactId: number) {
    return await getResponse(this.microservice.post(`association/${associationId}/social-contact/${socialConatactId}/delete`));
  }
}
