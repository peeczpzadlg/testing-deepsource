import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SaveAssociationDto, UpdateAssociationDto } from './associate.dto';
import { AssociateService } from './associate.service';

@ApiTags('Assocation')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('association')
export class AssociateController {
    constructor(
        private associateService: AssociateService
    ) {}

    @Get()
    async getAssociate(@Req() req: Request) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.associateService.getAssociate(token))
    }

    @Post('create')
    @ApiBody({ required: true, type: SaveAssociationDto })
    async createTeam(@Req() req: Request, @Body() body: SaveAssociationDto) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.associateService.createAssociate(body, token));
    }
    
    @Post(':associationId/update')
    @ApiBody({ required: false, type: UpdateAssociationDto })
    async updateTeam(
        @Req() req: Request, 
        @Param('associationId') associationId: number, 
        @Body() body: UpdateAssociationDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.associateService.updateAssociate(associationId, body, token));
    }
    
    @Post(':associationId/delete')
    async deleteTeam(@Req() req: Request, @Param('associationId') associationId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.associateService.deleteAssociate(associationId, token));
    }
}
