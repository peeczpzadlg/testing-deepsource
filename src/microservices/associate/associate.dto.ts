import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ISaveAssociation, IAssociationRegion, IUpdateAssociation } from './associate.interface';

export class AssociationRegionDto implements IAssociationRegion {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_association_region_country_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_region_address_detail: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_region_address_detail_en: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_association_region_province_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_association_region_district_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_association_region_subdistrict_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_region_zipcode: string;
}

export class SaveAssociationDto implements ISaveAssociation {
  @ApiProperty()
  @IsString()
  global_config_association_information_name_th: string;

  @ApiProperty()
  @IsString()
  global_config_association_information_name_en: string;

  @ApiProperty()
  @IsString()
  global_config_association_information_abbreviation_th: string;

  @ApiProperty()
  @IsString()
  global_config_association_information_abbreviation_en: string;

  @ApiProperty()
  global_config_association_regions: AssociationRegionDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_information_history_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_association_information_history_en: string;
}

export class UpdateAssociationDto implements IUpdateAssociation {
  @ApiProperty({ type: String, required: false})
  global_config_association_information_name_th: string;

  @ApiProperty({ type: String, required: false})
  global_config_association_information_name_en: string;


  @ApiProperty({ type: String, required: false})
  global_config_association_information_abbreviation_th: string;


  @ApiProperty({ type: String, required: false})
  global_config_association_information_abbreviation_en: string;

  @ApiPropertyOptional({ type: AssociationRegionDto, required: false})
  global_config_association_regions: AssociationRegionDto;

  @ApiProperty({ type: String, required: false})
  global_config_association_information_history_th: string;

  @ApiProperty({ type: String, required: false})
  global_config_association_information_history_en: string;

  @ApiProperty({ type: Boolean, required: false})
  global_config_association_published: string;
}