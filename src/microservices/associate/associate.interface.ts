export interface ISaveAssociation {
    global_config_association_information_name_th: string;
    global_config_association_information_name_en: string;
    global_config_association_information_abbreviation_th: string;
    global_config_association_information_abbreviation_en: string;
    global_config_association_regions: IAssociationRegion;
    global_config_association_information_history_th: string;
    global_config_association_information_history_en: string;
}

export interface IUpdateAssociation {
    global_config_association_information_name_th: string;
    global_config_association_information_name_en: string;
    global_config_association_information_abbreviation_th: string;
    global_config_association_information_abbreviation_en: string;
    global_config_association_regions: IAssociationRegion;
    global_config_association_information_history_th: string;
    global_config_association_information_history_en: string;
    global_config_association_published: string;
}

export interface IAssociationRegion {
    global_config_association_region_country_id: number;
    global_config_association_region_address_detail: string;
    global_config_association_region_address_detail_en: string;
    global_config_association_region_province_id: number;
    global_config_association_region_district_id: number;
    global_config_association_region_subdistrict_id: number;
    global_config_association_region_zipcode: string;
}