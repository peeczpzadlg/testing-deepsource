import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISaveAssociation } from './associate.interface';

@Injectable()
export class AssociateService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(AssociateService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getAssociate(accessToken: string) {
        try {
          return await getResponse(this.microservice.get('association',
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async createAssociate(body: ISaveAssociation, accessToken: string) {
        try {
          return await getResponse(this.microservice.post('association/create', body,
            {
              headers: {
                Authorization: accessToken
            }
          }));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updateAssociate(associationId: number, body: ISaveAssociation, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`association/${associationId}/update`, body,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        }
    }

    async deleteAssociate(associationId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`association/${associationId}/delete`, null,
            {
              headers: {
                Authorization: accessToken
            }
          }));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
}
