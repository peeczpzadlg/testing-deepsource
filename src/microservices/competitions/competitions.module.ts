import { Module } from '@nestjs/common';
import { MainCompetitionsModule } from './main-competitions/main-competitions.module';
import { SubCompetitionsModule } from './sub-competitions/sub-competitions.module';

@Module({
  imports: [MainCompetitionsModule, SubCompetitionsModule]
})
export class CompetitionsModule {}
