import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";
import { ToBoolean } from "../../../../shared/utils/transform";
import { ECompetitionLevel } from "../../../../shared/enum";
import { ISaveMainCompetition, IUpdateMainCompetitionPublished } from "../main-competitions.interface";

export class SaveMainCompetitionDto implements ISaveMainCompetition {
    @ApiProperty()
    @IsNumber()
    global_config_standard_types: number;
  
    @ApiProperty()
    @IsString()
    setting_competition_information_code: string;
  
    @ApiProperty()
    @IsString()
    setting_competition_information_name_th: string;
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_name_en: string;
  
    @ApiProperty()
    @IsString()
    setting_competition_information_organizer_th: string;
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_organizer_en: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_abbreviation: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_abbreviation_en: string;
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_history_th: string;
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    setting_competition_information_history_en: string;

    @ApiProperty({ type: String, enum: ECompetitionLevel, required: true })
    @IsString()
    setting_competition_level: ECompetitionLevel;
    
    @ApiPropertyOptional({ type: Boolean, required: false, default: true })
    @ToBoolean()
    active: boolean;
  }

  export class UpdateCompetitionPublished implements IUpdateMainCompetitionPublished{
    @ApiPropertyOptional({ type: Boolean, default:false })
    @IsBoolean()
    setting_competition_published?: boolean;
  }