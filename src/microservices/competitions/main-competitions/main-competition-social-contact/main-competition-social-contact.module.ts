import { Module } from '@nestjs/common';
import { MainCompetitionSocialContactService } from './main-competition-social-contact.service';

@Module({
  providers: [MainCompetitionSocialContactService],
  exports: [MainCompetitionSocialContactService]
})
export class MainCompetitionSocialContactModule {}
