import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { IUpdateMainCompetitionSocialContact } from '../main-competitions.interface';

@Injectable()
export class MainCompetitionSocialContactService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(MainCompetitionSocialContactService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getMainCompetitionSocialContact(params: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`competition/${params}/setting/social-contacts`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        }
    }

    async createMainCompetitionSocialContact(competitionId: number, body: IUpdateMainCompetitionSocialContact, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`competition/${competitionId}/setting/social-contacts`, body,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async deleteMainCompetitionSocialContact(competitionId: number, socialContactId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`competition/${competitionId}/setting/social-contacts/${socialContactId}/delete`, null,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
}
