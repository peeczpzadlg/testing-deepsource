import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ApiFile } from '../../../shared/utils/file.utils';
import { SearchPaginateActiveDto } from '../../../microservices/microservices.dto';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../shared/utils/helper.utils';
import { UpdateMainCompetitionSocialContactDto } from './dtos/main-competition-social-contact.dto';
import { SaveMainCompetitionDto, UpdateCompetitionPublished } from './dtos/main-competitions.dto';
import { MainCompetitionSocialContactService } from './main-competition-social-contact/main-competition-social-contact.service';
import { MainCompetitionsService } from './main-competitions.service';
import { JwtAuthGuard } from '../../../auth/jwt-auth.guard';
import FormData from 'form-data';

@ApiTags('Setting Main Competitions')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class MainCompetitionsController {
    constructor(
        private mainCompetitionsService: MainCompetitionsService,
        private mainCompetitionSocialContact: MainCompetitionSocialContactService
    ) {}

    @Get('competitions')
    async getMainCompetition(@Req() req: Request, @Query() query: SearchPaginateActiveDto) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.getMainCompetition(query, token))
    }

    @Get('competitions/:competitionId')
    async getMainCompetitionById(@Req() req: Request, @Param('competitionId') competitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.getMainCompetitionById(competitionId, token))
    }
    
    @Post('competitions/create')
    @ApiBody({ required: true, type: SaveMainCompetitionDto })
    async createMainCompetition(@Req() req: Request, @Body() body: SaveMainCompetitionDto) {
        body.active = body.active ?? true;
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.createMainCompetition(body, token));
    }

    @Post('competitions/:competitionId/update')
    async updateMainCompetition(
        @Req() req: Request,
        @Param('competitionId') competitionId: number, 
        @Body() body: SaveMainCompetitionDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.updateMainCompetition(competitionId, body, token));
    }

    @Post('competitions/:competitionId/delete')
    async deleteMainCompetition(@Req() req: Request, @Param('competitionId') competitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.deleteMainCompetition(competitionId, token));
    }

    @Get('competitions/:competitionId/setting/social-contacts')
    async getMainCompetitionSocialContact(@Req() req: Request, @Param('competitionId') competitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionSocialContact.getMainCompetitionSocialContact(competitionId, token));
    }
    
    @Post('competitions/:competitionId/setting/social-contacts')
    @ApiBody({ required: false, type: UpdateMainCompetitionSocialContactDto })
    async createMainCompetitionSocialContact(
        @Req() req: Request,
        @Param('competitionId') competitionId: number, 
        @Body() body: UpdateMainCompetitionSocialContactDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionSocialContact.createMainCompetitionSocialContact(competitionId, body, token));
    }

    @Post('competitions/:competitionId/setting/social-contacts/:socialContactId/delete')
    async deleteMainCompetitionSocialContact(
        @Req() req: Request,
        @Param('competitionId') competitionId: number,
        @Param('socialContactId') socialContactId: number,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionSocialContact.deleteMainCompetitionSocialContact(competitionId, socialContactId, token));
    }

    @Get('competitions/:competitionId/medias/profile')
    async getCompetitionProfileImage(@Req() req: Request, @Param('competitionId') competitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.getProfileImage(competitionId, token))
    }

    @Post('competitions/:competitionId/medias/profile')
    @ApiConsumes('multipart/form-data')
    @ApiFile()
    @UseInterceptors(FileInterceptor('file'))
    async uploadCompetitionProfileImage(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @UploadedFile() file: Express.Multer.File
    ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.mainCompetitionsService.uploadProfileImage(competitionId, body, token));
    }

    @Post('competitions/:competitionId/published/update')
    @ApiBody({ required: true, type: UpdateCompetitionPublished })
    async updateTeamPublished(@Req() req: Request, @Param('competitionId') competitionId: number,@Body() body: UpdateCompetitionPublished) {
    const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.mainCompetitionsService.updatePublished(competitionId,body,token));
    }
}
