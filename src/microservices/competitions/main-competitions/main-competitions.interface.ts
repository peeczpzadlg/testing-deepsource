import { IMicroservicePaginateQuery } from "../../../shared/interfaces/api-interfaces";

export interface ISearchMainCompetition extends IMicroservicePaginateQuery {
    name?: string;
    active?: string;
}

export interface ISaveMainCompetition {
    global_config_standard_types: number,
    setting_competition_information_code: string,
    setting_competition_information_name_th: string,
    setting_competition_information_name_en: string,
    setting_competition_information_organizer_th: string,
    setting_competition_information_organizer_en: string,
    setting_competition_information_abbreviation: string,
    setting_competition_information_abbreviation_en: string,
    setting_competition_information_history_th: string,
    setting_competition_information_history_en: string,
    setting_competition_level: string
    active: boolean
}

export interface IUpdateMainCompetitionSocialContact {
    setting_competition_social_contact_phone?: string,
    setting_competition_social_contact_email?: string,
    setting_competition_social_contact_youtube?: string,
    setting_competition_social_contact_facebook?: string,
    setting_competition_social_contact_line?: string,
    setting_competition_social_contact_instagram?: string,
    setting_competition_social_contact_twitter?: string,
    setting_competition_social_contact_website?: string,
    setting_competition_social_contact_fax?: string
}

export interface IUpdateMainCompetitionPublished {
    setting_competition_published?: boolean;
}