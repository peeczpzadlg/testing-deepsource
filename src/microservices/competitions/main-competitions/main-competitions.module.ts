import { Module } from '@nestjs/common';
import { MainCompetitionsController } from './main-competitions.controller';
import { MainCompetitionsService } from './main-competitions.service';
import { MainCompetitionSocialContactModule } from './main-competition-social-contact/main-competition-social-contact.module';

@Module({
  controllers: [MainCompetitionsController],
  providers: [MainCompetitionsService],
  imports: [MainCompetitionSocialContactModule]
})
export class MainCompetitionsModule {}
