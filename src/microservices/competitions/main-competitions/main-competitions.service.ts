import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import FormData from 'form-data';
import { ISearchPaginateActive } from '../../../microservices/microservices.interface';
import { getResponse } from '../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../shared/utils/microservice-axios.utils';
import { ISaveMainCompetition, IUpdateMainCompetitionPublished } from './main-competitions.interface';

@Injectable()
export class MainCompetitionsService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(MainCompetitionsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getMainCompetition(params: ISearchPaginateActive, accessToken: string) {
        try {
          return await getResponse(this.microservice.get('competitions',
            {
              headers: {
                Authorization: accessToken
            },
            params
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getMainCompetitionById(params: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`competitions/${params}`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async createMainCompetition(body: ISaveMainCompetition, accessToken: string) {
      try {
        return await getResponse(this.microservice.post('competitions/create', body,
          {
            headers: {
              Authorization: accessToken
          }
        }));
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

    async updateMainCompetition(competitionId: number, body: ISaveMainCompetition, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/update`, body,
          {
            headers: {
              Authorization: accessToken
          }
        }))
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

    async deleteMainCompetition(competitionId: number, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/delete`, null,
          {
            headers: {
              Authorization: accessToken
          }
        }))
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

    async getProfileImage(params: number, accessToken: string) {
      try {
        return await getResponse(this.microservice.get(`competitions/${params}/medias/profile`,
          {
            headers: {
              Authorization: accessToken
          }
        }))
      } catch(err) {
        this.logger.error(err);
        throw err;
      } 
    }

    async uploadProfileImage(competitionId: number, body: FormData, accessToken: string) {
      return await getResponse(this.microservice.post(`competitions/${competitionId}/medias/profile`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        }
      }));
    }

    async updatePublished(competitionId: number, body: IUpdateMainCompetitionPublished, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/published/update`,body,
          {
            headers: {
              Authorization: accessToken
          }
        }))
      } catch(err) {
        this.logger.error(err);
        throw err;
      } 
    }
}
