import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { CompetitionSectionsService } from './competition-sections.service';
import { CompetitionSectionCreateDto } from './dtos/competition-sections-create.dto';
import { GroupStagesCreateDto, GroupStagesUpdatePointdto } from './dtos/group-stages-create.dto';

@ApiTags('Competition Sections')
@Controller()
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class CompetitionSectionsController {
    constructor(
        private competitionSectionsService: CompetitionSectionsService,
        // private mainCompetitionSocialContact: MainCompetitionSocialContactService
    ) {}

    @Get('sub-competitions/:subCompetitionId/competition-sections')
    async getCompetitionSections(@Req() req: Request, @Param('subCompetitionId') subCompetitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.getListCompetitionSections(subCompetitionId, token))
    }

    @Get('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId')
    async getCompetitionSectionById(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Param('competitionSectionId') competitionSectionId: number, 
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.getCompetitionSectionById(subCompetitionId, competitionSectionId, token))
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/create')
    @ApiBody({ required: true, type: CompetitionSectionCreateDto})
    async createCompetitionSection(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Body() body: CompetitionSectionCreateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.createCompetitionSection(subCompetitionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/update')
    @ApiBody({ required: true, type: CompetitionSectionCreateDto})
    async updateCompetitionSection(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
        @Body() body: CompetitionSectionCreateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.updateCompetitionSection(subCompetitionId, competitionSectionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/delete')
    async deleteCompetitionSection(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.deleteCompetitionSection(subCompetitionId, competitionSectionId, token));
    }

    @Get('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages')
    async getCompetitionSectionGroupStages(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Param('competitionSectionId') competitionSectionId: number, 
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.getCompetitionSectionGroupStages(subCompetitionId, competitionSectionId, token))
    }

    @Get('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages/:groupStageId')
    async getCompetitionSectionGroupStagesById(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Param('competitionSectionId') competitionSectionId: number, 
        @Param('groupStageId') groupStageId: number,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.getCompetitionSectionGroupStagesById(subCompetitionId, competitionSectionId, groupStageId, token))
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages/create')
    @ApiBody({ required: true, type: GroupStagesCreateDto})
    async createGroupStages(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
        @Body() body: GroupStagesCreateDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.createGroupStages(subCompetitionId, competitionSectionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages/:groupStageId/update')
    @ApiBody({ required: true, type: GroupStagesCreateDto})
    async updateGroupStages(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
        @Param('groupStageId') groupStageId: number,
        @Body() body: GroupStagesCreateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.updateGroupStages(subCompetitionId, competitionSectionId, groupStageId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages/:groupStageId/update-point')
    @ApiBody({ required: true, type: [GroupStagesUpdatePointdto]})
    async updatePointGroupStages(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
        @Param('groupStageId') groupStageId: number,
        @Body() body: GroupStagesUpdatePointdto[]
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.updatePointGroupStages(subCompetitionId, competitionSectionId, groupStageId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stages/:groupStageId/delete')
    @ApiBody({ required: true, type: GroupStagesCreateDto})
    async deleteGroupStages(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('competitionSectionId') competitionSectionId: number, 
        @Param('groupStageId') groupStageId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionsService.deleteGroupStages(subCompetitionId, competitionSectionId, groupStageId, token));
    }
}