export interface ISaveCompetitionSection {
    section_type: string
    competition_section_round_name: string
    competition_section_name_th: string
    competition_section_name_en: string
    competition_section_description_th: string
    competition_section_description_en: string
    active: boolean
}

export interface ISaveGroupStages {
    competition_group_stage_name: string;
    competition_group_stage_name_en: string;
    competition_group_stage_team_join: ICompetitionGroupStageTeamJoin
}

export interface IUpdatePointGroupStages {
    team_id: number;
    points: number;
}

export interface ICompetitionGroupStageTeamJoin {
    team_id: number;
    team_name: string;
    team_name_en: string;
    team_img?: string;
}