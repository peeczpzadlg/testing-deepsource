import { Module } from '@nestjs/common';
import { CompetitionSectionsController } from './competition-sections.controller';
import { CompetitionSectionsService } from './competition-sections.service';
import { VersusMatchModule } from './versus-match/versus-match.module';
import { VersusMatchInGroupStagesModule } from './versus-match-in-group-stages/versus-match-in-group-stages.module';

@Module({
  controllers: [CompetitionSectionsController],
  providers: [CompetitionSectionsService],
  imports: [VersusMatchModule, VersusMatchInGroupStagesModule]
})
export class CompetitionSectionsModule {}
