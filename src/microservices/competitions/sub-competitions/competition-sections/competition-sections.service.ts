import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../../../../microservices/microservices.interface';
import { ISaveCompetitionSection, ISaveGroupStages, IUpdatePointGroupStages } from './competition-sections.interface';
// import { IDeleteMultipleTeamInSubCompetition, ISaveTeamInSubCompetition } from '../sub-competitions.interface';

@Injectable()
export class CompetitionSectionsService {

    private microservice: AxiosInstance;
    private logger: Logger = new Logger(CompetitionSectionsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getListCompetitionSections(params: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${params}/competition-sections`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getCompetitionSectionById(subCompetitionId: number, competitionSectionId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async createCompetitionSection(params: number, body: ISaveCompetitionSection, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${params}/competition-sections/create`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updateCompetitionSection(subCompetitionId: number, competitionSectionId: number, body: ISaveCompetitionSection, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/update`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async deleteCompetitionSection(subCompetitionId: number, competitionSectionId: number, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/delete`, null,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
    
    async getCompetitionSectionGroupStages(subCompetitionId: number, competitionSectionId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getCompetitionSectionGroupStagesById(subCompetitionId: number, competitionSectionId: number, groupStageId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages/${groupStageId}`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async createGroupStages(subCompetitionId: number, competitionSectionId: number, body: ISaveGroupStages, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages/create`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updateGroupStages(subCompetitionId: number, competitionSectionId: number, groupStageId: number, body: ISaveGroupStages, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages/${groupStageId}/update`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updatePointGroupStages(subCompetitionId: number, competitionSectionId: number, groupStageId: number, body: IUpdatePointGroupStages[], accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages/${groupStageId}/update-point`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async deleteGroupStages(subCompetitionId: number, competitionSectionId: number, groupStageId: number, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/competition-sections/${competitionSectionId}/group-stages/${groupStageId}/delete`, null,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
}