import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { ISaveCompetitionSection } from '../competition-sections.interface';

export class CompetitionSectionCreateDto implements ISaveCompetitionSection {
  @ApiProperty({ type: String, required: true })
  @IsString()
  section_type: string;

  @ApiProperty({ type: String, required: true })
  @IsString()
  competition_section_round_name: string;
  
  @ApiProperty({ type: String, required: true })
  @IsString()
  competition_section_name_th: string;

  @ApiPropertyOptional({ type: String, required: false })
  @IsOptional()
  @IsString()
  competition_section_name_en: string;

  @ApiPropertyOptional({ type: String, required: false })
  @IsOptional()
  @IsString()
  competition_section_description_th: string;

  @ApiPropertyOptional({ type: String, required: false })
  @IsOptional()
  @IsString()
  competition_section_description_en: string;

  @ApiPropertyOptional({ type: Boolean, default: true, required: false })
  @IsOptional()
  @IsBoolean()
  active: boolean;
}