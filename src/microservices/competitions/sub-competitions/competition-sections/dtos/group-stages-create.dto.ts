import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ICompetitionGroupStageTeamJoin, ISaveGroupStages, IUpdatePointGroupStages,  } from '../competition-sections.interface';

export class CompetitionGroupStageTeamJoinDto implements ICompetitionGroupStageTeamJoin {
    @ApiProperty({ type: Number, required: true })
    @IsNumber()
    team_id: number;

    @ApiProperty({ type: String, required: true })
    @IsString()
    team_name: string;

    @ApiProperty({ type: String, required: true })
    @IsString()
    team_name_en: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    team_img: string;
}

export class GroupStagesCreateDto implements ISaveGroupStages {
    @ApiProperty({ type: String, required: true })
    @IsString()
    competition_group_stage_name: string;
    
    @ApiProperty({ type: String, required: true })
    @IsString()
    competition_group_stage_name_en: string;

    @ApiPropertyOptional({ type: [CompetitionGroupStageTeamJoinDto] })
    competition_group_stage_team_join: CompetitionGroupStageTeamJoinDto;
  
}

export class GroupStagesUpdatePointdto implements IUpdatePointGroupStages {
    @ApiProperty({ type: Number, required: true })
    @IsNumber()
    team_id: number;

    @ApiProperty({ type: Number, required: true })
    @IsNumber()
    points: number;
  
}


