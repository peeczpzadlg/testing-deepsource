import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { ISearchVersusMatchInGroupStages } from '../versus-match-in-group-stages.interface';

export class VersusMatchGroupStageSearchDto implements ISearchVersusMatchInGroupStages {

  @IsNumber()
  @ApiProperty({type: Number, required: true})
  team_one_id: number;

  @IsNumber()
  @ApiProperty({type: Number, required: true})
  team_two_id: number;

}