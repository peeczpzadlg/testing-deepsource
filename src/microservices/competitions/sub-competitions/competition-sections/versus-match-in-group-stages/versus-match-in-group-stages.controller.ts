import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { getHeaderAccessToken, getMicroserviceResponse } from 'src/shared/utils/helper.utils';
import { JwtAuthGuard } from '../../../../../auth/jwt-auth.guard';
import { VersusMatchCreateOrUpdateDto } from '../versus-match/dtos/versus-match-create.dto';
import { VersusMatchGroupStageSearchDto } from './dtos/versus-match-in-group-stages-search.dto';
import { IQueryParam } from './versus-match-in-group-stages.interface';
import { VersusMatchInGroupStagesService } from './versus-match-in-group-stages.service';

@ApiTags('Versus Match In Group-stages')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class VersusMatchInGroupStagesController {
    constructor(
        private versusMatchInGroupStageService: VersusMatchInGroupStagesService
    ) {}

    @Get('competition-sections/:competitionSectionId/group-stages/:groupStageId/stadium-holder')
    async getVersusMatchGroupStageStadiumHolder(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
        @Query() query: VersusMatchGroupStageSearchDto,
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.getVersusMatchGroupStageStadiumHolder(params, query, token))
    }

    @Get('competition-sections/:competitionSectionId/group-stages/:groupStageId/get-team-match')
    async getVersusMatchGroupStageTeamMatch(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
        @Query() query: VersusMatchGroupStageSearchDto,
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.getVersusMatchGroupStageTeamMatch(params, query, token))
    }

    @Get('competition-sections/:competitionSectionId/group-stages/:groupStageId/versus-match')
    async getVersusMatchGroupStageVersusMatch(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.getVersusMatchGroupStageVersusMatch(params, token))
    }

    @Get('competition-sections/:competitionSectionId/group-stages/:groupStageId/versus-match/:versusMatchId')
    async getVersusMatchGroupStageVersusMatchById(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
        @Param('versusMatchId') versusMatchId: number,
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId,
            versusMatchId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.getVersusMatchGroupStageVersusMatchById(params, token))
    }

    @Post('competition-sections/:competitionSectionId/group-stages/:groupStageId/versus-match/create')
    @ApiBody({ type: VersusMatchCreateOrUpdateDto })
    async createGroupStageVersusMatch(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
        @Body() body: VersusMatchCreateOrUpdateDto
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.createGroupStageVersusMatch(params, body, token))
    }

    @Post('competition-sections/:competitionSectionId/group-stages/:groupStageId/versus-match/:versusMatchId/update')
    @ApiBody({ type: VersusMatchCreateOrUpdateDto })
    async updateVersusMatch(
        @Req() req: Request, 
        @Param('competitionSectionId') competitionSectionId: number,
        @Param('groupStageId') groupStageId: number,
        @Param('versusMatchId') versusMatchId: number,
        @Body() body: VersusMatchCreateOrUpdateDto
    ) {
        const params: IQueryParam = {
            competitionSectionId,
            groupStageId,
            versusMatchId
        }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchInGroupStageService.updateGroupStageVersusMatch(params, body, token))
    }
}
