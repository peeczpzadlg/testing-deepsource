export interface ISearchVersusMatchInGroupStages {
    team_one_id: number
    team_two_id: number
}

export interface IQueryParam {
    competitionSectionId?: number;
    groupStageId?: number;
    versusMatchId?: number;
}