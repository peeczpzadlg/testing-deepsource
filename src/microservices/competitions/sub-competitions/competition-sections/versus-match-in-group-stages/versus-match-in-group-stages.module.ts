import { Module } from '@nestjs/common';
import { VersusMatchInGroupStagesController } from './versus-match-in-group-stages.controller';
import { VersusMatchInGroupStagesService } from './versus-match-in-group-stages.service';

@Module({
  controllers: [VersusMatchInGroupStagesController],
  providers: [VersusMatchInGroupStagesService]
})
export class VersusMatchInGroupStagesModule {}
