import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';
import { ISaveVersusMatch } from '../versus-match/versus-match.interface';
import { VersusMatchService } from '../versus-match/versus-match.service';
import { ISearchVersusMatchInGroupStages, IQueryParam } from './versus-match-in-group-stages.interface';

@Injectable()
export class VersusMatchInGroupStagesService {

    private microservice: AxiosInstance;
    private logger: Logger = new Logger(VersusMatchService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getVersusMatchGroupStageStadiumHolder(query: IQueryParam, params: ISearchVersusMatchInGroupStages, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/stadium-holder`,
              {
                headers: {
                  Authorization: accessToken
              },
              params
            }))
          } catch(err) {
            this.logger.error(err);
            throw err;
          } 
    }

    async getVersusMatchGroupStageTeamMatch(query: IQueryParam, params: ISearchVersusMatchInGroupStages, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/get-team-match`,
              {
                headers: {
                  Authorization: accessToken
              },
              params
            }))
          } catch(err) {
            this.logger.error(err);
            throw err;
          } 
    }

    async getVersusMatchGroupStageVersusMatch(query: IQueryParam, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/versus-match`,
              {
                headers: {
                  Authorization: accessToken
              }
            }))
          } catch(err) {
            this.logger.error(err);
            throw err;
          } 
    }

    async getVersusMatchGroupStageVersusMatchById(query: IQueryParam, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/versus-match/${query.versusMatchId}`,
              {
                headers: {
                  Authorization: accessToken
              }
            }))
          } catch(err) {
            this.logger.error(err);
            throw err;
          } 
    }

    async createGroupStageVersusMatch(query: IQueryParam, body: ISaveVersusMatch, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/versus-match/create`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updateGroupStageVersusMatch(query: IQueryParam, body: ISaveVersusMatch, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`competition-sections/${query.competitionSectionId}/group-stages/${query.groupStageId}/versus-match/${query.versusMatchId}/update`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
}
