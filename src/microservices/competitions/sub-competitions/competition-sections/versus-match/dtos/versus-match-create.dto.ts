import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsNumber, IsObject, IsString } from "class-validator";
import { ISaveVersusMatch } from "../versus-match.interface";
import { VersusMatchRefereeDto } from "./versus-match-referee.dto";
import { VersusMatchStadiumHolderDto } from "./versus-match-stadium-holder.dto";
import { VersusMatchTeamDetailDto } from "./versus-match-team-dto";

export class VersusMatchCreateOrUpdateDto implements ISaveVersusMatch {
    @IsDate()
    @ApiProperty({ type: Date, required: true})
    versus_match_date: Date;

    @ApiProperty({ type: String, required: false})
    versus_match_start_at: string;

    @ApiProperty({ type: String, required: false})
    versus_match_end_at: string;

    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    setting_stadiums: number;

    @IsObject()
    @ApiProperty({ type: VersusMatchStadiumHolderDto, required: true})
    versus_match_stadium_holder: VersusMatchStadiumHolderDto;

    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    versus_match_visitors: number;

    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeDto, required: false})
    versus_match_referee: VersusMatchRefereeDto;

    @ApiProperty({ type: VersusMatchTeamDetailDto, required: false})
    versus_match_team_left: VersusMatchTeamDetailDto;

    @ApiProperty({ type: VersusMatchTeamDetailDto, required: false})
    versus_match_team_right: VersusMatchTeamDetailDto;

    created_by?: string;

    updated_by?: string;
}

