import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VersusMatchRefereeStaffDto {
    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    refereeId: number;

    @IsString()
    @ApiProperty({ type: String, required: true})
    refereeName: string;

    @IsString()
    @ApiProperty({ type: String, required: false})
    refereeImage: string;
}