import { IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { VersusMatchRefereeStaffDto } from './versus-match-referee-staff.dto';

export class VersusMatchRefereeDto {
 
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_one: VersusMatchRefereeStaffDto;
  
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_two: VersusMatchRefereeStaffDto;
  
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_three: VersusMatchRefereeStaffDto;
  
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_four: VersusMatchRefereeStaffDto;
  
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_five: VersusMatchRefereeStaffDto;
  
    @IsObject()
    @ApiProperty({ type: VersusMatchRefereeStaffDto, required: false})
    versus_match_referee_six: VersusMatchRefereeStaffDto;
  
}