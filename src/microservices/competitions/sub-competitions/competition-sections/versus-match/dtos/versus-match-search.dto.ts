import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";
import { SearchPageDto } from "../../../../../../microservices/microservices.dto";
import { ISearchVersusMatch } from "../versus-match.interface";

export class SearchVersusMatchDto implements ISearchVersusMatch {
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    name?: string;
}

export class SearchListVersusMatchDto extends SearchPageDto {
  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active?: string;
}