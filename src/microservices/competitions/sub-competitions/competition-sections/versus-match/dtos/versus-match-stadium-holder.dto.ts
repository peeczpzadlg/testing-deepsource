import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsString } from "class-validator";

export class VersusMatchStadiumHolderDto {

    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    id: number;
  
    @IsString()
    @ApiProperty({ type: String, required: true})
    image: string;
  
    @IsString()
    @ApiProperty({ type: String, required: false})
    name_th: string;
  
    @IsString()
    @ApiProperty({ type: String, required: false})
    name_en: string;
  
  }