import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VersusMatchDetailPlayerDto {
    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    accountId: number;

    @IsString()
    @ApiProperty({ type: String, required: true})
    accountName: string;

    @IsString()
    @ApiProperty({ type: String, required: false})
    accountPosition: string;

    @IsNumber()
    @ApiProperty({ type: Number, required: false})
    accountShirtNumber: number;

    @ApiProperty({ type: String, required: false})
    accountImage: string;

    @ApiProperty({ type: Boolean, required: false})
    captain: boolean;

    @ApiProperty({ type: Boolean, required: false})
    viceCaptain: boolean;

    @ApiProperty({ type: Boolean, required: false})
    main: boolean;

}