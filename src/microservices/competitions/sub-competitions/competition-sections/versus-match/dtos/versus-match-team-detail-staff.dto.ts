import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VersusMatchDetailStaffDto {

  @IsNumber()
  @ApiProperty({ type: Number, required: true})
  staffId: number;

  @IsString()
  @ApiProperty({ type: String, required: true})
  staffName: string;

  @IsString()
  @ApiProperty({ type: String, required: false})
  staffImage: string;

}