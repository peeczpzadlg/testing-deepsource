import { IsString, IsNumber } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { VersusMatchDetailStaffDto } from './versus-match-team-detail-staff.dto';
import { VersusMatchDetailPlayerDto } from './versus-match-team-detail-player.dto';

export class VersusMatchTeamDetailDto {

    @IsNumber()
    @ApiProperty({ type: Number, required: true})
    versus_match_team_id: number;

    @IsString()
    @ApiProperty({ type: String, required: true})
    versus_match_team_name: string;

    @ApiProperty({ type: String, required: false})
    versus_match_team_image: string;

    @ApiProperty({ type: Number, required: false})
    versus_match_score: number;

    @ApiPropertyOptional({ type: VersusMatchDetailStaffDto, required: false})
    versus_match_team_detail_staff: VersusMatchDetailStaffDto;

    @ApiPropertyOptional({ type: [VersusMatchDetailPlayerDto], required: false})
    versus_match_team_detail_player: VersusMatchDetailPlayerDto;
}