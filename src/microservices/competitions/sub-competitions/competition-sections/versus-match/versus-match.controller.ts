import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import {
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../../../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../../../../auth/jwt-auth.guard';
import { VersusMatchService } from './versus-match.service';
import {
  SearchListVersusMatchDto,
  SearchVersusMatchDto,
} from './dtos/versus-match-search.dto';
import { VersusMatchCreateOrUpdateDto } from './dtos/versus-match-create.dto';

@ApiTags('Versus Match')
@Controller()
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class VersusMatchController {
  constructor(private versusMatchService: VersusMatchService) {}

  @Get('competition-sections/:competitionSectionId/versus-match/stadium')
  async getListVersusMatchStadium(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Query() query: SearchVersusMatchDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getListVersusMatchStadium(
        competitionSectionId,
        query,
        token,
      ),
    );
  }

  @Get('competition-sections/:competitionSectionId/versus-match/stadium-holder')
  async getVersusMatchStadiumHolder(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getVersusMatchStadiumHolder(
        competitionSectionId,
        token,
      ),
    );
  }

  @Get('competition-sections/:competitionSectionId/versus-match/referee')
  async getListVersusMatchReferee(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Query() query: SearchVersusMatchDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getListVersusMatchReferee(
        competitionSectionId,
        query,
        token,
      ),
    );
  }

  @Get(
    'competition-sections/:competitionSectionId/versus-match/teams/:teamId/players',
  )
  async getVersusMatchTeamPlayer(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Param('teamId') teamId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getVersusMatchTeamPlayer(
        competitionSectionId,
        teamId,
        token,
      ),
    );
  }

  @Get(
    'competition-sections/:competitionSectionId/versus-match/teams/:teamId/staff',
  )
  async getVersusMatchTeamStaff(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Param('teamId') teamId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getVersusMatchTeamStaff(
        competitionSectionId,
        teamId,
        token,
      ),
    );
  }

  @Get('competition-sections/:competitionSectionId/versus-match')
  async getListCompetitionSectionVersusMatch(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Query() query: SearchListVersusMatchDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getListCompetitionSectionVersusMatch(
        competitionSectionId,
        query,
        token,
      ),
    );
  }

  @Get('competition-sections/:competitionSectionId/versus-match/:versusMatchId')
  async getVersusMatchById(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Param('versusMatchId') versusMatchId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.getVersusMatchById(
        competitionSectionId,
        versusMatchId,
        token,
      ),
    );
  }

  @Post('competition-sections/:competitionSectionId/versus-match/create')
  @ApiBody({ type: VersusMatchCreateOrUpdateDto })
  async createVersusMatch(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Body() body: VersusMatchCreateOrUpdateDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.createVersusMatch(
        competitionSectionId,
        body,
        token,
      ),
    );
  }

  @Post(
    'competition-sections/:competitionSectionId/versus-match/:versusMatchId/update',
  )
  @ApiBody({ type: VersusMatchCreateOrUpdateDto })
  async updateVersusMatch(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Param('versusMatchId') versusMatchId: number,
    @Body() body: VersusMatchCreateOrUpdateDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.updateVersusMatch(
        competitionSectionId,
        versusMatchId,
        body,
        token,
      ),
    );
  }

  @Post(
    'competition-sections/:competitionSectionId/versus-match/:versusMatchId/delete',
  )
  async deleteCompetitionSection(
    @Req() req: Request,
    @Param('competitionSectionId') competitionSectionId: number,
    @Param('versusMatchId') versusMatchId: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.versusMatchService.deleteVersusMatch(
        competitionSectionId,
        versusMatchId,
        token,
      ),
    );
  }
}
