import { IPaginateQuery } from "../../../../../shared/interfaces/api-interfaces";

export interface ISearchVersusMatch {
    name?: string;
}

export interface ISearchListVersusMatch extends IPaginateQuery {
    active?: string;
}

export interface ISaveVersusMatch {
    versus_match_date: Date;
    versus_match_start_at: string;
    versus_match_end_at: string;
    setting_stadiums: number;
    versus_match_stadium_holder: IVersusMatchStadiumHolder;
    versus_match_visitors: number;
    versus_match_referee: IVersusMatchReferee;
    versus_match_team_left: IVersusMatchTeamDetail;
    versus_match_team_right: IVersusMatchTeamDetail;
    created_by?: string;
}

interface IVersusMatchStadiumHolder {
    id: number;
    image: string;
    name_th?: string;
    name_en?: string
}

interface IVersusMatchReferee {
    versus_match_referee_one: TStaffReferee
    versus_match_referee_two: TStaffReferee
    versus_match_referee_three: TStaffReferee
    versus_match_referee_four: TStaffReferee
    versus_match_referee_five: TStaffReferee
    versus_match_referee_six: TStaffReferee
}

interface IVersusMatchTeamDetail {
    versus_match_team_id: number;
    versus_match_team_name: string;
    versus_match_team_image: string;
    versus_match_score: number;
    versus_match_team_detail_staff: TStaff;
    versus_match_team_detail_player: TPlayer;
}

type TStaffReferee = {
    refereeId: number;
    refereeName: string;
    refereeImage: string;
}

interface TStaff {
    staffId: number;
    staffName: string;
    staffImage: string;
}

interface TPlayer {
    accountId: number;
    accountName: string;
    accountPosition: string;
    accountShirtNumber: number;
    accountImage: string;
    captain: boolean;
    viceCaptain: boolean;
    main: boolean;
}