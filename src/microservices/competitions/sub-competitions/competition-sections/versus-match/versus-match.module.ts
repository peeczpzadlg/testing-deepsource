import { forwardRef, Module } from '@nestjs/common';
import { AccountPositionsModule } from 'src/personals/account-positions/account-positions.module';
import { VersusMatchController } from './versus-match.controller';
import { VersusMatchService } from './versus-match.service';

@Module({
  controllers: [VersusMatchController],
  imports: [
    forwardRef(() => AccountPositionsModule)
  ],
  providers: [VersusMatchService]
})
export class VersusMatchModule {}
