import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { AccountPositionsService } from 'src/personals/account-positions/account-positions.service';
import { EPersonnelPositionType } from 'src/shared/enum/position-type';
import { getResponse } from '../../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';
import {
  ISaveVersusMatch,
  ISearchListVersusMatch,
  ISearchVersusMatch,
} from './versus-match.interface';

@Injectable()
export class VersusMatchService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(VersusMatchService.name);

  constructor(private accountPositionService: AccountPositionsService) {
    this.microservice = microserviceAxios();
  }

  async getListVersusMatchStadium(
    competitionSectionId: number,
    params: ISearchVersusMatch,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/stadium`,
          {
            headers: {
              Authorization: accessToken,
            },
            params,
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getVersusMatchStadiumHolder(
    competitionSectionId: number,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/stadium-holder`,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getListVersusMatchReferee(
    competitionSectionId: number,
    params: ISearchVersusMatch,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/referee`,
          {
            headers: {
              Authorization: accessToken,
            },
            params,
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getVersusMatchTeamPlayer(
    competitionSectionId: number,
    teamId: number,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/teams/${teamId}/players`,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      ).then(async res => {
        try {
          const mapTeamPlayer = res.data?.team_player?.map(async ele => {
            if (!ele.accountPosition) return ele;
            try {
              const accountPosition = await this.accountPositionService.findPositionType(
                EPersonnelPositionType.Player,
                ele.accountPosition,
              );
              return {
                ...ele,
                accountPosition,
              };
            } catch(err) {
              if (err.error_code === HttpStatus.NOT_FOUND) {
                return ele
              }
              throw err;
            }
          });
          const team_player = await Promise.all(mapTeamPlayer);
          const result = {
            ...res,
            data: {
              team_player,
            },
          };
          return result;
        } catch (err) {
          throw err;
        }
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getVersusMatchTeamStaff(
    competitionSectionId: number,
    teamId: number,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/teams/${teamId}/staff`,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getListCompetitionSectionVersusMatch(
    competitionSectionId: number,
    params: ISearchListVersusMatch,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match`,
          {
            headers: {
              Authorization: accessToken,
            },
            params,
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getVersusMatchById(
    competitionSectionId: number,
    versusMatchId: number,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get(
          `competition-sections/${competitionSectionId}/versus-match/${versusMatchId}`,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async createVersusMatch(
    params: number,
    body: ISaveVersusMatch,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.post(
          `competition-sections/${params}/versus-match/create`,
          body,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async updateVersusMatch(
    competitionSectionId: number,
    versusMatchId: number,
    body: ISaveVersusMatch,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.post(
          `competition-sections/${competitionSectionId}/versus-match/${versusMatchId}/update`,
          body,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deleteVersusMatch(
    competitionSectionId: number,
    versusMatchId: number,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.post(
          `competition-sections/${competitionSectionId}/versus-match/${versusMatchId}/delete`,
          null,
          {
            headers: {
              Authorization: accessToken,
            },
          },
        ),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
