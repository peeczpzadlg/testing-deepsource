import { IsString, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SettingSubCompetitionAwardsTeamDto {

  @IsNumber()
  @ApiProperty({ type: Number, required: true})
  team_id: number;

  @IsString()
  @ApiProperty({ type: String, required: true})
  team_name: string;

  @IsString()
  @ApiProperty({ type: String, required: true})
  team_image_url: string;
}