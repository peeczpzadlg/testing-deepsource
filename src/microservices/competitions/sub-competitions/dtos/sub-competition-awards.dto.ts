import { ApiProperty } from "@nestjs/swagger";
import { IsObject } from "class-validator";
import { SettingSubCompetitionAwardsTeamDto } from "./sub-competition-award-team.dto";

export class SettingSubCompetitionAwardsCreateDto {

    @IsObject()
    @ApiProperty({ type: SettingSubCompetitionAwardsTeamDto, required: false})
    bronze: SettingSubCompetitionAwardsTeamDto;
  
    @IsObject()
    @ApiProperty({ type: SettingSubCompetitionAwardsTeamDto, required: false})
    silver: SettingSubCompetitionAwardsTeamDto;
  
    @IsObject()
    @ApiProperty({ type: SettingSubCompetitionAwardsTeamDto, required: false})
    gold: SettingSubCompetitionAwardsTeamDto;
    
  }