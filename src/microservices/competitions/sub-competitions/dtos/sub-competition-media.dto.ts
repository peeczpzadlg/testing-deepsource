import { ApiProperty } from "@nestjs/swagger";
import { IsUrl } from "class-validator";

export class PostVideoUrlDto {
    @ApiProperty()
    @IsUrl()
    setting_sub_competition_video_url: string;
}