import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsNumber, IsObject, IsOptional, IsString } from "class-validator";
import { ECompetitionLevel } from "../../../../shared/enum";
import { ISaveSubCompetition, IUpdateSubCompetition, IUpdateSubCompetitionPublished } from "../sub-competitions.interface";
import { SettingSubCompetitionAwardsCreateDto } from "./sub-competition-awards.dto";

export class SaveSubCompetitionDto implements ISaveSubCompetition {
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_name_th: string;
  
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_name_en: string;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_abbreviation: string;
  
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_year: string;
  
    @ApiProperty({ type: Date, required: true })
    @IsDate()
    setting_sub_competition_start_at: Date;

    @ApiProperty({ type: Date, required: true })
    @IsDate()
    setting_sub_competition_end_at: Date;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_hostname: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_hostname_en: string;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_cohostname: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_cohostname_en: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_type: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_category: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_category_en: string;

    @ApiPropertyOptional({ type: SettingSubCompetitionAwardsCreateDto, required: false })
    @IsOptional()
    @IsObject()
    setting_sub_competition_awards: SettingSubCompetitionAwardsCreateDto;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_history_th: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_history_en: string;

    @ApiPropertyOptional({ type: Number, required: false })
    @IsOptional()
    @IsNumber()
    global_config_sport_categories: number;
}

export class UpdateSubCompetitionDto implements IUpdateSubCompetition {
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_name_th: string;
  
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_name_en: string;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_abbreviation: string;
  
    @ApiProperty({ type: String, required: true })
    @IsString()
    setting_sub_competition_year: string;
  
    @ApiProperty({ type: Date, required: true })
    @IsDate()
    setting_sub_competition_start_at: Date;

    @ApiProperty({ type: Date, required: true })
    @IsDate()
    setting_sub_competition_end_at: Date;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_hostname: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_hostname_en: string;
  
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_cohostname: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_cohostname_en: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_type: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_category: string;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_category_en: string;

    @ApiPropertyOptional({ type: SettingSubCompetitionAwardsCreateDto, required: false })
    @IsOptional()
    @IsObject()
    setting_sub_competition_awards: SettingSubCompetitionAwardsCreateDto;
    
    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_history_th: string;

    @ApiPropertyOptional({ type: String, required: false })
    @IsOptional()
    @IsString()
    setting_sub_competition_history_en: string;

    @ApiPropertyOptional({ type: Number, required: false })
    @IsOptional()
    @IsNumber()
    global_config_sport_categories: number;

    @ApiPropertyOptional({ type: Boolean, required: false })
    @IsOptional()
    @IsBoolean()
    active: boolean;
}

export class UpdateSubCompetitionPublishDto implements IUpdateSubCompetitionPublished{
    @ApiPropertyOptional({ type: Boolean, default:false })
    @IsBoolean()
    @IsOptional()
    setting_sub_competition_published?: boolean;
  }