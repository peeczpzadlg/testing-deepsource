import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";
import { ISearchTeamChoose } from "../setting-teams-sub-competition.interface";

export class SearchTeamChooseDto implements ISearchTeamChoose {
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    name?: string;
  }