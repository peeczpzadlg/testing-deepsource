import { ApiProperty } from '@nestjs/swagger';
import { ISaveTeamInSubCompetition } from '../../sub-competitions.interface';

export class SettingTeamInSubCompetitionCreateDto implements ISaveTeamInSubCompetition {
  @ApiProperty({ type: [Number] })
  team_id: number[];
}