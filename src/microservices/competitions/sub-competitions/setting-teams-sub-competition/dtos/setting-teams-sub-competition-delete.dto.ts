import { ApiProperty } from '@nestjs/swagger';
import { IDeleteMultipleTeamInSubCompetition } from '../../sub-competitions.interface';

export class DeleteMultipleTeamInSubCompetitionCreateDto implements IDeleteMultipleTeamInSubCompetition {
  @ApiProperty({ type: [Number] })
  setting_team_in_sub_competition_id: number[];
}