import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchPaginateDto } from '../../../../microservices/microservices.dto';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { SettingTeamsSubCompetitionService } from './setting-teams-sub-competition.service';
import { SearchTeamChooseDto } from './dtos/setting-teams-search.dto';
import { SettingTeamInSubCompetitionCreateDto } from './dtos/setting-teams-sub-competition-create.dto';
import { DeleteMultipleTeamInSubCompetitionCreateDto } from './dtos/setting-teams-sub-competition-delete.dto';

@ApiTags('Setting Teams in Sub Competition')
@Controller()
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class SettingTeamsSubCompetitionController {
    constructor(
        private settingTeamsSubCompetitionService: SettingTeamsSubCompetitionService,
        // private mainCompetitionSocialContact: MainCompetitionSocialContactService
    ) {}

    @Get('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/all')
    async getListTeamsInSubCompetitionById(@Req() req: Request, @Param('subCompetitionId') subCompetitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.getListTeamsInSubCompetitionById(subCompetitionId, token))
    }

    @Get('sub-competitions/:subCompetitionId/setting-team-in-sub-competition')
    async getTeamsInSubCompetition(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchPaginateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.getTeamsInSubCompetition(subCompetitionId, query, token))
    }

    @Get('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/choose/teams')
    async getChooseTeamsInSubCompetitionById(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchTeamChooseDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.getChooseTeamsInSubCompetitionById(subCompetitionId, query, token))
    }

    @Get('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/choose/teams/club')
    async getChooseTeamsClubInSubCompetitionById(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchTeamChooseDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.getChooseTeamsClubInSubCompetitionById(subCompetitionId, query, token))
    }

    @Get('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/choose/teams/national')
    async getChooseTeamsNationalInSubCompetitionById(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchTeamChooseDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.getChooseTeamsNationalInSubCompetitionById(subCompetitionId, query, token))
    }

    @Post('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/create')
    @ApiBody({ required: true, type: SettingTeamInSubCompetitionCreateDto })
    async createTeamsInSubCompetition(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Body() body: SettingTeamInSubCompetitionCreateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.createTeamsInSubCompetition(subCompetitionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/multi/delete')
    @ApiBody({ required: true, type: DeleteMultipleTeamInSubCompetitionCreateDto })
    async deleteMultipleTeamsInSubCompetition(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Body() body: DeleteMultipleTeamInSubCompetitionCreateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.deleteMultipleTeamsInSubCompetition(subCompetitionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/setting-team-in-sub-competition/:settingTeamInSubCompetitionId/delete')
    async deleteTeamsInSubCompetition(
        @Req() req: Request, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('settingTeamInSubCompetitionId') settingTeamInSubCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.settingTeamsSubCompetitionService.deleteTeamsInSubCompetition(subCompetitionId, settingTeamInSubCompetitionId, token));
    }
}