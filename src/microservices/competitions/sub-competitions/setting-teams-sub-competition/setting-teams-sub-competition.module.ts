import { Module } from '@nestjs/common';
import { SettingTeamsSubCompetitionController } from './setting-teams-sub-competition.controller';
import { SettingTeamsSubCompetitionService } from './setting-teams-sub-competition.service';

@Module({
  controllers: [SettingTeamsSubCompetitionController],
  providers: [SettingTeamsSubCompetitionService]
})
export class SettingTeamsSubCompetitionModule {}
