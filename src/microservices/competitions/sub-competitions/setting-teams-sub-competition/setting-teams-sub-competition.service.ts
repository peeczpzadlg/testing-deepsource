import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../../../../microservices/microservices.interface';
import { ISearchTeamChoose } from './setting-teams-sub-competition.interface';
import { IDeleteMultipleTeamInSubCompetition, ISaveTeamInSubCompetition } from '../sub-competitions.interface';

@Injectable()
export class SettingTeamsSubCompetitionService {

    private microservice: AxiosInstance;
    private logger: Logger = new Logger(SettingTeamsSubCompetitionService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getTeamsInSubCompetition(subCompetitionId: number, params: ISearchPaginate, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition`,
            {
              headers: {
                Authorization: accessToken
            },
            params
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getListTeamsInSubCompetitionById(params: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${params}/setting-team-in-sub-competition/all`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getChooseTeamsInSubCompetitionById(subCompetitionId: number, params: ISearchTeamChoose, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/choose/teams`,
            {
              headers: {
                Authorization: accessToken
            },
            params
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getChooseTeamsClubInSubCompetitionById(subCompetitionId: number, params: ISearchTeamChoose, accessToken: string) {
      try {
        return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/choose/teams/club`,
          {
            headers: {
              Authorization: accessToken
          },
          params
        }))
      } catch(err) {
        this.logger.error(err);
        throw err;
      } 
  }

  async getChooseTeamsNationalInSubCompetitionById(subCompetitionId: number, params: ISearchTeamChoose, accessToken: string) {
    try {
      return await getResponse(this.microservice.get(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/choose/teams/national`,
        {
          headers: {
            Authorization: accessToken
        },
        params
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    } 
}

    async createTeamsInSubCompetition(subCompetitionId: number, body: ISaveTeamInSubCompetition, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/create`, body,
          {
            headers: {
              Authorization: accessToken
          }
        }));
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

    async deleteMultipleTeamsInSubCompetition(subCompetitionId: number, body: IDeleteMultipleTeamInSubCompetition, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/multi/delete`, body,
          {
            headers: {
              Authorization: accessToken
          }
        }));
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

    async deleteTeamsInSubCompetition(subCompetitionId: number, settingTeamInSubCompetitionId: number, accessToken: string) {
      try {
        return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/setting-team-in-sub-competition/${settingTeamInSubCompetitionId}/delete`, null,
          {
            headers: {
              Authorization: accessToken
          }
        }));
      } catch(err) {
        this.logger.error(err);
        throw err
      }
    }

}
