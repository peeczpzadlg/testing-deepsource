import { Module } from '@nestjs/common';
import { SubCompetitionSocialContactService } from './sub-competition-social-contact.service';

@Module({
  providers: [SubCompetitionSocialContactService],
  exports: [SubCompetitionSocialContactService]
})
export class SubCompetitionSocialContactModule {}
