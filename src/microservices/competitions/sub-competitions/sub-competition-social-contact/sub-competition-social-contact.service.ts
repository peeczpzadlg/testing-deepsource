import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { IUpdateSubCompetitionSocialContact } from '../sub-competitions.interface';

@Injectable()
export class SubCompetitionSocialContactService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(SubCompetitionSocialContactService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getSubCompetitionSocialContact(params: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`sub-competitions/${params}/social-contacts`,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err;
        }
    }

    async createSubCompetitionSocialContact(subCompetitionId: number, body: IUpdateSubCompetitionSocialContact, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/social-contacts`, body,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async deleteSubCompetitionSocialContact(subCompetitionId: number, socialContactId: number, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`sub-competitions/${subCompetitionId}/social-contacts/${socialContactId}/delete`, null,
            {
              headers: {
                Authorization: accessToken
            }
          }))
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }
}
