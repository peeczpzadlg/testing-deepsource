import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { query, Request } from 'express';
import { ApiFile } from '../../../shared/utils/file.utils';
import { SearchPageDto, SearchPaginateActiveDto } from '../../../microservices/microservices.dto';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../../auth/jwt-auth.guard';
import FormData from 'form-data';
import { SubCompetitionsService } from './sub-competitions.service';
import { SaveSubCompetitionDto, UpdateSubCompetitionDto, UpdateSubCompetitionPublishDto } from './dtos/sub-competition.dto';
import { SubCompetitionSocialContactService } from './sub-competition-social-contact/sub-competition-social-contact.service';
import { UpdateSubCompetitionSocialContactDto } from './dtos/sub-competition-social-contact.dto';
import { EFileType } from 'src/microservices/media/media.enum';
import { PostVideoUrlDto } from './dtos/sub-competition-media.dto';
import { AllSubCompeteitionDto } from './dtos/all-sub-competition.dto';

@ApiTags('Setting Sub Competitions')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller()
export class SubCompetitionsController {
    constructor(
        private subCompetitionsService: SubCompetitionsService,
        private subCompetitionSocialContact: SubCompetitionSocialContactService
    ) {}

    @Get('competitions/find-all/sub-competition')
    async findAllSubCompete(@Query() query: AllSubCompeteitionDto, @Req() req: Request) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.findAllSubCompete(query, token))
    }

    @Get('competitions/:competitionId/sub-competitions')
    async getSubCompetition(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Query() query: SearchPaginateActiveDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.getSubCompetition(competitionId, query, token))
    }

    @Get('competitions/:competitionId/sub-competitions/:subCompetitionId')
    async getSubCompetitionById(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number,
        @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.getSubCompetitionById(competitionId, subCompetitionId, token))
    }
    
    @Post('competitions/:competitionId/sub-competitions/create')
    @ApiBody({ required: true, type: SaveSubCompetitionDto })
    async createMainCompetition(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Body() body: SaveSubCompetitionDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.createSubCompetition(competitionId, body, token));
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/update')
    async updateMainCompetition(
        @Req() req: Request,
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number, 
        @Body() body: UpdateSubCompetitionDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.updateSubCompetition(competitionId, subCompetitionId, body, token));
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/delete')
    async deleteSubCompetition(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number,
        @Param('subCompetitionId') subCompetitionId: number,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.deleteSubCompetition(competitionId, subCompetitionId, token));
    }

    @Get('sub-competitions/:subCompetitionId/social-contacts')
    async getSubCompetitionSocialContact(@Req() req: Request, @Param('subCompetitionId') subCompetitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionSocialContact.getSubCompetitionSocialContact(subCompetitionId, token));
    }
    
    @Post('sub-competitions/:subCompetitionId/social-contacts')
    @ApiBody({ required: false, type: UpdateSubCompetitionSocialContactDto })
    async createSubCompetitionSocialContact(
        @Req() req: Request,
        @Param('subCompetitionId') subCompetitionId: number, 
        @Body() body: UpdateSubCompetitionSocialContactDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionSocialContact.createSubCompetitionSocialContact(subCompetitionId, body, token));
    }

    @Post('sub-competitions/:subCompetitionId/social-contacts/:socialContactId/delete')
    async deleteSubCompetitionSocialContact(
        @Req() req: Request,
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('socialContactId') socialContactId: number,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionSocialContact.deleteSubCompetitionSocialContact(subCompetitionId, socialContactId, token));
    }

    @Get('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/profile')
    async getSubCompetitionProfileImage(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number,
        @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.getProfileImage(EFileType.PROFILE, competitionId, subCompetitionId, token))
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/profile')
    @ApiConsumes('multipart/form-data')
    @ApiFile()
    @UseInterceptors(FileInterceptor('file'))
    async uploadSubCompetitionProfileImage(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number,
        @UploadedFile() file: Express.Multer.File
    ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.subCompetitionsService.uploadProfileImage(EFileType.PROFILE, competitionId, subCompetitionId, body, token));
    }

    @Get('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/images')
    async getSubCompetitionMediaImages(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number,
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchPageDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.getMediaImages(EFileType.IMAGE, competitionId, subCompetitionId, query, token))
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/images')
    @ApiConsumes('multipart/form-data')
    @ApiFile()
    @UseInterceptors(FileInterceptor('file'))
    async uploadSubCompetitionMediaImages(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number,
        @UploadedFile() file: Express.Multer.File
    ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.subCompetitionsService.uploadMediaImages(EFileType.IMAGE, competitionId, subCompetitionId, body, token));
    }

    @Get('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/videos')
    async getSubCompetitionMediaVideos(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number,
        @Param('subCompetitionId') subCompetitionId: number, 
        @Query() query: SearchPageDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.getMediaVideos(EFileType.VIDEO, competitionId, subCompetitionId, query, token))
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/videos')
    @ApiConsumes('multipart/form-data')
    @ApiFile()
    @UseInterceptors(FileInterceptor('file'))
    async uploadSubCompetitionMediaVideos(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number,
        @UploadedFile() file: Express.Multer.File
    ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.subCompetitionsService.uploadMediaVideos(EFileType.VIDEO, competitionId, subCompetitionId, body, token));
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/videos-url')
    async createSubCompetitionMediaVideoUrl(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Body() body: PostVideoUrlDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.uploadMediaVideoUrl(competitionId, subCompetitionId, body, token));
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/medias/:mediaId/delete')
    async deleteSubCompetitionMedias(
        @Req() req: Request, 
        @Param('competitionId') competitionId: number, 
        @Param('subCompetitionId') subCompetitionId: number,
        @Param('mediaId') mediaId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.deleteMedias(competitionId, subCompetitionId, mediaId, token));
    }

    @Post('competitions/:competitionId/sub-competitions/:subCompetitionId/published/update')
    @ApiBody({ required: true, type: UpdateSubCompetitionPublishDto })
    async updateTeamPublished(@Req() req: Request, @Param('competitionId') competitionId: number,@Param('subCompetitionId') subCompetitionId: number,@Body() body: UpdateSubCompetitionPublishDto) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionsService.updatePublished(competitionId,subCompetitionId,body,token));
    }

}

