import { IMicroservicePaginateQuery } from "../../../shared/interfaces/api-interfaces";

export interface ISearchMainCompetition extends IMicroservicePaginateQuery {
    name?: string;
    active?: string;
}

export interface IFindAllSubCompeteQuery {
    active?: boolean;
    name?: string;
    standard_types?: string;
}

export interface ISaveSubCompetition {
    setting_sub_competition_name_th: string
    setting_sub_competition_name_en: string
    setting_sub_competition_abbreviation: string
    setting_sub_competition_year: string
    setting_sub_competition_start_at: Date
    setting_sub_competition_end_at: Date
    setting_sub_competition_hostname: string
    setting_sub_competition_hostname_en: string
    setting_sub_competition_cohostname: string
    setting_sub_competition_cohostname_en: string
    setting_sub_competition_type: string
    setting_sub_competition_category: string
    setting_sub_competition_category_en: string
    setting_sub_competition_awards: ISubCompetitionAward
    setting_sub_competition_history_th: string
    setting_sub_competition_history_en: string
    global_config_sport_categories: number
}

export interface IUpdateSubCompetition {
    setting_sub_competition_name_th: string
    setting_sub_competition_name_en: string
    setting_sub_competition_abbreviation: string
    setting_sub_competition_year: string
    setting_sub_competition_start_at: Date
    setting_sub_competition_end_at: Date
    setting_sub_competition_hostname: string
    setting_sub_competition_hostname_en: string
    setting_sub_competition_cohostname: string
    setting_sub_competition_cohostname_en: string
    setting_sub_competition_type: string
    setting_sub_competition_category: string
    setting_sub_competition_category_en: string
    setting_sub_competition_awards: ISubCompetitionAward
    setting_sub_competition_history_th: string
    setting_sub_competition_history_en: string
    global_config_sport_categories: number
    active: boolean
}

export interface IUpdateSubCompetitionSocialContact {
    setting_sub_competition_social_contact_phone?: string,
    setting_sub_competition_social_contact_email?: string,
    setting_sub_competition_social_contact_youtube?: string,
    setting_sub_competition_social_contact_facebook?: string,
    setting_sub_competition_social_contact_line?: string,
    setting_sub_competition_social_contact_instagram?: string,
    setting_sub_competition_social_contact_twitter?: string,
    setting_sub_competition_social_contact_website?: string,
    setting_sub_competition_social_contact_fax?: string
}

export interface ISubCompetitionAward {
    bronze: IAwardTeam
    silver: IAwardTeam
    gold: IAwardTeam
}

interface IAwardTeam {
    team_id: number;
    team_name: string;
    team_image_url: string;
}

export interface ISaveMediaVideoUrl {
    setting_sub_competition_video_url: string;
}

export interface ISaveTeamInSubCompetition {
    team_id: number[]
}

export interface IDeleteMultipleTeamInSubCompetition {
    setting_team_in_sub_competition_id: number[]
}

export interface IUpdateSubCompetitionPublished{
    setting_sub_competition_published?: boolean;
}