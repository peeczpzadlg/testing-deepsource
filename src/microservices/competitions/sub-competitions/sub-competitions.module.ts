import { Module } from '@nestjs/common';
import { SubCompetitionsController } from './sub-competitions.controller';
import { SubCompetitionsService } from './sub-competitions.service';
import { SubCompetitionSocialContactModule } from './sub-competition-social-contact/sub-competition-social-contact.module';
import { SettingTeamsSubCompetitionModule } from './setting-teams-sub-competition/setting-teams-sub-competition.module';
import { CompetitionSectionsModule } from './competition-sections/competition-sections.module';

@Module({
  controllers: [SubCompetitionsController],
  providers: [SubCompetitionsService],
  imports: [SubCompetitionSocialContactModule, SettingTeamsSubCompetitionModule, CompetitionSectionsModule],
  exports: [SubCompetitionsService]
})
export class SubCompetitionsModule {}
