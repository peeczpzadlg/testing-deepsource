import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import FormData from 'form-data';
import { IMicroservicePaginateQuery } from '../../../shared/interfaces/api-interfaces';
import { ISearchPaginateActive } from '../../../microservices/microservices.interface';
import { getResponse } from '../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../shared/utils/microservice-axios.utils';
import { IFindAllSubCompeteQuery, ISaveMediaVideoUrl, ISaveSubCompetition, IUpdateSubCompetition, IUpdateSubCompetitionPublished } from './sub-competitions.interface';
import { EFileType } from '../../../microservices/media/media.enum';

@Injectable()
export class SubCompetitionsService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(SubCompetitionsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async findAllSubCompete(params: IFindAllSubCompeteQuery, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competitions/find-all/sub-competition/`,
                {
                    headers: {
                        Authorization: accessToken
                    },
                    params
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getSubCompetition(competitionId: number, params: ISearchPaginateActive, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competitions/${competitionId}/sub-competitions/`,
                {
                    headers: {
                        Authorization: accessToken
                    },
                    params
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async getSubCompetitionById(competitionId: number, subCompetitionId: number, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competitions/${competitionId}/sub-competitions/${subCompetitionId}`,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async createSubCompetition(params: number, body: ISaveSubCompetition, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`competitions/${params}/sub-competitions/create`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async updateSubCompetition(competitionId: number, subCompetitionId: number, body: IUpdateSubCompetition, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/update`, body,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async deleteSubCompetition(competitionId: number, subCompetitionId: number, accessToken: string) {
        try {
            return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/delete`, null,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err
        }
    }

    async getProfileImage(fileType: EFileType, competitionId: number, subCompetitionId: number, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`,
                {
                    headers: {
                        Authorization: accessToken
                    }
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async uploadProfileImage(fileType: EFileType, competitionId: number, subCompetitionId: number , body: FormData, accessToken: string) {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`, body,
            {
                headers: {
                    Authorization: accessToken,
                    ...body.getHeaders()
                }
            }
        ));
    }

    async getMediaImages(fileType: EFileType, competitionId: number, subCompetitionId: number, params: IMicroservicePaginateQuery, accessToken: string) {
        try {
          return await getResponse(this.microservice.get(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`,
                {
                    headers: {
                        Authorization: accessToken
                    },
                    params
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async uploadMediaImages(fileType: EFileType, competitionId: number, subCompetitionId: number, body: FormData, accessToken: string) {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`, body,
            {
                headers: {
                    Authorization: accessToken,
                    ...body.getHeaders()
                }
            }
        ));
    }

    async getMediaVideos(fileType: EFileType, competitionId: number, subCompetitionId: number, params: IMicroservicePaginateQuery, accessToken: string) {
        try {
            return await getResponse(this.microservice.get(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`,
                {
                    headers: {
                        Authorization: accessToken
                    },
                    params
                }
            ));
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }

    async uploadMediaVideos(fileType: EFileType, competitionId: number, subCompetitionId: number, body: FormData, accessToken: string) {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${fileType}`, body,
            {
                headers: {
                    Authorization: accessToken,
                    ...body.getHeaders()
                },
                maxBodyLength: Infinity
            }
        ));
    }

    async uploadMediaVideoUrl(competitionId: number, subCompetitionId: number, body: ISaveMediaVideoUrl, accessToken: string) {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/videos-url`, body,
            {
                headers: {
                    Authorization: accessToken
                }
            }
        ));
    }

    async deleteMedias(competitionId: number, subCompetitionId: number, mediaId: number, accessToken: string) {
        return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/medias/${mediaId}/delete`, null,
            {
                headers: {
                    Authorization: accessToken
                }
            }
        ));
    }

    async checkSportCategoryUsed(categoryId: number, accessToken: string) {
        return await getResponse(
          this.microservice.get(
            `check/global-config-sport-categories/${categoryId}`,
            {
              headers: {
                Authorization: accessToken,
              },
            },
          ),
        );
      }

    async updatePublished(competitionId: number,subCompetitionId: number, body: IUpdateSubCompetitionPublished, accessToken: string) {
        try {
          return await getResponse(this.microservice.post(`competitions/${competitionId}/sub-competitions/${subCompetitionId}/published/update`,body,
                {
                    headers: {
                        Authorization: accessToken
                    },                
                }
            ))
        } catch(err) {
          this.logger.error(err);
          throw err;
        } 
    }
}
