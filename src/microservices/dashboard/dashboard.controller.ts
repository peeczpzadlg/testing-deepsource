import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import {
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
  FindPlayerInProvicesQueryDto,
  YearStartEndQueryDto,
} from './dashboard.dto';
import { DashboardService } from './dashboard.service';

@ApiTags('Dashboard')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('dashboard')
export class DashboardController {
  constructor(private dashboardService: DashboardService) {}
  @Get('find-player-in-provinces')
  async getFindPlayerInProvinces(
    @Req() req: Request,
    @Query() query: FindPlayerInProvicesQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.getFindPlayerInProvinces(query, token),
    );
  }

  @Get('personnel-ratio')
  async getPersonel(@Req() req: Request) {
    const data = await this.dashboardService.getPersonnelRatio();
    return new ResponseSuccess('successful', data);
  }

  @Get('team-nation-player-history')
  async getTeamNationPlayerHistory(
    @Req() req: Request,
    @Query() query: YearStartEndQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.getTeamNationPlayerHistory(query, token),
    );
   
  }

  @Get('team-club-player-history')
  async getTeamClubPlayerHistory(
    @Req() req: Request,
    @Query() query: YearStartEndQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.getTeamClubPlayerHistory(query, token),
    );
   
  }

  @Get('find-player-in-team-club')
  async findPlayerInTeamClub(
    @Req() req: Request,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.findPlayerInTeamClub(token),
    );
   
  }

  @Get('find-player-in-team-national')
  async findPlayerInTeamNation(
    @Req() req: Request,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.findPlayerInTeamNational(token),
    );
   
  }

  @Get('find-player-in-year/:year')
  async findStaffInYear(
    @Req() req: Request,
    @Param('year') year: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.dashboardService.findPlayerInYear(year, token),
    );
   
  }

}
