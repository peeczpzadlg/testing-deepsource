import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString, Length, MaxLength, MinLength } from 'class-validator';
import { EGender } from '../../shared/enum/gender.enum';
import { IDashboardQueryDto } from './dashboard.interface';
export class FindPlayerInProvicesQueryDto implements IDashboardQueryDto{
    @ApiPropertyOptional({
      enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER],
    })
    @IsOptional()
    @IsEnum(EGender)
    gender?: EGender;

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    age_min?: number;
  
    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    age_max?: number;

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    position_id?: number;

}

export class YearStartEndQueryDto {

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size: number;

  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  start_year: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  end_year: number;
  
}