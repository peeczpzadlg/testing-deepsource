export interface IDashboardQueryDto {
  gender?: number;
  age_min?: number;
  age_max?: number;
  position_id?: number;
}

export interface IDashboardYearStartEndQueryDto {
  start_year:number;
  end_year:number;
}