import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from '../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from '../../settings/primary/configs/entities/global-config-positions.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([AccountSpecifications, GlobalConfigPositions]),
  ],
  providers: [DashboardService],
  controllers: [DashboardController],
})
export class DashboardModule {}
