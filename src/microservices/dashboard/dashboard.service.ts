import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { AccountSpecifications } from '../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from '../../settings/primary/configs/entities/global-config-positions.entity';
import { getResponse } from '../../shared/utils/helper.utils';
import { getManager, Repository } from 'typeorm';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import {
  FindPlayerInProvicesQueryDto,
  YearStartEndQueryDto,
} from './dashboard.dto';
import { IDashboardYearStartEndQueryDto } from './dashboard.interface';

@Injectable()
export class DashboardService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(DashboardService.name);
  constructor(
    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,

    @InjectRepository(AccountSpecifications)
    private accountSpecificationsRepository: Repository<AccountSpecifications>,
  ) {
    this.microservice = microserviceAxios();
  }
  async getFindPlayerInProvinces(
    params: FindPlayerInProvicesQueryDto,
    accessToken: string,
  ) {
    try {
      let accountsSpecificationQuery = this.accountSpecificationsRepository
        .createQueryBuilder('account_specifications')
        .select(['account_specifications.id'])

      if (params.position_id) {
        accountsSpecificationQuery = this.accountSpecificationsRepository
        .createQueryBuilder('account_specifications')
        .select(['account_specifications.id']).where(
          'account_specifications.global_config_positions::jsonb ? :position_id',
          { position_id: params.position_id },
        );
      }
      const accountsSpecification = await accountsSpecificationQuery.getRawMany();
      const account_specifications_id = [];
      for (let i in accountsSpecification) {
        account_specifications_id.push(
          parseInt(accountsSpecification[i].account_specifications_id),
        );
      }
      const body = {
        account_specifications: account_specifications_id,
      };
      return await getResponse(
        this.microservice.post('/dashboard/find-player-in-provinces', body, {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getPersonnelRatio() {
    try {
      const personaltype = await getManager().query(
        'SELECT a.id,a.global_config_position_name_th,a.global_config_position_name_en,(select count(*) from account_specifications where (global_config_positions::json->>0)::INTEGER = a.id) from global_config_positions a where a.deleted = FALSE and a.global_config_position_type = 2',
      );
      return personaltype;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getTeamNationPlayerHistory(
    params: YearStartEndQueryDto,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get('/dashboard/team-nation-player-history', {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getTeamClubPlayerHistory(
    params: YearStartEndQueryDto,
    accessToken: string,
  ) {
    try {
      return await getResponse(
        this.microservice.get('/dashboard/team-club-player-history', {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getTeamnationExport(params: YearStartEndQueryDto, accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get('/dashboard/team-club-player-history', {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findPlayerInTeamClub(accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get('dashboard/find-player-in-team-club', {
          headers: {
            Authorization: accessToken,
          },
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findPlayerInTeamNational(accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get('dashboard/find-player-in-team-national', {
          headers: {
            Authorization: accessToken,
          },
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findPlayerInYear(year: number, accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get(`dashboard/find-player-in-year/${year}`, {
          headers: {
            Authorization: accessToken,
          },
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
