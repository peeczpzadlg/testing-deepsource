import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { PaginateDto, PostVideoLinkDto } from './media.dto';
import { EDocumentType, EFileType, EUserType } from './media.enum';
import { MediaService } from './media.service';
import FormData from 'form-data';
import { ApiFile } from '../../shared/utils/file.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { Request } from 'express';

@ApiTags('Media')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account')
export class MediaController {
  constructor(
    private mediaService: MediaService
  ) {}

  @Get(':accountId/medias/players/profile')
  async getPlayerProfile(
    @Req() req: Request, 
    @Param('accountId') accountId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.PLAYER, EFileType.PROFILE, accountId, null, null, token));
  }

  @Post(':accountId/medias/players/profile')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerProfile(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.PLAYER, EFileType.PROFILE, accountId, body, token));
  }

  @Get(':accountId/medias/players/preview')
  async getPlayerPreview(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.PLAYER, EFileType.PROFILE_PREVIEW, accountId, null, null, token));
  }

  @Post(':accountId/medias/players/preview')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerProfilePreview(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.PLAYER, EFileType.PROFILE_PREVIEW, accountId, body, token));
  }

  @Get(':accountId/medias/players/images')
  async getPlayerImages(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: PaginateDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.PLAYER, EFileType.IMAGE, accountId, query, null, token));
  }

  @Get(':accountId/medias/players/images/:mediaId')
  async getPlayerImage(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Param('mediaId') mediaId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.PLAYER, EFileType.IMAGE, accountId, null, mediaId, token));
  }

  @Post(':accountId/medias/players/images')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerImages(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.PLAYER, EFileType.IMAGE, accountId, body, token));
  }

  @Get(':accountId/medias/players/videos')
  async getPlayerVideos(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: PaginateDto
    ) {
      const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.PLAYER, EFileType.VIDEO, accountId, query, null, token));
  }

  @Post(':accountId/medias/players/videos')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerVideo(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.PLAYER, EFileType.VIDEO, accountId, body, token));
  }

  
  @Get(':accountId/medias/players/documents/citizen')
  async getPlayerCitizen(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.PLAYER, EDocumentType.CITIZEN, accountId, token));
  }

  @Post(':accountId/medias/players/documents/citizen')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerCitizen(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.PLAYER, EDocumentType.CITIZEN, accountId, body, token));
  }
  
  @Get(':accountId/medias/players/documents/passport')
  async getPlayerPassport(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.PLAYER, EDocumentType.PASSPORT, accountId, token));
  }

  @Post(':accountId/medias/players/documents/passport')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerPassport(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.PLAYER, EDocumentType.PASSPORT, accountId, body, token));
  }
  
  @Get(':accountId/medias/players/documents/house-particulars')
  async getPlayerHouseParticular(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.PLAYER, EDocumentType.HOUSE_PARTICULAR, accountId, token));
  }

  @Post(':accountId/medias/players/documents/house-particulars')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerHouseParticular(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.PLAYER, EDocumentType.HOUSE_PARTICULAR, accountId, body, token));
  }
  
  @Get(':accountId/medias/players/documents/code-personel')
  async getPlayerHouseCodePersonel(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.PLAYER, EDocumentType.CODE_PERSONEL, accountId, token));
  }

  @Post(':accountId/medias/players/documents/code-personel')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerCodePersonel(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.PLAYER, EDocumentType.CODE_PERSONEL, accountId, body, token));
  }
  
  @Get(':accountId/medias/players/documents/other')
  async getPlayerOther(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.PLAYER, EDocumentType.OTHER, accountId, token));
  }

  @Post(':accountId/medias/players/documents/other')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postPlayerOther(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.PLAYER, EDocumentType.OTHER, accountId, body, token));
  }

  
  ////// staff
  @Get(':accountId/medias/staffs/profile')
  async getStaffprofile(
    @Req() req: Request, 
    @Param('accountId') accountId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.STAFF, EFileType.PROFILE, accountId, null, null, token));
  }

  @Post(':accountId/medias/staffs/profile')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffProfile(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.STAFF, EFileType.PROFILE, accountId, body, token));
  }

  @Get(':accountId/medias/staffs/preview')
  async getStaffPreview(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.STAFF, EFileType.PROFILE_PREVIEW, accountId, null, null, token));
  }

  @Post(':accountId/medias/staffs/preview')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffProfilePreview(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.STAFF, EFileType.PROFILE_PREVIEW, accountId, body, token));
  }

  @Get(':accountId/medias/staffs/images')
  async getStaffImages(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: PaginateDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.STAFF, EFileType.IMAGE, accountId, query, null, token));
  }

  // @Get(':accountId/medias/staffs/images/:mediaId')
  // async getStaffImage(@Param('accountId') accountId: number, @Param('mediaId') mediaId: number) {
  //   return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.STAFF, EFileType.IMAGE, accountId, mediaId));
  // }

  @Post(':accountId/medias/staffs/images')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffImages(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.STAFF, EFileType.IMAGE, accountId, body, token));
  }

  @Get(':accountId/medias/staffs/videos')
  async getStaffVideos(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: PaginateDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getMedias(EUserType.STAFF, EFileType.VIDEO, accountId, query, null, token));
  }

  @Post(':accountId/medias/staffs/videos')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffVideo(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postMedia(EUserType.STAFF, EFileType.VIDEO, accountId, body, token));
  }

  
  @Get(':accountId/medias/staffs/documents/citizen')
  async getStaffCitizen(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.STAFF, EDocumentType.CITIZEN, accountId, token));
  }

  @Post(':accountId/medias/staffs/documents/citizen')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffCitizen(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.STAFF, EDocumentType.CITIZEN, accountId, body, token));
  }
  
  @Get(':accountId/medias/staffs/documents/passport')
  async getStaffPassport(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.STAFF, EDocumentType.PASSPORT, accountId, token));
  }

  @Post(':accountId/medias/staffs/documents/passport')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffPassport(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.STAFF, EDocumentType.PASSPORT, accountId, body, token));
  }
  
  @Get(':accountId/medias/staffs/documents/house-particulars')
  async getStaffHouseParticular(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.STAFF, EDocumentType.HOUSE_PARTICULAR, accountId, token));
  }

  @Post(':accountId/medias/staffs/documents/house-particulars')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffHouseParticular(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.STAFF, EDocumentType.HOUSE_PARTICULAR, accountId, body, token));
  }
  
  @Get(':accountId/medias/staffs/documents/code-personel')
  async getStaffHouseCodePersonel(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.STAFF, EDocumentType.CODE_PERSONEL, accountId, token));
  }

  @Post(':accountId/medias/staffs/documents/code-personel')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffCodePersonel(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.STAFF, EDocumentType.CODE_PERSONEL, accountId, body, token));
  }
  
  @Get(':accountId/medias/staffs/documents/other')
  async getStaffOther(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getDocumentMedia(EUserType.STAFF, EDocumentType.OTHER, accountId, token));
  }

  @Post(':accountId/medias/staffs/documents/other')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async postStaffOther(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);

    return await getMicroserviceResponse(this.mediaService.postDocumentMedia(EUserType.STAFF, EDocumentType.OTHER, accountId, body, token));
  }

  @Get(':accountId/medias/video-link')
  async getVideoLink(@Req() req: Request, @Param('accountId') accountId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.getVideoLink(accountId, token));
  }

  @Post(':accountId/medias/video-link')
  async postVideoLink(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Body() body: PostVideoLinkDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.postVideoLink(accountId, body, token));
  }

  @Post(':accountId/medias/:mediaId/delete')
  async deleteMedia(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Param('mediaId') mediaId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.mediaService.deleteMedia(accountId, mediaId, token));
  }
}
