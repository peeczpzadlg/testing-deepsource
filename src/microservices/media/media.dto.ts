import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString, IsUrl } from "class-validator";
import { IPaginate, IPostVideoLink } from "./media.interface";

export class PostVideoLinkDto implements IPostVideoLink {
  @ApiProperty()
  @IsUrl()
  account_media_video_url: string;

  created_by: string;
}

export class PaginateDto implements IPaginate {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  page?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  perPage?: string;
}