export enum EUserType {
  PLAYER = 'players',
  STAFF = 'staffs'
}

export enum EFileType {
  IMAGE = 'images',
  VIDEO = 'videos',
  PROFILE = 'profile',
  PROFILE_PREVIEW = 'preview'
}

export enum EDocumentType {
  CITIZEN = 'citizen',
  PASSPORT = 'passport',
  HOUSE_PARTICULAR = 'house-particulars',
  CODE_PERSONEL = 'code-personnel',
  OTHER = 'other'
}