export interface IPostVideoLink {
  account_media_video_url: string;
  created_by: string;
}

export interface IPaginate {
  page?: string;
  perPage?: string;
}