import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { EDocumentType, EFileType, EUserType } from './media.enum';
import { IPaginate, IPostVideoLink } from './media.interface';
import FormData from 'form-data';

@Injectable()
export class MediaService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getMedias(userType: EUserType, fileType: EFileType, accountId: number, query: IPaginate, mediaId: number, accessToken: string) {
    let params: any;
    if(query){
      params = {
        page: query.page,
        size: query.perPage
      }
    }

    if(mediaId){
      return await getResponse(this.microservice.get(`account/${accountId}/medias/${userType}/${fileType}/${mediaId}`,
        {
          headers: {
            Authorization: accessToken
        },
        params
      }));
    }else{
      return await getResponse(this.microservice.get(`account/${accountId}/medias/${userType}/${fileType}`,
        {
          headers: {
            Authorization: accessToken
        },
        params
      }));
    }
    
  }

  async postMedia(userType: EUserType, fileType: EFileType, accountId: number, body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medias/${userType}/${fileType}`, body,
      {
        headers: {
          Authorization: accessToken,
          ...body.getHeaders()
        },
        maxBodyLength: Infinity
    }));
  }

  async getDocumentMedia(userType: EUserType, documentType: EDocumentType, accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/medias/${userType}/documents/${documentType}`,
      {
        headers: {
          Authorization: accessToken
        }
      }
    ));
  }

  async postDocumentMedia(userType: EUserType, documentType: EDocumentType, accountId: number, body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medias/${userType}/documents/${documentType}`, body,
      {
        headers: {
          Authorization: accessToken,
          ...body.getHeaders()
      }
    }));
  }

  async getVideoLink(accountId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`account/${accountId}/medias/video-link`,
      {
        headers: {
          Authorization: accessToken
        }
      }
    ));
  }

  async postVideoLink(accountId: number, body: IPostVideoLink, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medias/video-link`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteMedia(accountId: number, mediaId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`account/${accountId}/medias/${mediaId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

}
