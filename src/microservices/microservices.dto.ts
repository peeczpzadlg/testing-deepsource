import { ApiPropertyOptional, ApiQuery } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { IMicroservicePaginateQuery } from '../shared/interfaces/api-interfaces';
import {
  ISearchPaginate,
  ISearchPaginateActive,
} from './microservices.interface';

export class SearchPageDto implements IMicroservicePaginateQuery {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size: number;

  constructor(data: any) {
    Object.assign(this, data);
  }
}

export class SearchPaginateDto implements ISearchPaginate {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;
}

export class SearchPaginateActiveDto implements ISearchPaginateActive {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;

  @ApiPropertyOptional({
    type: Boolean,
  })
  @IsOptional()
  @IsString()
  active: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;
}
