import { IMicroservicePaginateQuery } from "../shared/interfaces/api-interfaces";

export interface ISearchPaginate extends IMicroservicePaginateQuery {
  name?: string;
}

export interface ISearchPaginateActive extends IMicroservicePaginateQuery {
  name?: string;
  active?: string;
}

