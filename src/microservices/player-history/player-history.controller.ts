import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SavePlayerHistoryDto, SearchPlayerChooseDto, SearchPlayerHistoryDto } from './player-history.dto';
import { PlayerHistoryService } from './player-history.service';

@ApiTags('Team Players Histories')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class PlayerHistoryController {
  constructor(private playerHistoryService: PlayerHistoryService) {}

  @Get(':teamId/players/choose')
  async getPlayerChoose(
    @Req() req: Request, 
    @Param('teamId') teamId: number,
    @Query() query: SearchPlayerChooseDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.playerHistoryService.getPlayerChoose(teamId, query, token),
    );
  }

  @Get(':teamId/players')
  async getPlayerHistories(
    @Req() req: Request, 
    @Param('teamId') teamId: number,
    @Query() query: SearchPlayerHistoryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.playerHistoryService.getPlayerHistories(teamId, query, token),
    );
  }

  @Get(':teamId/players/present')
  async getPlayerHistoryPresent(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchPlayerHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.getPlayerHistoryPresent(teamId, query, token));
  }

  @Get(':teamId/players/past')
  async getPlayerHistoryPast(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchPlayerHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.getPlayerHistoryPast(teamId, query, token));
  }

  @Get(':teamId/players/:playerHistoryId')
  async getPlayerHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('playerHistoryId') playerHistoryId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.getPlayerHistory(teamId, playerHistoryId, token));
  }

  @Post(':teamId/players')
  async createPlayerHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SavePlayerHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.createPlayerHistory(teamId, body, token))
  }

  @Post(':teamId/players/:playerHistoryId')
  async upcatePlayerHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('playerHistoryId') playerHistoryId: number, 
    @Body() body: SavePlayerHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.updatePlayerHistory(teamId, playerHistoryId, body, token));
  }

  @Post(':teamId/players/:playerHistoryId/delete')
  async deleteById(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('playerHistoryId') playerHistoryId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.playerHistoryService.deleteByIdPlayerHistory(teamId, playerHistoryId, token))
  }
}
