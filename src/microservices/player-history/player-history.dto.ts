import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsNumber, IsOptional, IsString } from 'class-validator';
import { ToBoolean } from 'src/shared/utils/transform';
import { SearchPaginateDto } from '../microservices.dto';
import {
  ISavePlayerHistory,
  ISearchPlayerChoose,
  ISearchPlayerHistory,
} from './player-history.interface';

export class SearchPlayerChooseDto implements ISearchPlayerChoose {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;
}

export class SearchPlayerHistoryDto extends SearchPaginateDto
  implements ISearchPlayerHistory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  type_personnel?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @ToBoolean()
  team_player_history_loan?: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_min?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_max?: number;
}

export class SavePlayerHistoryDto implements ISavePlayerHistory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  accounts: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_positions: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_player_history_match_count: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_player_history_shirt_number: number;

  @ApiPropertyOptional()
  // @IsOptional()
  // @IsDate()
  team_player_history_begin_at: Date;

  @ApiPropertyOptional()
  // @IsOptional()
  // @IsDate()
  team_player_history_end_at: Date;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_player_history_status: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  team_player_history_loan: boolean;
}

