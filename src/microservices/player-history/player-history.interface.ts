import { ISearchPaginate } from "../microservices.interface";

export interface ISearchPlayerChoose {
  name?: string;
}

export interface ISearchPlayerHistory extends ISearchPaginate {
  type_personnel?: string;
  team_player_history_loan?: boolean;
  age_min?: number;
  age_max?: number;
}

export interface ISavePlayerHistory {
  accounts: number;
  global_config_positions: number;
  team_player_history_match_count: number;
  team_player_history_shirt_number: number;
  team_player_history_begin_at: Date;
  team_player_history_end_at: Date;
  team_player_history_status: number;
  team_player_history_loan: boolean;
}

