import { Module } from '@nestjs/common';
import { PlayerHistoryService } from './player-history.service';
import { PlayerHistoryController } from './player-history.controller';

@Module({
  providers: [PlayerHistoryService],
  controllers: [PlayerHistoryController]
})
export class PlayerHistoryModule {}
