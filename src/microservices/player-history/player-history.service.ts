import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISavePlayerHistory, ISearchPlayerChoose, ISearchPlayerHistory } from './player-history.interface';

@Injectable()
export class PlayerHistoryService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getPlayerChoose(teamId: number, params: ISearchPlayerChoose, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/players/choose/players`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getPlayerHistories(teamId: number, params: ISearchPlayerHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/players`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getPlayerHistory(teamId: number, playerHistioryId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/players/${playerHistioryId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }
  
  async getPlayerHistoryPresent(teamId: number, params: ISearchPlayerHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/players/present`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getPlayerHistoryPast(teamId: number, params: ISearchPlayerHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/players/past`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async createPlayerHistory(teamId: number, body: ISavePlayerHistory, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/players`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updatePlayerHistory(teamId: number, playerHistoryId: number, body: ISavePlayerHistory, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/players/${playerHistoryId}/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }

  async deleteByIdPlayerHistory(teamId: number, playerHistoryId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/players/${playerHistoryId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }
}
