import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SearchNameDto } from './region.dto';
import { RegionService } from './region.service';

@ApiTags('Region')
@Controller('regions')
export class RegionController {
  constructor(
    private regionService: RegionService
  ) {}

  @Get('country')
  async getCountry(@Query() query: SearchNameDto) {
    return await getMicroserviceResponse(this.regionService.getCountry(query.name));
  }

  @Get('province')
  async getProvinces(@Query() query: SearchNameDto) {
    return await getMicroserviceResponse(this.regionService.getProvinces(query.name));
  }

  @Get(':provinceId/district')
  async getDistrict(@Param('provinceId') provinceId: number, @Query() query: SearchNameDto) {
    return await getMicroserviceResponse(this.regionService.getDistrict(provinceId, query.name));
  }

  @Get(':districtId/sub-district')
  async getSubdistrict(@Param('districtId') districtId: number, @Query() query: SearchNameDto) {
    return await getMicroserviceResponse(this.regionService.getSubDistrict(districtId, query.name));
  }
}
