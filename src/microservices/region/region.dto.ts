import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class SearchNameDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;
}