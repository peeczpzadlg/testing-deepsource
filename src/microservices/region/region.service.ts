import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';

@Injectable()
export class RegionService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getCountry(name?: string) {
    const params = new URLSearchParams();
    if (name) {
      params.append('name', name);
    }
    return await getResponse(
      this.microservice.get('regions/countries', { params }).then(res => {
        return res;
      }),
    );
  }

  async getProvinces(name?: string) {
    const params = new URLSearchParams();
    if (name) {
      params.append('name', name);
    }
    return await getResponse(
      this.microservice.get('regions/provinces', { params }),
    );
  }

  async getDistrict(provinceCode: number, name?: string) {
    const params = new URLSearchParams();
    if (name) {
      params.append('name', name);
    }
    return await getResponse(
      this.microservice.get(`regions/district/${provinceCode}`, { params }),
    );
  }

  async getSubDistrict(districtCode: number, name?: string) {
    const params = new URLSearchParams();
    if (name) {
      params.append('name', name);
    }
    return await getResponse(
      this.microservice.get(`regions/sub-district/${districtCode}`, { params }),
    );
  }
}
