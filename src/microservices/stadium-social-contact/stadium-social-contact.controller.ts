import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SaveStadiumSocialContactDto } from './stadium-social-contact.dto';
import { StadiumSocialContactService } from './stadium-social-contact.service';

@ApiTags('Setting Stadium Social-contact')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('stadium')
export class StadiumSocialContactController {
  constructor(
    private stadiumSocialContactService: StadiumSocialContactService
  ) {}

  @Get(':stadiumId/social-contact')
  async getStadiunSocialContact(@Req() req: Request, @Param('stadiumId') stadiumId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumSocialContactService.getStadiumSocialContact(stadiumId, token));
  }

  @Post(':stadiumId/social-contact')
  @ApiBody({ required: false, type: SaveStadiumSocialContactDto })
  async saveStadiumSocialContact(
    @Req() req: Request, 
    @Param('stadiumId') stadiumId: number, 
    @Body() body: SaveStadiumSocialContactDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumSocialContactService.saveStadiumSocialContact(stadiumId, body, token));
  }

  @Post(':stadiumId/social-contact/:socialContactId/delete')
  async deleteStadiumSocialContact(
    @Req() req: Request, 
    @Param('stadiumId') stadiumId: number, 
    @Param('socialContactId') socialContactId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumSocialContactService.deleteStadiumSocialContact(stadiumId, socialContactId, token));
  }
}
