import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { ISaveStadiumSocialContact } from './stadium-social-contact.interface';

export class SaveStadiumSocialContactDto implements ISaveStadiumSocialContact {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_email?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_phone?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_facebook?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_line?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_instagram?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_twitter?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_youtube?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_website?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_social_contact_fax?: string;
}
