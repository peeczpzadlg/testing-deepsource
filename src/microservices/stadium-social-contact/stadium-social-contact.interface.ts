export interface ISaveStadiumSocialContact {
  setting_stadium_social_contact_email?: string;
  setting_stadium_social_contact_phone?: string;
  setting_stadium_social_contact_facebook?: string;
  setting_stadium_social_contact_line?: string;
  setting_stadium_social_contact_instagram?: string;
  setting_stadium_social_contact_twitter?: string;
  setting_stadium_social_contact_youtube?: string;
  setting_stadium_social_contact_website?: string;
  setting_stadium_social_contact_fax?: string;
}
