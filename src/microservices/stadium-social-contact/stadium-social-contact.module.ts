import { Module } from '@nestjs/common';
import { StadiumSocialContactService } from './stadium-social-contact.service';
import { StadiumSocialContactController } from './stadium-social-contact.controller';

@Module({
  providers: [StadiumSocialContactService],
  controllers: [StadiumSocialContactController]
})
export class StadiumSocialContactModule {}
