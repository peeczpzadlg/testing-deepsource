import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISaveStadiumSocialContact } from './stadium-social-contact.interface';

@Injectable()
export class StadiumSocialContactService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStadiumSocialContact(stadiumId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`stadium/${stadiumId}/setting/social-contacts`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async saveStadiumSocialContact(stadiumId: number, body: ISaveStadiumSocialContact, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/setting/social-contacts`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteStadiumSocialContact(stadiumId: number, socialContactId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/setting/social-contacts/${socialContactId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }
}
