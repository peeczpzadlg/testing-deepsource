import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { ApiFile } from '../../shared/utils/file.utils';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { deleteMultipleStadiumDto, SaveStadiumDto, SearchStadiumDto, UpdateStatdiumPublished } from './stadium.dto';
import { StadiumService } from './stadium.service';
import FormData from 'form-data';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { Request } from 'express';

@ApiTags('stadium')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('stadium')
export class StadiumController {
  constructor(
    private stadiumService: StadiumService
  ) {}

  @Get()
  async getStadiums(@Req() req: Request, @Query() query: SearchStadiumDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.getStadiums(query, token));
  }

  @Get(':stadiumId')
  async getStadium(@Req() req: Request, @Param('stadiumId') stadiumId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.getStadium(stadiumId, token));
  }

  @Post()
  async createStadium(@Req() req: Request, @Body() body: SaveStadiumDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.createStadium(body, token));
  }

  @Post(':stadiumId/update')
  async updateStadium(
    @Req() req: Request, 
    @Param('stadiumId') stadiumId: number, 
    @Body() body: SaveStadiumDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.updateStadium(stadiumId, body, token))
  }

  @Post('multi/delete')
  async deleteMultiple(@Req() req: Request, @Body() body: deleteMultipleStadiumDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.deleteMultipleStadium(body, token))
  }

  @Post(':stadiumId/delete')
  async deleteStadium(@Req() req: Request, @Param('stadiumId') stadiumId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.deleteStadium(stadiumId, token));
  }

  @Post(':stadiumId/upload/images')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async uploadStadiumImage(
    @Req() req: Request, 
    @Param('stadiumId') stadiumId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.stadiumService.uploadStadiumImage(stadiumId, body, token));
  }

  @Post(':stadiumId/published/update')
  @ApiBody({ required: true, type: UpdateStatdiumPublished })
  async updateTeamPublished(@Req() req: Request, @Param('stadiumId') stadiumId: number,@Body() body: UpdateStatdiumPublished) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.stadiumService.updateStadiumPublished(stadiumId,body,token));
  }
}
