import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";
import { SearchPaginateDto } from "../microservices.dto";
import { IStadium, IStadiumRegion, IUpdateStadiumPublished } from "./stadium.interface";

class StadiumRegionDto implements IStadiumRegion {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_region_country_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_region_address_detail?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_region_address_detail_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_region_province_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_region_district_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_region_subdistrict_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_region_zipcode?: string;
}

export class SaveStadiumDto implements IStadium {
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_owner_name_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_owner_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_capacity?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  pitch_detail_grass?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  pitch_detail_width?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  pitch_detail_length?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_map?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  setting_stadium_regions?: StadiumRegionDto;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_history_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_stadium_history_en?: string;
}

export class deleteMultipleStadiumDto {
  @ApiPropertyOptional({ type: [Number] })
  @IsOptional()
  stadium_id: number[];
}

export class SearchStadiumDto extends SearchPaginateDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_country_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_province_id?: number;
}

export class UpdateStatdiumPublished implements IUpdateStadiumPublished{
  @ApiPropertyOptional({ type: Boolean,default:false })
  @IsBoolean()
  setting_stadium_published?: boolean;
}