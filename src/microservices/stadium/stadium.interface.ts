export interface IStadium {
  setting_stadium_name_th?: string;
  setting_stadium_name_en?: string;
  setting_stadium_owner_name_th?: string;
  setting_stadium_owner_name_en?: string;
  setting_stadium_capacity?: number;
  pitch_detail_grass?: number;
  pitch_detail_width?: string;
  pitch_detail_length?: string;
  setting_stadium_map?: string;
  setting_stadium_regions?: IStadiumRegion;
  setting_stadium_history_th?: string;
  setting_stadium_history_en?: string;
}

export interface IStadiumRegion {
  setting_stadium_region_country_id?: number;
  setting_stadium_region_address_detail?: string;
  setting_stadium_region_address_detail_en?: string;
  setting_stadium_region_province_id?: number;
  setting_stadium_region_district_id?: number;
  setting_stadium_region_subdistrict_id?: number;
  setting_stadium_region_zipcode?: string;
}

export interface IDeleteMultipleStadium {
  stadium_id: number[];
}

export interface IUpdateStadiumPublished{
  setting_stadium_published?: boolean;
}
