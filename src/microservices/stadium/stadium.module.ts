import { Module } from '@nestjs/common';
import { StadiumService } from './stadium.service';
import { StadiumController } from './stadium.controller';

@Module({
  providers: [StadiumService],
  controllers: [StadiumController]
})
export class StadiumModule {}
