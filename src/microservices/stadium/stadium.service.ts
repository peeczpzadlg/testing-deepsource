import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../microservices.interface';
import { IDeleteMultipleStadium, IStadium, IUpdateStadiumPublished } from './stadium.interface';
import FormData from 'form-data';

@Injectable()
export class StadiumService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStadiums(params: ISearchPaginate, accessToken: string) {
    return await getResponse(this.microservice.get('stadium',
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getStadium(stadiumId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`stadium/${stadiumId}`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async createStadium(body: IStadium, accessToken: string) {
    return await getResponse(this.microservice.post('stadium', body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateStadium(stadiumId: number, body: IStadium, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteMultipleStadium(body: IDeleteMultipleStadium, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/multi/delete`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }

  async deleteStadium(stadiumId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async uploadStadiumImage(stadiumId: number, body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/upload/images`, body,
      {
        headers: {
          Authorization: accessToken,
          ...body.getHeaders()
      }
    }));
  }

  async updateStadiumPublished(stadiumId: number, body:IUpdateStadiumPublished, accessToken: string) {
    return await getResponse(this.microservice.post(`stadium/${stadiumId}/published/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

}
