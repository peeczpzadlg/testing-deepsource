import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SaveStaffHistoryDto, SearchStaffChooseDto, SearchStaffHistoryDto } from './staff-history.dto';
import { StaffHistoryService } from './staff-history.service';

@ApiTags('Team Staff History')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class StaffHistoryController {
  constructor(
    private staffHistoryService: StaffHistoryService
  ) {}

  @Get(':teamId/staffs/choose')
  async getPlayerChoose(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchStaffChooseDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.getStaffChoose(teamId, query, token));
  }

  @Get(':teamId/staffs')
  async getStaffHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchStaffHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.getStaffHistory(teamId, query, token))
  }

  @Get(':teamId/staffs/present')
  async getStaffHistoryPresent(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchStaffHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.getStaffHistoryPresent(teamId, query, token));
  }

  @Get(':teamId/staffs/past')
  async getStaffHistoryPast(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Query() query: SearchStaffHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.getStaffHistoryPast(teamId, query, token));
  }

  @Get(':teamId/staffs/:historyId')
  async getStaffHistoryById(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('historyId') historyId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.getStaggHistoryById(teamId, historyId, token));
  }


  @Post(':teamId/staffs')
  async saveStaffHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SaveStaffHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.createStaffHistory(teamId, body, token));
  }

  @Post(':teamId/staffs/:historyId/update')
  async updateStaffHistory(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('historyId') historyId: number, 
    @Body() body: SaveStaffHistoryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.updateStaffHistory(teamId, historyId, body, token));
  }

  @Post(':teamId/staffs/:historyId/delete')
  async deleteById(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('historyId') historyId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.staffHistoryService.deleteByIdStaffHistory(teamId, historyId, token))
  }
}
