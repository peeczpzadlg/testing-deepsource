import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsNumber, IsOptional, IsString } from 'class-validator';
import { SearchPaginateDto } from '../microservices.dto';
import {
  ISaveStaffHistory,
  ISearchStaffChoose,
  ISearchStaffHistory,
} from './staff-history.interface';

export class SearchStaffChooseDto implements ISearchStaffChoose {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;
}

export class SaveStaffHistoryDto implements ISaveStaffHistory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_positions: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_department: number;

  @ApiPropertyOptional()
  // @IsOptional()
  // @IsDate()
  team_staff_history_begin_at: Date;

  @ApiPropertyOptional()
  // @IsOptional()
  // @IsDate()
  team_staff_history_end_at: Date;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_staff_history_status: number;
}

export class SearchStaffHistoryDto extends SearchPaginateDto
  implements ISearchStaffHistory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  type_personnel?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  team_player_history_loan?: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_min?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  age_max?: number;
}

