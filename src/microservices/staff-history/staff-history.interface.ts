import { ISearchPaginate } from "../microservices.interface";

export interface ISearchStaffChoose {
  name?: string;
}

export interface ISaveStaffHistory {
  global_config_positions: number;
  global_config_department: number;
  team_staff_history_begin_at: Date;
  team_staff_history_end_at: Date;
  team_staff_history_status: number;
}

export interface ISearchStaffHistory extends ISearchPaginate {
  type_personnel?: string;
  team_player_history_loan?: boolean;
  age_min?: number;
  age_max?: number;
}
