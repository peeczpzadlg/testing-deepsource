import { Module } from '@nestjs/common';
import { StaffHistoryService } from './staff-history.service';
import { StaffHistoryController } from './staff-history.controller';

@Module({
  providers: [StaffHistoryService],
  controllers: [StaffHistoryController]
})
export class StaffHistoryModule {}
