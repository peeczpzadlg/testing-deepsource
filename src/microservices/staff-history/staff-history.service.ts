import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISaveStaffHistory, ISearchStaffChoose, ISearchStaffHistory } from './staff-history.interface';

@Injectable()
export class StaffHistoryService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStaffChoose(teamId: number, params: ISearchStaffChoose, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/staffs/choose/staffs`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getStaffHistory(teamId: number, params: ISearchStaffHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/staffs`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }
  
  async getStaffHistoryPresent(teamId: number, params: ISearchStaffHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/staffs/present`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async getStaggHistoryById(teamId: number, historyId: number, accessToken) {
    return await getResponse(this.microservice.get(`team/${teamId}/staffs/${historyId}`,
      {
        headers: {
          Authorization: accessToken
      },
    }));
  }

  async getStaffHistoryPast(teamId: number, params: ISearchStaffHistory, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/staffs/past`,
      {
        headers: {
          Authorization: accessToken
      },
      params
    }));
  }

  async createStaffHistory(teamId: number, body: ISaveStaffHistory, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/staffs`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async updateStaffHistory(teamId: number, historyId: number, body: ISaveStaffHistory, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/staffs/${historyId}/update`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteByIdStaffHistory(teamId: number, staffHistoryId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/staffs/${staffHistoryId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }
}
