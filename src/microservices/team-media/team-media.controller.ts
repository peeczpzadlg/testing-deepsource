import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import FormData from 'form-data';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ApiFile, ApiFileDocument } from '../../shared/utils/file.utils';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { EFileType } from '../media/media.enum';
import { SearchPageDto } from '../microservices.dto';
import { SaveTeamDocumentDto, SaveTeamMediaRemarkDto, SaveTeamVideoLinkDto } from './team-media.dto';
import { TeamMediaService } from './team-media.service';

@ApiTags('Setting Team Medias')
@Controller('team')
export class TeamMediaController {
  constructor(
    private teamMediaService: TeamMediaService
  ) {}

  @Get(':teamId/medias/profile')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getProfile(
    @Req() req: Request, 
    @Param('teamId') teamId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.getTeamProfile(EFileType.PROFILE, teamId, token));
  }

  @Post(':teamId/medias/profile')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async postProfile(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.teamMediaService.saveTeamProfile(EFileType.PROFILE, teamId, body, token));
  }

  @Get(':teamId/medias/images')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getImage(@Req() req: Request, @Param('teamId') teamId: number,@Query() query:SearchPageDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.getTeamMedia(EFileType.IMAGE, teamId, query, token))
  }

  @Post(':teamId/medias/images')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async saveImage(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.teamMediaService.saveTeamMedia(EFileType.IMAGE, teamId, body, token));
  }

  @Get(':teamId/medias/videos')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getVideo(@Req() req: Request, @Param('teamId') teamId: number,@Query() query:SearchPageDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.getTeamMedia(EFileType.VIDEO, teamId, query, token))
  }

  @Post(':teamId/medias/videos')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async saveVideo(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData();
    body.append('file', file.buffer, file.originalname);
    return await getMicroserviceResponse(this.teamMediaService.saveTeamMedia(EFileType.VIDEO, teamId, body, token));
  }

  @Post(':teamId/medias/videos-url')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async saveVideoLinks(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SaveTeamVideoLinkDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.saveVideoLink(teamId, body, token));
  }

  @Get(':teamId/medias/documents')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getDocument(@Req() req: Request, @Param('teamId') teamId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.getDocument(teamId, token));
  }

  @Post(':teamId/medias/documents/')
  @ApiConsumes('multipart/form-data')
  @ApiFileDocument()
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async saveDocument(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @UploadedFile() file: Express.Multer.File, 
    @Body() body: SaveTeamDocumentDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const formData = new FormData();
    formData.append('file', file.buffer, file.originalname);
    formData.append('setting_team_media_file_name', body.setting_team_media_file_name);
    return await getMicroserviceResponse(this.teamMediaService.saveDocument(teamId, formData, token));
  }

  @Post(':teamId/medias/:mediaId/delete')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async deleteMedia(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('mediaId') mediaId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.deleteMedia(teamId, mediaId, token));
  }

  @Get(':teamId/medias/remark')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getRemark(
    @Req() req: Request, 
    @Param('teamId') teamId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.getRemark(teamId, token));
  }

  @Post(':teamId/medias/remark')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async saveRemark(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SaveTeamMediaRemarkDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamMediaService.saveRemark(teamId, body, token));
  }

  @Post('shirt-uniform/onhold')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async onHoldImageUniform(
    @Req() req: Request, 
    @UploadedFile() file: Express.Multer.File, 
    @Res() response: Response
    ) {
      return response.status(HttpStatus.OK).send({success: true});
  }
}
