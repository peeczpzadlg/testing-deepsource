import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { ISaveTeamDocument, ISaveTeamMediaRemark, ISaveTeamVideoLink } from "./team-media.interface";

export class SaveTeamMediaRemarkDto implements ISaveTeamMediaRemark {
  @ApiProperty()
  @IsString()
  setting_team_information_remark: string;

  @ApiProperty()
  @IsString()
  setting_team_information_remark_en: string;
}

export class SaveTeamVideoLinkDto implements ISaveTeamVideoLink {
  @ApiProperty()
  @IsString()
  setting_team_media_video_url: string;
}

export class SaveTeamDocumentDto implements ISaveTeamDocument {
  @ApiProperty()
  @IsString()
  setting_team_media_file_name: string;
}