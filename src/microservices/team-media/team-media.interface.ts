export interface ISaveTeamVideoLink {
  setting_team_media_video_url: string;
}

export interface ISaveTeamMediaRemark {
  setting_team_information_remark: string;
  setting_team_information_remark_en: string;
}

export interface ISaveTeamDocument {
  setting_team_media_file_name: string;
}

export interface ISearchPage {
  page?:number;
  size?:number;
}