import { Module } from '@nestjs/common';
import { TeamMediaService } from './team-media.service';
import { TeamMediaController } from './team-media.controller';

@Module({
  providers: [TeamMediaService],
  controllers: [TeamMediaController]
})
export class TeamMediaModule {}
