import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import FormData from 'form-data';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { EFileType } from '../media/media.enum';
import { ISaveTeamMediaRemark, ISaveTeamVideoLink, ISearchPage } from './team-media.interface';

@Injectable()
export class TeamMediaService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getTeamProfile(type: EFileType, teamId: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${teamId}/medias/${type}`,
        {
          headers: {
            Authorization: accessToken
        }
      }),
    );
  }

  async saveTeamProfile(type: EFileType, teamId: number, body: FormData, accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${teamId}/medias/${type}`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
          },
          maxBodyLength: Infinity
      }),
    );
  }

  async getTeamMedia(type: EFileType, teamId: number,params:ISearchPage,accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${teamId}/medias/${type}`,
        {
          headers: {
            Authorization: accessToken
        },params
      }),
    );
  }

  async saveTeamMedia(type: EFileType, teamId: number, body: FormData, accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${teamId}/medias/${type}`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
          },
          maxBodyLength: Infinity
      }),
    );
  }

  async saveVideoLink(teamId: number, body: ISaveTeamVideoLink, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/medias/videos-url`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }

  async getDocument(teamId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/medias/documents`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async saveDocument(teamId: number, body: FormData, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/medias/documents`, body,
      {
        headers: {
          Authorization: accessToken,
          ...body.getHeaders()
      }
    }));
  }

  async deleteMedia(teamId: number, mediaId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/medias/${mediaId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async getRemark(teamId: number, accessToken: string) {
    return await getResponse(this.microservice.get(`team/${teamId}/medias/remark`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async saveRemark(teamId: number, body: ISaveTeamMediaRemark, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/medias/remark`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }
}
