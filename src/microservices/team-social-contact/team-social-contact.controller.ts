import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { SaveTeamSocialContactDto } from './team-social-contact.dto';
import { TeamSocialContactService } from './team-social-contact.service';

@ApiTags('Setting Team Social-Contacts')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class TeamSocialContactController {
  constructor(
    private teamSocialContactService: TeamSocialContactService
  ) {}

  @Get(':teamId/social-contact')
  async getSocialContact(@Req() req: Request, @Param('teamId') teamId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamSocialContactService.getSocialContact(teamId, token));
  }

  @Post(':teamId/social-contacts')
  async saveSocialContact(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SaveTeamSocialContactDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamSocialContactService.saveSocialContact(teamId, body, token));
  }

  @Post(':teamId/social-contacts/:socialContactId/delete')
  async deleteSocialContact(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('socialContactId') socialContactId: number
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamSocialContactService.deleteSocialContact(teamId, socialContactId, token));
  }
}
