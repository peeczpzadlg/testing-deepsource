import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { ISaveTeamSocialContact } from './team-social-contact.interface';

export class SaveTeamSocialContactDto implements ISaveTeamSocialContact {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_email: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_phone: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_facebook: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_line: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_instagram: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_twitter: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_youtube: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_website: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_social_contact_fax: string;
}
