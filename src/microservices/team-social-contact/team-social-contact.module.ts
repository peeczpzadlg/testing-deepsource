import { Module } from '@nestjs/common';
import { TeamSocialContactService } from './team-social-contact.service';
import { TeamSocialContactController } from './team-social-contact.controller';

@Module({
  providers: [TeamSocialContactService],
  controllers: [TeamSocialContactController]
})
export class TeamSocialContactModule {}
