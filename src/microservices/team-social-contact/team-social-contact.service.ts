import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISaveTeamSocialContact } from './team-social-contact.interface';

@Injectable()
export class TeamSocialContactService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getSocialContact(teamId: number, accessToken: string) {
    return await getResponse(this.microservice(`team/${teamId}/social-contacts`,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async saveSocialContact(teamId: number, body: ISaveTeamSocialContact, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/social-contacts`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }

  async deleteSocialContact(teamId: number, socialConatactId: number, accessToken: string) {
    return await getResponse(this.microservice.post(`team/${teamId}/social-contacts/${socialConatactId}/delete`, null,
      {
        headers: {
          Authorization: accessToken
      }
    }));
  }
}
