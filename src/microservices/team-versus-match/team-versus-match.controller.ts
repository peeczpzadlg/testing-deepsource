import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import {
  getHeaderAccessToken,
  getMicroserviceResponse,
} from 'src/shared/utils/helper.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
  TeamVersusmatchQueryDto,
  TeamVersusmatchUpdateBodyDto,
} from './team-versus-match.dto';
import { TeamVersusMatchService } from './team-versus-match.service';

@ApiTags('Team-Versus-Match')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class TeamVersusMatchController {
  constructor(private teamVersusMatch: TeamVersusMatchService) {}

  @Get(':team_id/match/history')
  async getMatchHistory(
    @Req() req: Request,
    @Query() query: TeamVersusmatchQueryDto,
    @Param('team_id') team_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamVersusMatch.getMatchHistory(team_id,query,token),
    );
  }

  @Get(':team_id/match/important')
  async getMatchImportant(
    @Req() req: Request,
    @Query() query: TeamVersusmatchQueryDto,
    @Param('team_id') team_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamVersusMatch.getMatchImportant(team_id,query,token),
    );
  }

  @Get(':team_id/match/versus-match/:versus_match_id')
  async getMatchVersusmatch(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Param('versus_match_id') versus_match_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamVersusMatch.getTeamVersusMatch(team_id,versus_match_id,token),
    );
  }

  @Post(
    ':team_id/match/:versus_match_id/team-detail/:versus_match_team_detail_id/update',
  )
  async getMatchTeamDetailUpdate(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Param('versus_match_id') versus_match_id: number,
    @Param('versus_match_team_detail_id') versus_match_team_detail_id: number,
    @Body() body: TeamVersusmatchUpdateBodyDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamVersusMatch.teamDetailVersusMatchUpdate(team_id,versus_match_id,versus_match_team_detail_id,body,token),
    );
  }
}
