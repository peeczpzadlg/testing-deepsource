import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import {
  ITeamversusmatch,
  ITeamversusmatchUpdate,
} from './team-versus-match.interface';

export class TeamVersusmatchQueryDto implements ITeamversusmatch {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_competitions?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadiums?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_sub_competition_year?: number;
}

export class TeamVersusmatchUpdateBodyDto implements ITeamversusmatchUpdate {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  versus_match_team_detail_remark: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  versus_match_team_detail_remark_en: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  versus_match_team_detail_important: boolean;
}
