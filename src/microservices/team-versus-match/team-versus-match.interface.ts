export interface ITeamversusmatch {
  page?: number;
  size?: number;
  setting_competitions?: number;
  setting_stadiums?: number;
  setting_sub_competition_year?: number;
}

export interface ITeamversusmatchUpdate {
    versus_match_team_detail_remark: string,
    versus_match_team_detail_important:boolean;

}