import { Module } from '@nestjs/common';
import { TeamVersusMatchService } from './team-versus-match.service';
import { TeamVersusMatchController } from './team-versus-match.controller';

@Module({
  providers: [TeamVersusMatchService],
  controllers: [TeamVersusMatchController]
})
export class TeamVersusMatchModule {}
