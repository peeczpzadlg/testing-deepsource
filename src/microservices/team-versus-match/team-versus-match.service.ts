import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { ITeamversusmatch, ITeamversusmatchUpdate } from './team-versus-match.interface';

@Injectable()
export class TeamVersusMatchService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }
  async getMatchHistory(team_id:number,params:ITeamversusmatch ,accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${team_id}/match/history`, {
        headers: {
          Authorization: accessToken,
        },params
      }),
    );
  }

  async getMatchImportant(team_id:number,params:ITeamversusmatch ,accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${team_id}/match/important`, {
        headers: {
          Authorization: accessToken,
        },params
      }),
    );
  }

  async getTeamVersusMatch(team_id:number,versus_match_id:number,accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${team_id}/match/versus-match/${versus_match_id}`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async teamDetailVersusMatchUpdate(team_id:number,versus_match_id:number,versus_match_team_detail_id:number,body:ITeamversusmatchUpdate,accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${team_id}/match/${versus_match_id}/team-detail/${versus_match_team_detail_id}/update`, body, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }


}
