import { Body, Controller, Get, Param, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import FormData from 'form-data';
import { ApiFile } from 'src/shared/utils/file.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { deleteMultipleTeamDto, SaveTeamDto, SearchTeamDto, TeamChooseDto, UpdateTeamPublishedDto } from './team.dto';
import { TeamService } from './team.service';

@ApiTags('Team')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class TeamController {
  constructor(
    private teamService: TeamService
  ) {}

  @Get()
  async getTeams(@Req() req: Request, @Query() query: SearchTeamDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.getTeams(query, token))
  }
  
  @Get('choose')
  async getTeamsChoose(@Req() req: Request, @Query() query: TeamChooseDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.getTeamChoose(query, token))
  }

  @Get('nation')
  async getNationTeams(@Req() req: Request, @Query() query: SearchTeamDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.getNationTeams(query, token))
  }

  @Get('club')
  async getClubTeams(@Req() req: Request, @Query() query: SearchTeamDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.getClubTeams(query, token))
  }

  @Get(':teamId')
  async getTeam(@Req() req: Request, @Param('teamId') teamId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.getTeam(teamId, token));
  }

  @Post('create')
  async createTeam(@Req() req: Request, @Body() body: SaveTeamDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.createTeam(body, token));
  }

  @Post(':teamId/update')
  async updateTeam(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Body() body: SaveTeamDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.updateTeam(teamId, body, token));
  }

  @Post('multi/delete')
  async deleteMultiple(@Req() req: Request, @Body() body: deleteMultipleTeamDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.deleteMultipleTeam(body, token))
  }

  @Post(':teamId/delete')
  async deleteTeam(@Req() req: Request, @Param('teamId') teamId: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.deleteTeam(teamId, token));
  }

  @Post(':teamId/published/update')
  @ApiBody({ required: true, type: UpdateTeamPublishedDto })
  async updateTeamPublished(@Req() req: Request, @Param('teamId') teamId: number,@Body() body: UpdateTeamPublishedDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.teamService.updateTeamPublished(teamId,body,token));
  }


  @Post('team/import/excel/teams')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async importAccountExcelStaff(
      @Req() req: Request, 
      @UploadedFile() file: Express.Multer.File
  ) {
        const token = await getHeaderAccessToken(req.headers);
        const body = new FormData();
        body.append('file', file.buffer, file.originalname);
        return await getMicroserviceResponse(this.teamService.importExcelTeam(body, token));
  }

}
