import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { ToBoolean } from '../../shared/utils/transform';
import { SearchPaginateDto } from '../microservices.dto';
import { ISaveTeam, ITeamChoose, ITeamRegion, IUpdateTeamPublished } from './team.interface';

export class TeamRegionDto implements ITeamRegion {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_country_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_region_address_detail: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_region_address_detail_en: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_province_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_district_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_subdistrict_id: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_region_zipcode: string;
}

export class SaveTeamDto implements ISaveTeam {
  @ApiProperty()
  @IsNumber()
  global_config_local_types: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_abbreviation: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_name_en: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_nickname: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_established: string;

  @ApiPropertyOptional()
  @IsOptional()
  setting_team_regions: TeamRegionDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_history_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  setting_team_information_history_en: string;
}

export class deleteMultipleTeamDto {
  @ApiPropertyOptional({ type: [Number] })
  @IsOptional()
  team_id: number[];
}

export class TeamChooseDto implements ITeamChoose {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @ToBoolean()
  team_national?: boolean;

  @ApiPropertyOptional()
  @ToBoolean()
  team_club?: boolean;
}

export class SearchTeamDto extends SearchPaginateDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  local_types?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_country_id?: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_team_region_province_id?: number;
}

export class UpdateTeamPublishedDto implements IUpdateTeamPublished {
  @ApiPropertyOptional({ type: Boolean, default:false })
  @IsBoolean()
  setting_team_published?: boolean;
}
