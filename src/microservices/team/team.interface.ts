export interface ISaveTeam {
  global_config_local_types: number;
  setting_team_information_abbreviation: string;
  setting_team_information_name_th: string;
  setting_team_information_name_en: string;
  setting_team_information_nickname: string;
  setting_team_information_established: string;
  setting_team_regions: ITeamRegion;
  setting_team_information_history_th: string;
  setting_team_information_history_en: string;
}

export interface ITeamRegion {
  setting_team_region_country_id: number;
  setting_team_region_address_detail: string;
  setting_team_region_address_detail_en: string;
  setting_team_region_province_id: number;
  setting_team_region_district_id: number;
  setting_team_region_subdistrict_id: number;
  setting_team_region_zipcode: string;
}

export interface IDeleteMultipleTeam {
  team_id: number[];
}

export interface IUpdateTeamPublished{
  setting_team_published?: boolean;
}

export interface ITeamChoose {
  team_national?: boolean;
  team_club?: boolean;
}