import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import FormData from 'form-data';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../microservices.interface';
import { IDeleteMultipleTeam, ISaveTeam, ITeamChoose, IUpdateTeamPublished } from './team.interface';

@Injectable()
export class TeamService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(TeamService.name);

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getTeams(params: ISearchPaginate, accessToken: string) {
    try {
      return await getResponse(this.microservice.get('team',
        {
          headers: {
            Authorization: accessToken
        },
        params
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    } 
  }

  async getTeamChoose(params: ITeamChoose, accessToken: string) {
    try {
      return await getResponse(this.microservice.get('team/choose',
        {
          headers: {
            Authorization: accessToken
        },
        params
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    } 
  }

  async getNationTeams(params: ISearchPaginate, accessToken: string) {
    try {
      return await getResponse(this.microservice.get('team/nationals',
        {
          headers: {
            Authorization: accessToken
        },
        params
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    } 
  }

  async getClubTeams(params: ISearchPaginate, accessToken: string) {
    try {
      return await getResponse(this.microservice.get('team/club',
        {
          headers: {
            Authorization: accessToken
        },
        params
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    } 
  }

  async getTeam(teamId: number, accessToken: string) {
    try {
      return await getResponse(this.microservice.get(`team/${teamId}`,
        {
          headers: {
            Authorization: accessToken
        }
      }));
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async createTeam(body: ISaveTeam, accessToken: string) {
    try {
      return await getResponse(this.microservice.post('team/create', body,
        {
          headers: {
            Authorization: accessToken
        }
      }));
    } catch(err) {
      this.logger.error(err);
      throw err
    }
  }

  async updateTeam(teamId: number, body: ISaveTeam, accessToken: string) {
    try {
      return await getResponse(this.microservice.post(`team/${teamId}/update`, body,
        {
          headers: {
            Authorization: accessToken
        }
      }))
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deleteMultipleTeam(body: IDeleteMultipleTeam, accessToken: string) {
    return await getResponse(this.microservice.post(`team/multi/delete`, body,
      {
        headers: {
          Authorization: accessToken
      }
    }))
  }

  async deleteTeam(teamId: number, accessToken: string) {
    try {
      return await getResponse(this.microservice.post(`team/${teamId}/delete`, null,
        {
          headers: {
            Authorization: accessToken
        }
      }));
    } catch(err) {
      this.logger.error(err);
      throw err
    }
  }

  async updateTeamPublished(teamId: number, body: IUpdateTeamPublished, accessToken: string) {
    try {
      return await getResponse(this.microservice.post(`team/${teamId}/published/update`, body,
        {
          headers: {
            Authorization: accessToken
        }
      }));
    } catch(err) {
      this.logger.error(err);
      throw err
    }
  }

  async importExcelTeam(body: FormData, accessToken: string) {

    try {
      return await getResponse(this.microservice.post(`team/import/excel/teams`, body,
        {
          headers: {
              Authorization: accessToken,
              ...body.getHeaders()
          },
          maxBodyLength: Infinity
        }
      ));
    } catch(err) {
      this.logger.error(err);
      throw err
    }
    
  }

}
