import { Controller, Get, Param, Post, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { ApiFile } from '../../shared/utils/file.utils';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../shared/utils/helper.utils';
import { UniformService } from './uniform.service';
import FormData from 'form-data';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { Request } from 'express';

@ApiTags('Team Setting Uniform')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class UniformController {
  constructor(
    private uniformService: UniformService
  ) {}

  @Get(':teamId/uniforms/:year')
  async getUniforms(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('year') year: string
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(this.uniformService.getUniform(teamId, year, token))
  }

  @Post(':teamId/uniforms/home/:year')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async saveHomeUniforms(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('year') year: string, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData()
    body.append('file', file.buffer, file.originalname);
    body.append('year', year);
    return await getMicroserviceResponse(this.uniformService.saveHomeUniform(teamId, body, token));
  }

  @Post(':teamId/uniforms/away/:year')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async saveAwayUniforms(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('year') year: string, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData()
    body.append('file', file.buffer, file.originalname);
    body.append('year', year);
    return await getMicroserviceResponse(this.uniformService.saveAwayUniform(teamId, body, token));
  }

  @Post(':teamId/uniforms/spare/:year')
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @UseInterceptors(FileInterceptor('file'))
  async saveSpareUniforms(
    @Req() req: Request, 
    @Param('teamId') teamId: number, 
    @Param('year') year: string, 
    @UploadedFile() file: Express.Multer.File
  ) {
    const token = await getHeaderAccessToken(req.headers);
    const body = new FormData()
    body.append('file', file.buffer, file.originalname);
    body.append('year', year);
    return await getMicroserviceResponse(this.uniformService.saveSpareUniform(teamId, body, token));
  }
}
