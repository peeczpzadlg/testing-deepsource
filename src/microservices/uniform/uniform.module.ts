import { Module } from '@nestjs/common';
import { UniformService } from './uniform.service';
import { UniformController } from './uniform.controller';

@Module({
  providers: [UniformService],
  controllers: [UniformController]
})
export class UniformModule {}
