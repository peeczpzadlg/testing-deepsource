import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';
import FormData from 'form-data';

@Injectable()
export class UniformService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getUniform(teamId: number, year: string, accessToken: string) {
    return await getResponse(
      this.microservice.get(`team/${teamId}/uniforms/year/${year}`,
        {
          headers: {
            Authorization: accessToken
        }
      }),
    );
  }

  async saveHomeUniform(teamId: number, body: FormData, accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${teamId}/uniforms/home`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        }
      }),
    );
  }

  async saveAwayUniform(teamId: number, body: FormData, accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${teamId}/uniforms/away`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        }
      }),
    );
  }

  async saveSpareUniform(teamId: number, body: FormData, accessToken: string) {
    return await getResponse(
      this.microservice.post(`team/${teamId}/uniforms/spare`, body,
        {
          headers: {
            Authorization: accessToken,
            ...body.getHeaders()
        }
      }),
    );
  }
}
