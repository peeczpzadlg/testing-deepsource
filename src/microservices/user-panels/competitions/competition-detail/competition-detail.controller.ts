import {
    Controller,
    Get,
    Param,
    Query,
    Req,
    UseGuards
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchPageDto, SearchPaginateDto } from '../../../../microservices/microservices.dto';
import { ECompetitionLevel } from '../../../../shared/enum';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import { CompetitionDetailService } from './competition-detail.service';
  
export enum ERoute {
    BANNER="BANNER",
    INFORMATION="INFORMATION"
}

@ApiTags('User Panel - Competition Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('competition')
export class CompetitionDetailController {
    constructor(private competitionDetailService: CompetitionDetailService) {}
  
    @Get('competition-level/:settingCompetitionLevel')
    async getCompetitionLevel(
      @Req() req: Request,
      @Param('settingCompetitionLevel') settingCompetitionLevel: ECompetitionLevel,
      @Query() pageDto: SearchPageDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionDetailService.getCompetitionLevel(
            settingCompetitionLevel,
            pageDto,
            token
        ));
    }
    
    @Get(':competitionId/banner')
    async getCompetitionBanner(
      @Req() req: Request,
      @Param('competitionId') competitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionDetailService.getCompetitionInformationOrBanner(
            competitionId,
            ERoute.BANNER,
            token
        ));
    }
    
    @Get(':competitionId/information')
    async getCompetitionInformation(
      @Req() req: Request,
      @Param('competitionId') competitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionDetailService.getCompetitionInformationOrBanner(
            competitionId,
            ERoute.INFORMATION,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition')
    async getSubCompetitionByCompet(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Query() pageDto: SearchPaginateDto
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionDetailService.getSubCompetitionByCompet(
            competitionId,
            pageDto,
            token
        ));
    }
  }
  