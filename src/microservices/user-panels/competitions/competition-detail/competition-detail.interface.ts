import { IMicroservicePaginateQuery } from "../../../../shared/interfaces/api-interfaces";

export interface IPaginationCompetition extends IMicroservicePaginateQuery {
    page?: number,
    size?: number
}