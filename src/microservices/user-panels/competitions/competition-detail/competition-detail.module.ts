import { Module } from '@nestjs/common';
import { CompetitionDetailController } from './competition-detail.controller';
import { CompetitionDetailService } from './competition-detail.service';

@Module({
  controllers: [CompetitionDetailController],
  providers: [CompetitionDetailService]
})
export class CompetitionDetailModule {}
