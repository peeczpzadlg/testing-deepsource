import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ECompetitionLevel } from '../../../../shared/enum';
import { IPaginationCompetition } from './competition-detail.interface';
import { ERoute } from './competition-detail.controller';
import { ISearchPaginate } from '../../../../microservices/microservices.interface';

@Injectable()
export class CompetitionDetailService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(CompetitionDetailService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getCompetitionLevel(
        settingCompetitionLevel: ECompetitionLevel,
        query: IPaginationCompetition,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/competition-level/${settingCompetitionLevel}`, {
            headers: {
                Authorization: accessToken,
            },
            params: query,
        }));
    }
    
    async getCompetitionInformationOrBanner(
        competitionId: number,
        type: ERoute,
        accessToken: string,
    ) {
        let url: string;
        switch (type) {
            case type = ERoute.BANNER:
                url = `competition/${competitionId}/banner`;
                break;
            case type = ERoute.INFORMATION:
                url = `competition/${competitionId}/information`;
                break;
            default:
                break;
        }
        return await getResponse(this.microservice.get(url, {
            headers: {
                Authorization: accessToken,
            }
        }));
    }

    async getSubCompetitionByCompet(
        competitionId: number,
        query: ISearchPaginate,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition`, {
            headers: {
                Authorization: accessToken,
            },
            params: query,
        }));
    }
}
