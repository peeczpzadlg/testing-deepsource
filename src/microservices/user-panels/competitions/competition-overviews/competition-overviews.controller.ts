import {
    Controller,
    Get,
    Query,
    Req,
    UseGuards
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchPaginateDto } from '../../../..//microservices/microservices.dto';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import { CompetitionOverviewsService } from './competition-overviews.service';
  
  @ApiTags('User Panel - Competition Menu')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Controller('competition')
  export class CompetitionOverviewsController {
    constructor(private competitionOverviewsService: CompetitionOverviewsService) {}
  
    @Get('overview')
    async getCompetitionOverviews(
      @Req() req: Request,
      @Query() pageDto: SearchPaginateDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionOverviewsService.getCompetitionOverviews(
          pageDto,
            token
        ));
    }
  }
  