import { Module } from '@nestjs/common';
import { CompetitionOverviewsController } from './competition-overviews.controller';
import { CompetitionOverviewsService } from './competition-overviews.service';

@Module({
  controllers: [CompetitionOverviewsController],
  providers: [CompetitionOverviewsService]
})
export class CompetitionOverviewsModule {}
