import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../../../..//microservices/microservices.interface';
@Injectable()
export class CompetitionOverviewsService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(CompetitionOverviewsService.name);

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getCompetitionOverviews(
    query: ISearchPaginate,
    accessToken: string,
  ) {
    return await getResponse(this.microservice.get('competition/overview', {
        headers: {
          Authorization: accessToken,
        },
        params: query,
      })
    );
  }
}