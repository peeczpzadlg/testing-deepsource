import { Module } from '@nestjs/common';
import { CompetitionOverviewsModule } from './competition-overviews/competition-overviews.module';
import { CompetitionDetailModule } from './competition-detail/competition-detail.module';
import { SubCompetitionDetailModule } from './sub-competition-detail/sub-competition-detail.module';

@Module({
  imports: [CompetitionOverviewsModule, CompetitionDetailModule, SubCompetitionDetailModule]
})
export class CompetitionsModule {}
