import {
    Controller,
    Get,
    Param,
    Query,
    Req,
    UseGuards
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchPaginateDto } from '../../../../../microservices/microservices.dto';
import { JwtAuthGuard } from '../../../../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../../shared/utils/helper.utils';
import { CompetitionSectionOverviewsService } from './competition-section-overviews.service';
import { VersusMatchDetailsService } from '../versus-match-details/versus-match-details.service';

export enum ESectionRound {
    OVERVIEW="OVERVIEW-ROUND",
    FINAL="FINAL-ROUND",
    THIRDPLACE="THIRD-PLACE",
    SEMI="SEMI-FINAL-ROUND",
    THIRD="THIRD-ROUND",
    SECOND="SECOND-ROUND"
}

@ApiTags('User Panel - Competition Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('competition')
export class CompetitionSectionOverviewsController {
    constructor(
        private competitionSectionOverviewsService: CompetitionSectionOverviewsService,
        private versusMatchDetailsService: VersusMatchDetailsService
    ){}
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section')
    async getSectionOverview(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.OVERVIEW,
            token
        ));
    }
    
    @Get(':competitionId/sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/find-by-id')
    async getSectionFindById(@Req() req: Request, @Param('competitionId') competitionId: number, @Param('subCompetitionId') subCompetitionId: number, @Param('competitionSectionId') competitionSectionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        const params = { competitionId, subCompetitionId, competitionSectionId }
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionFindById(params, token));
    }
    
    @Get(':competitionId/sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/knockout')
    async getSectionKnockOutById(@Req() req: Request, @Param('competitionId') competitionId: number, @Param('subCompetitionId') subCompetitionId: number, @Param('competitionSectionId') competitionSectionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        const params = { competitionId, subCompetitionId, competitionSectionId }
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionKnockOutById(params, token));
    }
    
    @Get(':competitionId/sub-competitions/:subCompetitionId/competition-sections/:competitionSectionId/group-stage')
    async getSectionGroupStageById(@Req() req: Request, @Param('competitionId') competitionId: number, @Param('subCompetitionId') subCompetitionId: number, @Param('competitionSectionId') competitionSectionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        const params = { competitionId, subCompetitionId, competitionSectionId }
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionGroupStageById(params, token));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/final')
    async getSectionFinalRound(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.FINAL,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/third-place-match')
    async getSectionThirdPlaceMatch(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.THIRDPLACE,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/semi-final')
    async getSectionSemiFinalRound(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.SEMI,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/third-round')
    async getSectionThirdRound(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.THIRD,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/second-round')
    async getSectionSecondRound(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionRound(
            competitionId,
            subCompetitionId,
            ESectionRound.SECOND,
            token
        ));
    }
    
    @Get(':competitionId/sub-competitions/:subCompetitionId/competition-sections/group-stage')
    async getSectionGroupStage(@Req() req: Request, @Param('competitionId') competitionId: number, @Param('subCompetitionId') subCompetitionId: number) {
        const token = await getHeaderAccessToken(req.headers);
        const params = { competitionId, subCompetitionId }
        return await getMicroserviceResponse(this.competitionSectionOverviewsService.getSectionGroupStage(params, token));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/competition-section/:competitionSectionId/versus-match/:versusMatchId')
    async getVersusMatchDetail(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number,
      @Param('competitionSectionId') competitionSectionId: number,
      @Param('versusMatchId') versusMatchId: number
    ) {
        const params = { competitionId, subCompetitionId, competitionSectionId, versusMatchId }
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.versusMatchDetailsService.getVersusMatchDetail(
            params,
            token
        ));
    }
}
