import { Module } from '@nestjs/common';
import { VersusMatchDetailsService } from '../versus-match-details/versus-match-details.service';
import { CompetitionSectionOverviewsController } from './competition-section-overviews.controller';
import { CompetitionSectionOverviewsService } from './competition-section-overviews.service';

@Module({
  controllers: [CompetitionSectionOverviewsController],
  providers: [CompetitionSectionOverviewsService, VersusMatchDetailsService]
})
export class CompetitionSectionOverviewsModule {}
