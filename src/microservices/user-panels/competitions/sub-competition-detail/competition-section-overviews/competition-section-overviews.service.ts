import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';
import { ESectionRound } from './competition-section-overviews.controller';

@Injectable()
export class CompetitionSectionOverviewsService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(CompetitionSectionOverviewsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }
    
    async getSectionRound(
        competitionId: number,
        subCompetitionId: number,
        sectionRound: ESectionRound,
        accessToken: string,
    ) {
        let url: string;
        switch (sectionRound) {
            case sectionRound = ESectionRound.OVERVIEW:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section`;
                break;
            case sectionRound = ESectionRound.FINAL:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/final`;
                break;
            case sectionRound = ESectionRound.THIRDPLACE:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/third-place-match`;
                break;
            case sectionRound = ESectionRound.SEMI:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/semi-final`;
                break;
            case sectionRound = ESectionRound.THIRD:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/third-round`;
                break;
            case sectionRound = ESectionRound.SECOND:
                url = `competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/second-round`;
                break;
            default:
                break;
        }
        return await getResponse(this.microservice.get(url, {
            headers: {
                Authorization: accessToken,
            }
        }));
    }
    
    async getSectionFindById(params: any, accessToken: string) {
        const { competitionId, subCompetitionId, competitionSectionId } = params;
        try {
          return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/${competitionSectionId}/find-by-id`,
            {
                headers: {
                    Authorization: accessToken
                }
            }))
        } catch(err) {
            this.logger.error(err);
            throw err;
        } 
    }
    
    async getSectionKnockOutById(params: any, accessToken: string) {
        const { competitionId, subCompetitionId, competitionSectionId } = params;
        try {
            return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/${competitionSectionId}/knockout`,
            {
                headers: {
                    Authorization: accessToken
                }
            }))
        } catch(err) {
            this.logger.error(err);
            throw err;
        } 
  }
  
  async getSectionGroupStageById(params: any, accessToken: string) {
    const { competitionId, subCompetitionId, competitionSectionId } = params;
    try {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/${competitionSectionId}/group-stage`,
            {
            headers: {
                Authorization: accessToken
            }
        }))
    } catch(err) {
        this.logger.error(err);
        throw err;
    } 
  }
  
  async getSectionGroupStage(params: any, accessToken: string) {
    const { competitionId, subCompetitionId } = params;
    try {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/group-stage`,
            {
            headers: {
                Authorization: accessToken
            }
        }))
    } catch(err) {
        this.logger.error(err);
        throw err;
    } 
  }
}
