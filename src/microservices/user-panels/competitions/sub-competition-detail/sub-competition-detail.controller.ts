import {
    Controller,
    Get,
    Param,
    Query,
    Req,
    UseGuards
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchPaginateDto, SearchPageDto } from '../../../../microservices/microservices.dto';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import { SubCompetitionDetailService } from './sub-competition-detail.service';

@ApiTags('User Panel - Competition Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('competition')
export class SubCompetitionDetailController {
    constructor(private subCompetitionDetailService: SubCompetitionDetailService) {}
    
    @Get(':competitionId/sub-competition/:subCompetitionId/banner')
    async getSubCompetitionBanner(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionDetailService.getSubCompetitionBanner(
            competitionId,
            subCompetitionId,
            token
        ));
    }

    @Get(':competitionId/sub-competition/:subCompetitionId/information')
    async getSubCompetitionInformation(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionDetailService.getSubCompetitionInformation(
            competitionId,
            subCompetitionId,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/team-in-sub-competition')
    async getTeamInSubCompetition(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number,
      @Query() pageDto: SearchPaginateDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionDetailService.getTeamInSubCompetition(
            competitionId,
            subCompetitionId,
            pageDto,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/images')
    async getImagesInSubCompetition(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number,
      @Query() pageDto: SearchPageDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionDetailService.getImagesInSubCompetition(
            competitionId,
            subCompetitionId,
            pageDto,
            token
        ));
    }
    
    @Get(':competitionId/sub-competition/:subCompetitionId/videos')
    async getVideosInSubCompetition(
      @Req() req: Request,
      @Param('competitionId') competitionId: number,
      @Param('subCompetitionId') subCompetitionId: number,
      @Query() pageDto: SearchPageDto,
    ) {
        const token = await getHeaderAccessToken(req.headers);
        return await getMicroserviceResponse(this.subCompetitionDetailService.getVideosInSubCompetition(
            competitionId,
            subCompetitionId,
            pageDto,
            token
        ));
    }

  }
  