import { Module } from '@nestjs/common';
import { CompetitionSectionOverviewsModule } from './competition-section-overviews/competition-section-overviews.module';
import { VersusMatchDetailsModule } from './versus-match-details/versus-match-details.module';
import { SubCompetitionDetailController } from './sub-competition-detail.controller';
import { SubCompetitionDetailService } from './sub-competition-detail.service';

@Module({
  imports: [CompetitionSectionOverviewsModule, VersusMatchDetailsModule],
  controllers: [SubCompetitionDetailController],
  providers: [SubCompetitionDetailService]
})
export class SubCompetitionDetailModule {}
