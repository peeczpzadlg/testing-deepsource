import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ISearchPaginate } from '../../../../microservices/microservices.interface';
import { IPaginationCompetition } from '../competition-detail/competition-detail.interface';

@Injectable()
export class SubCompetitionDetailService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(SubCompetitionDetailService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }
    
    async getSubCompetitionBanner(
        competitionId: number,
        subCompetitionId: number,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/banner`, {
            headers: {
                Authorization: accessToken,
            }
        }));
    }

    async getSubCompetitionInformation(
        competitionId: number,
        subCompetitionId: number,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/information`, {
            headers: {
                Authorization: accessToken,
            }
        }));
    }
    
    async getTeamInSubCompetition(
        competitionId: number,
        subCompetitionId: number,
        query: ISearchPaginate,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/team-in-sub-competition`, {
            headers: {
                Authorization: accessToken,
            },
            params: query
        }));
    }
    
    async getImagesInSubCompetition(
        competitionId: number,
        subCompetitionId: number,
        query: IPaginationCompetition,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/images`, {
            headers: {
                Authorization: accessToken,
            },
            params: query
        }));
    }
    
    async getVideosInSubCompetition(
        competitionId: number,
        subCompetitionId: number,
        query: IPaginationCompetition,
        accessToken: string,
    ) {
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/videos`, {
            headers: {
                Authorization: accessToken,
            },
            params: query
        }));
    }
}
