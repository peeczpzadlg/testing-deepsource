import { Module } from '@nestjs/common';
import { VersusMatchDetailsService } from './versus-match-details.service';

@Module({
  controllers: [],
  providers: [VersusMatchDetailsService]
})
export class VersusMatchDetailsModule {}
