import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';

@Injectable()
export class VersusMatchDetailsService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(VersusMatchDetailsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }
    
    async getVersusMatchDetail(
        params: any,
        accessToken: string,
    ) {
        const { competitionId, subCompetitionId, competitionSectionId, versusMatchId } = params;
        
        return await getResponse(this.microservice.get(`competition/${competitionId}/sub-competition/${subCompetitionId}/competition-section/${competitionSectionId}/versus-match/${versusMatchId}`, {
            headers: {
                Authorization: accessToken,
            }
        }));
    }
}
