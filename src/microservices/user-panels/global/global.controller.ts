import { Controller, Get, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import { checkAuthMicroservice, getHeaderAccessToken, getMicroserviceResponse } from 'src/shared/utils/helper.utils';
import { GlobalService } from './global.service';


@ApiTags('User Panel - Dropdown')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)

@Controller('global')
export class GlobalController {
  constructor(private globalService: GlobalService) {}
  @Get('position/player/all')
  async playerAll(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.globalService.getGlobalPosition(1);
      return new ResponseSuccess('successfully', data);
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('position/staff/all')
  async staffAll(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.globalService.getGlobalPosition(2);
      return new ResponseSuccess('successfully', data);
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('position/department/all')
  async departmentAll(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.globalService.getGlobalDepartment();
      return new ResponseSuccess('successfully', data);
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('team/all')
  async teamAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalTeamAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('nationality/all')
  async nationalityAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalNationalityAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('country/all')
  async countryAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalContryAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('province/all')
  async provinceAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalProvinceAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('local-type/all')
  async localTypeAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalLocalTypeAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('competition/all')
  async competitionAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalCompletitionAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('stadium/all')
  async statdiumAll(@Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      return await getMicroserviceResponse(
        this.globalService.getGlobalStadiumAll(token),
      );
    } catch (err) {
      throw new ResponseFailed(
        'something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  

 



}

// global/country/all
// global/province/all
// global/local-type/all

// competition/all
// stadium/all