import { Module } from '@nestjs/common';
import { GlobalService } from './global.service';
import { GlobalController } from './global.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';

@Module({
  imports:[TypeOrmModule.forFeature([GlobalConfigPositions,GlobalConfigDepartments])],
  providers: [GlobalService],
  controllers: [GlobalController]
})
export class GlobalModule {}
