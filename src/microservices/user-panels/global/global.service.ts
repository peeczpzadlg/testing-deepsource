import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';

@Injectable()
export class GlobalService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(GlobalService.name);
  constructor(
    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,
    @InjectRepository(GlobalConfigDepartments)
    private globalConfigDepartmentRepository: Repository<GlobalConfigDepartments>,
  ) {
    this.microservice = microserviceAxios();
  }
  async getGlobalPosition(global_config_position_type: number) {
    try {
      const globalConfig = await this.globalConfigPositionsRepository.createQueryBuilder('g')
      .where({global_config_position_type,active:true})
      .select(['id','global_config_position_name_th','global_config_position_name_en'])
      .orderBy('id', 'ASC')
      .getRawMany();
    let new_response = {
        position: globalConfig,
      };
      return new_response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getGlobalDepartment() {
    try {
      const globalConfig = await this.globalConfigDepartmentRepository.createQueryBuilder('g')
      .where({active:true})
      .select(['id','global_config_department_name_th','global_config_department_name_en'])
      .orderBy('id', 'ASC')
      .getRawMany();
    let new_response = {
        department: globalConfig,
      };
      return new_response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getGlobalTeamAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/team/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalNationalityAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/nationality/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalContryAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/country/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalProvinceAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/province/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalLocalTypeAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/local-type/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalCompletitionAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/competition/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }

  async getGlobalStadiumAll(accessToken:string) {
    return await getResponse(this.microservice.get(`global/stadium/all`, {
      headers: {
        Authorization: accessToken
      }
    }))
  }
}
