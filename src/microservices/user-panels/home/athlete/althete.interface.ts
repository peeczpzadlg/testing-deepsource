export interface IPlayerQuery {
  page?: number;
  size?: number;
  injuries: string;
}
