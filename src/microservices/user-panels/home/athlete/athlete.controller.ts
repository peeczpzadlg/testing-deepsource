import {
  Controller,
  Get,
  HttpStatus,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';

import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../../../../shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
} from '../../../../shared/utils/helper.utils';
import { AthleteService } from './athlete.service';
import { PlayerQueryDto } from './athlete.dto';

@ApiTags('User Panel - Home Overviews')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('players')
export class AthleteController {
  constructor(private athleteService: AthleteService) {}

  @Get()
  async getData(@Req() req: Request, @Query() query: PlayerQueryDto) {
    let response;
    const token = await getHeaderAccessToken(req.headers);
    let injuries: boolean = JSON.parse(query.injuries);
    try {
    
      let response;
      if (injuries) {
        response = await this.athleteService.getInjuries(query, token);
      } else {
        response = await this.athleteService.getPlayers(query, token);
      }
      const { data } = response;
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      console.log(err, '<--- err');
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }
}
