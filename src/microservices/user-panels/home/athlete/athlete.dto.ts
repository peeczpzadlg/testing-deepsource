import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { IPlayerQuery } from './althete.interface';

export class PlayerQueryDto extends SearchPageDto implements IPlayerQuery {
  @ApiPropertyOptional({ type: Boolean, default: false })
  @IsString()
  @IsOptional()
  injuries: string;
}
