import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountInjuries } from '../../../../personals/account-injuries/account-injuries.entity';
import { AccountSpecifications } from '../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPhysicalFitness } from '../../../../settings/primary/configs/entities/global-config-physical-fitness.entity';
import { GlobalConfigPositions } from '../../../../settings/primary/configs/entities/global-config-positions.entity';
import { AthleteController } from './athlete.controller';
import { AthleteService } from './athlete.service';

@Module({
  controllers: [AthleteController],
  providers: [AthleteService],
  imports: [
    TypeOrmModule.forFeature([
      AccountSpecifications,
      GlobalConfigPositions,
      GlobalConfigPhysicalFitness,
      AccountInjuries,
    ])
  ],
})
export class AthleteModule {}
