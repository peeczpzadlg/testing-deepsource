import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';

import { AccountInjuries } from '../../../../personals/account-injuries/account-injuries.entity';
import { AccountSpecifications } from '../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from '../../../../settings/primary/configs/entities/global-config-positions.entity';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { IPlayerQuery } from './althete.interface';

@Injectable()
export class AthleteService {
  private microservice: AxiosInstance;

  constructor(
    @InjectRepository(AccountSpecifications)
    private accountSpecificationsRepository: Repository<AccountSpecifications>,
    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,

    @InjectRepository(AccountInjuries)
    private accountInjuriesRepository: Repository<AccountInjuries>,
  ) {
    this.microservice = microserviceAxios();
  }

  async getPlayers(params: IPlayerQuery, accessToken: string) {
    // console.log('xxx',params.injuries);
    const res = await this.microservice.post(
      '/players',null,
      {
        headers: {
          Authorization: accessToken,
        },
        params,
      },
    );
    const { data: responseFromMS } = res;
    const {
      data: {
        account: { data },
        account,
      },
    } = responseFromMS;
    let results = {
      ...account,
    };

    if(!data) return responseFromMS;

    const newData = await Promise.all(
      data.map(async d => {
        const {
          account_specifications: account_specifications_id,
          account_id,
        } = d;
        const account_specification_data = await this.accountSpecificationsRepository.findOne(
          account_specifications_id,
          {
            relations: ['account_physical_fitness'],
          },
        );

        const {
          global_config_positions: global_config_position_ids,
          account_physical_fitness,
        } = account_specification_data;

        let global_config_positions = [];

        if (global_config_position_ids?.length > 0) {
          global_config_positions = await this.globalConfigPositionsRepository
            .createQueryBuilder('globalConfigPosition')
            .where(
              'globalConfigPosition.id IN (:...global_config_position_ids)',
              { global_config_position_ids },
            )
            .getMany();
        }

        // injury
        const account_injuries = await this.accountInjuriesRepository.find({
          account_id,
        });

        return {
          ...d,
          global_config_positions,
          account_injuries,
          global_config_physical_fitness: account_physical_fitness,
        };
      }),
    );
    responseFromMS.data.account.data = newData
    results = {
      ...responseFromMS
    };

    return results;
  }

  async getInjuries(params: SearchPageDto, accessToken: string) {
    const injuries = await this.accountInjuriesRepository.find({
      where: {
        account_injury_present: true,
        deleted: false,
      },
      order: {
        account_injury_date: 'DESC',
      },
      select: ['account_id'],
    });
    const res = await this.microservice.post(
      '/players',
      {
        account_ids: injuries.map(ij => ij.account_id),
      },
      {
        headers: {
          Authorization: accessToken,
        },
        params,
      },
    );

    const { data: responseFromMS } = res;
    const {
      data: {
        account: { data },
        account,
      },
    } = responseFromMS;
    let results = {
      ...account,
    };
    if(!data) return responseFromMS;

    const newData = await Promise.all(
      data.map(async d => {
        const {
          account_specifications: account_specifications_id,
          account_id,
        } = d;
        const account_specification_data = await this.accountSpecificationsRepository.findOne(
          account_specifications_id,
          {
            relations: ['account_physical_fitness'],
          },
        );

        const {
          global_config_positions: global_config_position_ids,
          account_physical_fitness,
        } = account_specification_data;

        let global_config_positions = [];

        if (global_config_position_ids?.length > 0) {
          global_config_positions = await this.globalConfigPositionsRepository
            .createQueryBuilder('globalConfigPosition')
            .where(
              'globalConfigPosition.id IN (:...global_config_position_ids)',
              { global_config_position_ids },
            )
            .getMany();
        }

        // injury
        const account_injuries = await this.accountInjuriesRepository.find({
          where:{
            account_id,account_injury_present:true,deleted:false
          },
      
        }

        );

        return {
          ...d,
          global_config_positions,
          account_injuries,
          global_config_physical_fitness: account_physical_fitness,
        };
      }),
    );
    responseFromMS.data.account.data = newData
    results = {
      ...responseFromMS
    };

    return results;
  }
}
