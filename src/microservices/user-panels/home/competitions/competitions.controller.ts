import { HttpStatus } from '@nestjs/common';
import { Controller, Get, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../../../../shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../../../shared/utils/helper.utils';
import { CompetitionQueryDto } from './competitions.dto';
import { CompetitionsService } from './competitions.service';

@ApiTags('User Panel - Home Overviews')
@Controller('/main/competitions')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class CompetitionsController {
  constructor(private competitionsService: CompetitionsService) {}

  @Get()
  async getData(@Req() req: Request, @Query() query: CompetitionQueryDto) {

    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.competitionsService.getCompetitions(query, token)
    );
    
  }

}
