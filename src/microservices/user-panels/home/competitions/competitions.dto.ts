import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { IComptitionQuery } from './competitions.interface';


export class CompetitionQueryDto extends SearchPageDto implements IComptitionQuery {
  @ApiPropertyOptional({ type: Boolean, default: false })
  @IsString()
  @IsOptional()
  competitions: string;

  @ApiPropertyOptional({ type: Boolean, default: false })
  @IsString()
  @IsOptional()
  latest: string;
}
