export interface IComptitionQuery {
  page?: number;
  size?: number;
  matches?: string;
  latest?: string;
}
