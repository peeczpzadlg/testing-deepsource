import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { CompetitionQueryDto } from './competitions.dto';

@Injectable()
export class CompetitionsService {
  private microservice: AxiosInstance;
  constructor() {
    this.microservice = microserviceAxios();
  }
  async getCompetitions(
    params: CompetitionQueryDto,
    accessToken: string,
  ) {
    const newparam = {
        ...params,
        matches:params.competitions
    }
    return await getResponse(
      this.microservice.get(`/main/competitions`, {
        headers: {
          Authorization: accessToken,
        },params:newparam
      }),
    );
  }


}
