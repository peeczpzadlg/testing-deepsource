import { Module } from '@nestjs/common';
import { AthleteModule } from './athlete/athlete.module';
import { StaffsModule } from './staffs/staffs.module';
import { TeamsModule } from './teams/teams.module';
import { CompetitionsModule } from './competitions/competitions.module';
import { LatestVersusMatchModule } from './latest-versus-match/latest-versus-match.module';

@Module({
  imports: [AthleteModule, StaffsModule, TeamsModule, CompetitionsModule, LatestVersusMatchModule]
})
export class HomeModule {}
