import { Controller, Get, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';

import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import {
  ResponseFailed,
  ResponseSuccess,
} from '../../../../shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../../../shared/utils/helper.utils';
import { StaffsService } from './staffs.service';
import { StaffsQueryDto } from './staffs.dto';

@ApiTags('User Panel - Home Overviews')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('staffs')
export class StaffsController {
  constructor(private staffService: StaffsService) {}

  @Get()
  async getData(@Req() req: Request, @Query() query: StaffsQueryDto) {

    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.staffService.getStaffs(query, token)
    );
  }
}
