import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from '../../../../microservices/microservices.dto';

import { IStaffQuery } from './starffs.interface';


export class StaffsQueryDto extends SearchPageDto implements IStaffQuery {
// 
}
