import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';
import { StaffsController } from './staffs.controller';
import { StaffsService } from './staffs.service';

@Module({
    imports: [TypeOrmModule.forFeature([AccountSpecifications,GlobalConfigDepartments])],
    controllers: [StaffsController],
    providers: [StaffsService]
})
export class StaffsModule {}
