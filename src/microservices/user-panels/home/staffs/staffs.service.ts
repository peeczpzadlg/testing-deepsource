import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { response } from 'express';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';
import { getResponse } from 'src/shared/utils/helper.utils';
import { Repository } from 'typeorm';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';

@Injectable()
export class StaffsService {
  private microservice: AxiosInstance;

  @InjectRepository(AccountSpecifications)
  private accountSpecificationRepository:Repository<AccountSpecifications>

  @InjectRepository(GlobalConfigDepartments)
  private globalConfigDepartmentRepository:Repository<GlobalConfigDepartments>


  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStaffs(query: SearchPageDto, accessToken: string) {
    const responseFromMS =  await getResponse(this.microservice.get('/staffs', {
      headers: {
        Authorization: accessToken,
      },
      params: query,
    }))

    const { data: { account: {data} } } = responseFromMS;
    if (!data || data.length < 1) {
        return responseFromMS
    }
    let new_data = [];
    await Promise.all(data.map(async d => {
      const { account_id, account_specifications } = d;
      let global_config_positions = [];
      let global_config_departments = null;
      if (account_specifications) {
        const Specification = await this.accountSpecificationRepository.findOne({
          where: {id:account_specifications},
          relations:['global_config_departments']
        });
        console.log(Specification);

        if(Specification){
          if(Specification.global_config_positions && Specification.global_config_positions.length>0){
            global_config_positions = await this.globalConfigDepartmentRepository
            .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
              select *
              from unnest(array[${Specification.global_config_positions}]) with ordinality
            ) as x (id, ordering) on a.id = x.id where a.id in (${Specification.global_config_positions}) order by x.ordering;`);
          }
          global_config_departments = Specification.global_config_departments
        }
      }

      new_data.push({
        ...d,
        global_config_departments,
        global_config_positions: global_config_positions || [],
      });
    }));
    
    const newResponse = responseFromMS;
    newResponse.data.account.data = new_data;
    return responseFromMS;
  }
}
