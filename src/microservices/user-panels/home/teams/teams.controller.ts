import {
  Controller,
  Get,
  HttpStatus,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from '../../../../shared/utils/helper.utils';
import { SearchTeamsDto } from './teams.dto';
import { TeamsService } from './teams.service';
import { Request } from 'express';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';

@ApiTags('User Panel - Home Overviews')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @Get()
  async getData(@Req() req: Request, @Query() query: SearchTeamsDto) {
    const token = await getHeaderAccessToken(req.headers);
     return await getMicroserviceResponse(this.teamsService.getTeams(query, token));
  }
}
