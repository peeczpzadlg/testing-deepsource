import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from 'src/microservices/microservices.dto';
import { ITeamOverview } from './teams.interface';

export class SearchTeamsDto extends SearchPageDto implements ITeamOverview {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_local_types: number;

  @ApiPropertyOptional({ type: Boolean, default: false })
  @IsString()
  @IsOptional()
  total: string;

  
}
