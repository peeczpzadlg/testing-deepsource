export interface ITeamOverview {
    page?: number,
    size?: number,
    global_config_local_types?: number
    nation?:string;
    total?:string;
}