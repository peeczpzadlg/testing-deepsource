import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { ITeamOverview } from './teams.interface';
import { SearchPageDto } from '../../../../microservices/microservices.dto';

@Injectable()
export class TeamsService {
    private microservice: AxiosInstance;
    private logger: Logger = new Logger(TeamsService.name);

    constructor() {
        this.microservice = microserviceAxios();
    }

    async getNationTeamsTotal(params: ITeamOverview, accessToken: string) {
        return await getResponse(this.microservice.get('home/overview/teams/nations/total',
            {
                headers: {
                    Authorization: accessToken
                },
                params
            }))
    }

    async getTeams(params: ITeamOverview, accessToken: string) {
        return await getResponse(this.microservice.get('home/overview/teams',
            {
                headers: {
                    Authorization: accessToken
                },
                params
            }))
    }

}
