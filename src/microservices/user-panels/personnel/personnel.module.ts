import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { PlayersModule } from './players/players.module';
import { StaffsModule } from './staffs/staffs.module';
import { TeamsController } from './teams/teams.controller';
import { TeamsService } from './teams/teams.service';

@Module({
  imports: [PlayersModule, StaffsModule,TypeOrmModule.forFeature([GlobalConfigDepartments,GlobalConfigPositions])],
  controllers: [TeamsController],
  providers: [TeamsService],
})
export class PersonnelModule {}
