import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { EInjuryLevel } from 'src/shared/enum';
import { EGender } from '../../../../../shared/enum/gender.enum';
import { SearchPageDto } from '../../../../../microservices/microservices.dto';

export class GetDefendersQueryDTO extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_id?: number;
}

export class GetPlayerOverviewQueryDTO extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  minAge?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  maxAge?: number;

  @ApiPropertyOptional({
    enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER],
  })
  @IsOptional()
  @IsEnum(EGender)
  gender?: EGender;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  nationality?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_national_status?:number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  provinces?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_positions?: number;

  @ApiPropertyOptional({
    enum: [EInjuryLevel.MINOR, EInjuryLevel.MODERATE, EInjuryLevel.SERIOUSLY],
  })
  @IsOptional()
  @IsString()
  account_injury_level?: EInjuryLevel;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_physical_fitness_percentage?: number;
}



export class GetPlayerOverviewQueryInjuriesDTO extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  minAge?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  maxAge?: number;

  @ApiPropertyOptional({
    enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER],
  })
  @IsOptional()
  @IsEnum(EGender)
  gender?: EGender;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  nationality?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  team_national_status?:number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  provinces?: number;


}