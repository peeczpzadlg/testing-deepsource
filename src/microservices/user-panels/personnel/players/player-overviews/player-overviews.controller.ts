import {
  Controller,
  Get,
  HttpStatus,
  Param,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SearchAccountDto } from '../../../../../microservices/account/dto/account.dto';
import { JwtAuthGuard } from '../../../../../auth/jwt-auth.guard';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../../../../../shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
} from '../../../../../shared/utils/helper.utils';
import { PlayerOverviewsService } from './player-overviews.service';
import { AxiosInstance } from 'axios';
import { GetPlayerOverviewQueryDTO, GetPlayerOverviewQueryInjuriesDTO } from './player-overview.dto';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';
import { SearchPageDto } from '../../../../../microservices/microservices.dto';

@ApiTags('User Panel - Players Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('/player/overview')
export class PlayerOverviewsController {
  private microservice: AxiosInstance;

  constructor(private playerOverviewsService: PlayerOverviewsService) {
    this.microservice = microserviceAxios();
  }

  @Get()
  async getOverview(
    @Req() req: Request,
    @Query() query: GetPlayerOverviewQueryDTO,
  ) {
    try {
     
      const token = await getHeaderAccessToken(req.headers);
      const res = await this.playerOverviewsService.getPlayerOverview(query,token)
      return new ResponseSuccess('Successfully', res);
    } catch (err) {
      throw new ResponseFailed(err.message || 'Something went wrong', err.error_code || 400, '');
    }
  }
   @Get('injuries/all')
   async getOverviewInjuries(
    @Req() req: Request,
    @Query() query: GetPlayerOverviewQueryInjuriesDTO,
  ) {
    try {
     
      const token = await getHeaderAccessToken(req.headers);
      const res = await this.playerOverviewsService.getPlayerOverviewInjuries(query,token)
      return new ResponseSuccess('Successfully', res);
    } catch (err) {
      throw new ResponseFailed(err.message || 'Something went wrong', err.error_code || 400, '');
    }
  }

  @Get('midfielder')
  async getMidfielders(
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const token = await getHeaderAccessToken(req.headers);

      const data = await this.playerOverviewsService.getMidfielders(query, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      console.log(err, '<--- err');
      throw new ResponseFailed(err.message || 'Something went wrong', err.error_code || 400, '');
    }
  }

  // team
  @Get('team/:team_id/defender')
  async getTeamDefenders(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const token = await getHeaderAccessToken(req.headers);

      const data = await this.playerOverviewsService.getDefenders(team_id, query, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      console.log(err, '<--- err');
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/goalkeeper')
  async getGoalkeeper(
    @Req() req: Request,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getGoalkeeper(searchAccountDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/defender')
  async getDefender(
    @Req() req: Request,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getDefender(searchAccountDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/forward')
  async getForward(
    @Req() req: Request,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getForward(searchAccountDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/team/:team_id/goalkeeper')
  async getTeamGoalkeeper(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getTeamGoalkeeper(
        team_id,
        searchAccountDto,
        token,
      );
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/team/:team_id/forward')
  async getTeamForward(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getTeamForward(
        team_id,
        searchAccountDto,
        token,
      );
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/team/:team_id/midfielder')
  async getTeamMidfielder(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() searchAccountDto: SearchPageDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.playerOverviewsService.getTeamMidfielder(
        team_id,
        searchAccountDto,
        token,
      );
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }
}
