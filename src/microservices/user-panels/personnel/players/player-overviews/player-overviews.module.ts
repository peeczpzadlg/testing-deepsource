import { Module } from '@nestjs/common';
import { PlayerOverviewsService } from './player-overviews.service';
import { PlayerOverviewsController } from './player-overviews.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountInjuries } from 'src/personals/account-injuries/account-injuries.entity';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPhysicalFitness } from 'src/settings/primary/configs/entities/global-config-physical-fitness.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';

@Module({
  controllers: [PlayerOverviewsController],
  providers: [PlayerOverviewsService],
  imports: [
    TypeOrmModule.forFeature([
      AccountSpecifications,
      GlobalConfigPositions,
      GlobalConfigPhysicalFitness,
      AccountInjuries,
      AccountSpecifications,
      AccountInjuries,
      GlobalConfigPositions,
      GlobalConfigPhysicalFitness,
    ]),
  ],
})
export class PlayerOverviewsModule {}
