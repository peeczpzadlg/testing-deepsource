import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { SearchAccountDto } from 'src/microservices/account/dto/account.dto';
import { SearchPageDto } from 'src/microservices/microservices.dto';

import { AccountInjuries } from 'src/personals/account-injuries/account-injuries.entity';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPhysicalFitness } from 'src/settings/primary/configs/entities/global-config-physical-fitness.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { EMainPosition } from 'src/shared/enum';
import { convertAge, getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { In, Repository } from 'typeorm';
import { GetPlayerOverviewQueryDTO, GetPlayerOverviewQueryInjuriesDTO } from './player-overview.dto';

@Injectable()
export class PlayerOverviewsService {
  private microservice: AxiosInstance;
  constructor(
    @InjectRepository(AccountSpecifications)
    private accountSpecificationsRepository: Repository<AccountSpecifications>,

    @InjectRepository(AccountInjuries)
    private accountInjuriesRepository: Repository<AccountInjuries>,

    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,
    @InjectRepository(GlobalConfigPhysicalFitness)
    private globalConfigPhtsicalFitness: Repository<
      GlobalConfigPhysicalFitness
    >,
  ) {
    this.microservice = microserviceAxios();
  }

  async getGoalkeeper(searchAccountDto: SearchPageDto, accessToken: string) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'GOALKEEPER',
      })
      .getRawMany();
    // console.log('Goalkeeper', getIds);
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        GOALKEEPER: ids,
      },
    };
    // console.log('aaa',account_specifications)
    try {
      const responseFromMS = await getResponse(
        this.microservice.post(
          `player/overview/goalkeeper`,
          account_specifications,
          {
            headers: {
              Authorization: accessToken,
            },
            params: searchAccountDto,
          },
        ),
      );
      // console.log('eee', responseFromMS);
      const newData = await this.genData(getIds, responseFromMS);
      const newResponse = {
        ...responseFromMS,
        data: newData,
      };
      return newResponse;
    } catch (err) {
      throw false;
    }
  }

  async getForward(searchAccountDto: SearchPageDto, accessToken: string) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'FORWARD',
      })
      .getRawMany();
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        FORWARD: ids,
      },
    };
    // console.log(account_specifications);
    const responseFromMS = await getResponse(
      this.microservice.post(
        `player/overview/forward`,
        account_specifications,
        {
          headers: {
            Authorization: accessToken,
          },
          params: searchAccountDto,
        },
      ),
    );
    const newData = await this.genData(getIds, responseFromMS);
    const newResponse = {
      ...responseFromMS,
      data: newData,
    };
    return newResponse;
  }

  async getDefender(searchAccountDto: SearchPageDto, accessToken: string) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'DEFENDER',
      })
      .getRawMany();
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        DEFENDER: ids,
      },
    };
    // console.log(account_specifications);
    const responseFromMS = await getResponse(
      this.microservice.post(
        `player/overview/defender`,
        account_specifications,
        {
          headers: {
            Authorization: accessToken,
          },
          params: searchAccountDto,
        },
      ),
    );
    const newData = await this.genData(getIds, responseFromMS);
    const newResponse = {
      ...responseFromMS,
      data: newData,
    };
    return newResponse;
  }

  async getTeamGoalkeeper(
    team_id: number,
    searchAccountDto: SearchPageDto,
    accessToken: string,
  ) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'GOALKEEPER',
      })
      .getRawMany();
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        GOALKEEPER: ids,
      },
    };
    const responseFromMS = await getResponse(
      this.microservice.post(
        `player/overview/team/${team_id}/goalkeeper`,
        account_specifications,
        {
          headers: {
            Authorization: accessToken,
          },
          params: searchAccountDto,
        },
      ),
    );
    const newData = await this.genData(getIds, responseFromMS);
    const newResponse = {
      ...responseFromMS,
      data: newData,
    };
    return newResponse;
  }

  async getTeamMidfielder(
    team_id: number,
    searchAccountDto: SearchPageDto,
    accessToken: string,
  ) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'MIDFIELDER',
      })
      .getRawMany();
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        MIDFIELDER: ids,
      },
    };
    const responseFromMS = await getResponse(
      this.microservice.post(
        `player/overview/team/${team_id}/midfielder`,
        account_specifications,
        {
          headers: {
            Authorization: accessToken,
          },
          params: searchAccountDto,
        },
      ),
    );
    const newData = await this.genData(getIds, responseFromMS);
    const newResponse = {
      ...responseFromMS,
      data: newData,
    };
    return newResponse;
  }

  async getTeamForward(
    team_id: number,
    searchAccountDto: SearchPageDto,
    accessToken: string,
  ) {
    const getIds = await this.accountSpecificationsRepository
      .createQueryBuilder('account_specifications')
      .select(['id', 'global_config_positions', 'account_physical_fitness'])
      .where('account_specification_main_position = :position', {
        position: 'FORWARD',
      })
      .getRawMany();
    const ids = [];
    getIds.map(v => {
      ids.push(v.id);
    });
    const account_specifications = {
      account_specifications: {
        FORWARD: ids,
      },
    };
    const responseFromMS = await getResponse(
      this.microservice.post(
        `player/overview/team/${team_id}/forward`,
        account_specifications,
        {
          headers: {
            Authorization: accessToken,
          },
          params: searchAccountDto,
        },
      ),
    );
    const newData = await this.genData(getIds, responseFromMS);
    const newResponse = {
      ...responseFromMS,
      data: newData,
    };
    return newResponse;
  }

  async genData(getIds, responseFromMS) {
    const {
      data: {
        account: { data },
      },
    } = responseFromMS;
    if (!Array.isArray(data) || data.length < 1) {
      return { ...responseFromMS.data };
    }
    const newData = await Promise.all(
      data.map(async d => {
        const { account_specifications, account_id } = d;
        const account_injuries = await this.accountInjuriesRepository.findOne({
          where: {
            account_id,
            account_injury_present: true,
          },
          select: [
            'id',
            'account_injury_name_th',
            'account_injury_name_en',
            'account_injury_level',
            'account_injury_recovery_time',
            'account_injury_recovery_time_unit',
            'account_injury_present',
          ],
        });
        let global_config_position_ids = [];
        let global_physical_fitness_id;
        await getIds.map(x => {
          if (x.id == account_specifications) {
            global_config_position_ids = x.global_config_positions;
            global_physical_fitness_id = x.account_physical_fitness;
          }
        });
        const global_config_positions = await this
          .globalConfigPositionsRepository
          .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
            select *
            from unnest(array[${global_config_position_ids}]) with ordinality
        ) as x (id, ordering) on a.id = x.id where a.id in (${global_config_position_ids}) order by x.ordering;`);
        // console.log(global_config_positions);

        const global_config_physical_fitness = await this.globalConfigPhtsicalFitness.findOne(
          {
            where: { id: global_physical_fitness_id },
            select: [
              'id',
              'global_config_physical_fitness_name_th',
              'global_config_physical_fitness_name_en',
              'global_config_physical_fitness_percentage',
            ],
          },
        );
        return {
          ...d,
          global_config_positions,
          global_config_physical_fitness,
          account_injuries,
        };
      }),
    );
    return newData;
  }

  async mapWithPositionsAndInjuries(data) {
    const newData = await Promise.all(
      data.map(async d => {
        const {
          account_specifications: account_specifications_id,
          account_id,
        } = d;
        const account_specification_data = await this.accountSpecificationsRepository.findOne(
          account_specifications_id,
          {
            relations: ['account_physical_fitness'],
          },
        );

        const {
          global_config_positions: global_config_position_ids,
          account_physical_fitness,
        } = account_specification_data;

        let global_config_positions = [];

        if (global_config_position_ids?.length > 0) {
          global_config_positions = await this.globalConfigPositionsRepository
            .createQueryBuilder('globalConfigPosition')
            .where(
              'globalConfigPosition.id IN (:...global_config_position_ids)',
              { global_config_position_ids },
            )
            .getMany();
        }

        // injury
        const account_injuries = await this.accountInjuriesRepository.findOne({
          account_id,
        });

        return {
          ...d,
          global_config_positions,
          account_injuries,
          global_config_physical_fitness: account_physical_fitness,
        };
      }),
    );

    return newData;
  }

  async getMidfielders(params: any, accessToken: string) {
    const midfielders = await this.accountSpecificationsRepository.find({
      account_specification_main_position: EMainPosition.MIDFIELDER,
    });

    const res = await this.microservice.post(
      '/player/overview/midfielder',
      {
        account_specifications: {
          MIDFIELDER: midfielders?.map(d => d.id),
        },
      },
      {
        headers: {
          Authorization: accessToken,
        },
        params,
      },
    );
    const { data: responseFromMS } = res;
    const {
      data: {
        account: { data },
        account,
      },
    } = responseFromMS;

    const newData = await this.mapWithPositionsAndInjuries(data);

    return {
      ...account,
      data: newData,
    };
  }

  async getDefenders(team_id: number, params: any, accessToken: string) {
    const defenders = await this.accountSpecificationsRepository.find({
      account_specification_main_position: EMainPosition.DEFENDER,
    });

    const res = await this.microservice.post(
      `player/overview/team/${team_id}/defender`,
      {
        account_specifications: {
          DEFENDER: defenders?.map(d => d.id),
        },
      },
      {
        headers: {
          Authorization: accessToken,
        },
        params,
      },
    );
    const { data: responseFromMS } = res;
    const {
      data: {
        account: { data },
        account,
      },
    } = responseFromMS;

    const newData = await this.mapWithPositionsAndInjuries(data);

    return {
      ...account,
      data: newData,
    };
  }

  async getPlayerOverview(params: GetPlayerOverviewQueryDTO, token: string) {


    const getSpecification = this.accountSpecificationsRepository.createQueryBuilder('account_specification')
    .leftJoin('global_config_physical_fitness','global_fitness','global_fitness.id = account_specification.account_physical_fitness')
    .select(['account_specification.id']).where({active:true,deleted:false})

    if(params.global_config_positions){
      getSpecification.andWhere(`account_specification.global_config_positions::jsonb ? '${params.global_config_positions}'`);
    }
    if(params.global_config_physical_fitness_percentage){
      getSpecification.andWhere(`global_config_physical_fitness_percentage::integer <= '${params.global_config_physical_fitness_percentage}'::integer`);
    }
    let account_injuries = [];
    if(params.account_injury_level){
      account_injuries = await this.accountInjuriesRepository.find({
        where:{account_injury_level:params.account_injury_level,active:true,deleted:false},
        select:['account_id']
      })
    }
   

    const account_specification = await getSpecification.getMany();
    const specification_ids = account_specification.map(a => a.id * 1);
    const account_ids = account_injuries.map(a => a.account_id * 1);

    const body = {
      "account_ids":account_ids,
      "specification_ids":specification_ids
    }
    const responseFromMS = await getResponse(this.microservice.post('/player/overview', body, {
      headers: {
        Authorization: token,
      },
      params
    }));

    // return responseFromMS;
    const { data: { account: {data} } } = responseFromMS;
    // console.log(data);
    // if(!account) return responseFromMS.data;
    // return responseFromMS.data.account.data.data;
    if (!data || data.length < 1) {
        return responseFromMS.data
    }
    let new_data = [];
    await Promise.all(data.map(async d => {
      const { account_id, account_specifications } = d;
      // console.log(account_specifications);
      let global_config_positions = [];
      let global_config_physical_fitness = null;
      let new_account_injuries = [];
      if (account_specifications) {
        const Specification = await this.accountSpecificationsRepository.findOne({
          where:{id:account_specifications},
          relations:['account_physical_fitness']
        });
        // console.log(getSpecification);
        // console.log(Specification);
        if(Specification){
          if(Specification.global_config_positions && Specification.global_config_positions.length>0){
            global_config_positions = await this.globalConfigPositionsRepository
            .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
              select *
              from unnest(array[${Specification.global_config_positions}]) with ordinality
            ) as x (id, ordering) on a.id = x.id where a.id in (${Specification.global_config_positions}) order by x.ordering;`);
          }
          console.log(Specification);
          global_config_physical_fitness = Specification.account_physical_fitness
          // if(Specification.account_physical_fitness){
          //   global_config_physical_fitness = await this.globalConfigPhtsicalFitness.findOne({id:Specification.account_physical_fitness});
          //   console.log(global_config_physical_fitness);
          // }
          
        }
      }
      if(account_id){
        new_account_injuries = await this.accountInjuriesRepository.find({
            where:{account_id:account_id}
        })
      }
      new_data.push({
        ...d,
        account_injuries:new_account_injuries,
        global_config_physical_fitness:global_config_physical_fitness,
        global_config_positions: global_config_positions || [],
      });
    }));
    
    const newResponse = responseFromMS;
    newResponse.data.account.data = new_data;
    return newResponse.data;
  }

  async getPlayerOverviewInjuries(params: GetPlayerOverviewQueryInjuriesDTO, token: string) {
   
    const account_injuries = await this.accountInjuriesRepository.find(
      {
        where:{active:true,deleted:false},
        select:['account_id']
      }
    )
   

    const account_ids = account_injuries.map(a => a.account_id * 1);

    const body = {
      "account_ids":account_ids,
      "specification_ids":[]
    }
    const responseFromMS = await getResponse(this.microservice.post('/player/overview', body, {
      headers: {
        Authorization: token,
      },
      params
    }));

    // return responseFromMS;
    const { data: { account: {data} } } = responseFromMS;

    if (!data || data.length < 1) {
        return responseFromMS.data
    }
    let new_data = [];
    await Promise.all(data.map(async d => {
      const { account_id, account_specifications } = d;
      // console.log(account_specifications);
      let global_config_positions = [];
      let global_config_physical_fitness = null;
      let new_account_injuries = [];
      if (account_specifications) {
        const Specification = await this.accountSpecificationsRepository.findOne({
          where:{id:account_specifications},
          relations:['account_physical_fitness']
        });
        // console.log(getSpecification);
        // console.log(Specification);
        if(Specification){
          if(Specification.global_config_positions && Specification.global_config_positions.length>0){
            global_config_positions = await this.globalConfigPositionsRepository
            .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
              select *
              from unnest(array[${Specification.global_config_positions}]) with ordinality
            ) as x (id, ordering) on a.id = x.id where a.id in (${Specification.global_config_positions}) order by x.ordering;`);
          }
          console.log(Specification);
          global_config_physical_fitness = Specification.account_physical_fitness
          // if(Specification.account_physical_fitness){
          //   global_config_physical_fitness = await this.globalConfigPhtsicalFitness.findOne({id:Specification.account_physical_fitness});
          //   console.log(global_config_physical_fitness);
          // }
          
        }
      }
      if(account_id){
        new_account_injuries = await this.accountInjuriesRepository.find({
            where:{account_id:account_id}
        })
      }
      new_data.push({
        ...d,
        account_injuries:new_account_injuries,
        global_config_physical_fitness:global_config_physical_fitness,
        global_config_positions: global_config_positions || [],
      });
    }));
    
    const newResponse = responseFromMS;
    newResponse.data.account.data = new_data;
    return newResponse.data;
  }
}
