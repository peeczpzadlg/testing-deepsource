import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SearchPageDto } from 'src/microservices/microservices.dto';
import { PaginationQuery } from 'src/shared/decorators/search-page-query.decorator';
import { IPaginateQuery, PaginateQueryDto } from 'src/shared/interfaces/api-interfaces';
import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from 'src/shared/utils/helper.utils';
import { PlayersService } from './players.service';

@ApiTags('User Panel - Players Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('player')
export class PlayersController {
  constructor(private playersService: PlayersService) {}

  @Get(':account_id/banner')
  async getAccountBanner(
    @Param('account_id') account_id: number,
    @Req() req: Request,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);

      const data = await this.playersService.getAccountBanner(account_id, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/information')
  async getAccountInfo(
    @Param('account_id') account_id: number,
    @Req() req: Request,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.playersService.getAccountInfo(account_id, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  // team
  @Get(':account_id/team_history/nation')
  async getHistoryOfNationTeam(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 10 }) query: SearchPageDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.playersService.getHistoryOfNationTeam(
        account_id,
        token,
        query,
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/team_history/club')
  async getHistoryOfClubTeam(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    try {
      const data = await this.playersService.getHistoryOfClubTeam(account_id,token,query,);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message || 'Something when wrong', err.error_code || 400, '');
    }
  
  }

  // honors
  @Get(':account_id/honors/nation')
  async getHonorsOfNation(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.playersService.getHonorsWithKey(
        account_id,
        'account_honor_nations',
        query,
        token,
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/honors/club')
  async getHonorsOfClub(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.playersService.getHonorsWithKey(
        account_id,
        'account_honor_clubs',
        query,
        token
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/honors/personal')
  async getHonorsOfPersonal(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @Query() query: SearchPageDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.playersService.getHonorsWithKey(
        account_id,
        'account_honor_personals',
        query,
        token
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  // injuries
  @Get(':account_id/injuries/current')
  async getCurrentInjuries(@Param('account_id') account_id: number,@PaginationQuery({ size: 8 }) query: SearchPageDto) {
    try {
      const data = await this.playersService.getCurrentInjuries(account_id,query);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/injuries')
  async getInjuries(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 10 }) query: SearchPageDto,
  ) {
    try {
      const data = await this.playersService.getInjuries(account_id, query);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  // penalties
  @Get(':account_id/penalties/current')
  async getCurrentPenalties(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 10 }) query: SearchPageDto,
  ) {
    try {
      const data = await this.playersService.getPenalties(account_id, query, true);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/penalties')
  async getPenalties(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 10 }) query: SearchPageDto,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.playersService.getPenalties(account_id, query, false);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  // images
  @Get(':account_id/images')
  async getImages(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 12 }) query: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.playersService.getImages(account_id, query, token)
    );
    
  }

  // videos
  @Get(':account_id/videos')
  async getVideos(
    @Param('account_id') account_id: number,
    @Req() req: Request,
    @PaginationQuery({ size: 4 }) query: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.playersService.getVideos(account_id, query, token)
    );
  }
}
