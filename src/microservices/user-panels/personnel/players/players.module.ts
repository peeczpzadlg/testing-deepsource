import { Module } from '@nestjs/common';
import { PlayerOverviewsModule } from './player-overviews/player-overviews.module';
import { PlayerDetailModule } from './player-detail/player-detail.module';
import { PlayersController } from './players.controller';
import { PlayersService } from './players.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { GlobalConfigPhysicalFitness } from 'src/settings/primary/configs/entities/global-config-physical-fitness.entity';
import { AccountInjuries } from 'src/personals/account-injuries/account-injuries.entity';
import { AccountTechnicalSkills } from 'src/personals/account-technical-skills/account-technical-skills.entity';
import { AccountHonors } from 'src/personals/account-honors/account-honors.entity';
import { AccountPenalties } from 'src/personals/account-penalties/account-penalties.entity';

@Module({
  controllers: [PlayersController],
  providers: [PlayersService],
  imports: [
    PlayerOverviewsModule,
    PlayerDetailModule,
    TypeOrmModule.forFeature([
      AccountSpecifications,
      GlobalConfigPositions,
      GlobalConfigPhysicalFitness,
      AccountInjuries,
      AccountSpecifications,
      AccountInjuries,
      GlobalConfigPositions,
      GlobalConfigPhysicalFitness,
      AccountTechnicalSkills,
      AccountHonors,
      AccountPenalties
    ]),
  ],
})
export class PlayersModule {}
