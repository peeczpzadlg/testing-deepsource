import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { IHonor } from 'src/personals/account-honors/account-honor.interface';
import { AccountHonors } from 'src/personals/account-honors/account-honors.entity';
import { AccountInjuries } from 'src/personals/account-injuries/account-injuries.entity';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';
import { AccountTechnicalSkills } from 'src/personals/account-technical-skills/account-technical-skills.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';
import { SearchPageDto } from 'src/microservices/microservices.dto';
import { AccountPenalties } from 'src/personals/account-penalties/account-penalties.entity';
import { IMicroservicePaginateQuery, IPaginateQuery, PaginateResponse } from 'src/shared/interfaces/api-interfaces';
import { response } from 'express';

@Injectable()
export class PlayersService {
  private microservice: AxiosInstance;
  constructor(
    @InjectRepository(AccountSpecifications)
    private accountSpecificationsRepository: Repository<AccountSpecifications>,

    @InjectRepository(AccountInjuries)
    private accountInjuriesRepository: Repository<AccountInjuries>,

    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,
    @InjectRepository(AccountTechnicalSkills)
    private accountTechnicalSkillsRepository: Repository<
      AccountTechnicalSkills
    >,
    @InjectRepository(AccountHonors)
    private accountHonorsRepository: Repository<AccountHonors>,
    @InjectRepository(AccountPenalties)
    private accountPenaltiesRepository: Repository<AccountPenalties>,
  ) {
    this.microservice = microserviceAxios();
  }

  // utilities
  async getMoreDataFromSpecification(
    account_specification_id,
    account_id,
    keys = [],
  ) {
    let finalData = {};
    // query base data
    const specification_data = await this.accountSpecificationsRepository.findOne(
      account_specification_id,
      {
        relations: ['account_physical_fitness'],
      },
    );

    const {
      global_config_positions: global_config_position_ids,
      account_physical_fitness: global_config_physical_fitness,
    } = specification_data;

    // positions
    if (keys.includes('positions')) {
      let global_config_positions = [];
      if (global_config_position_ids?.length > 0) {
        global_config_positions = await this.globalConfigPositionsRepository
          .createQueryBuilder('globalConfigPosition')
          .where(
            'globalConfigPosition.id IN (:...global_config_position_ids)',
            {
              global_config_position_ids,
            },
          )
          .getMany();
      }
      finalData = {
        ...finalData,
        global_config_positions,
      };
    }

    // let positions = null;
    // if (keys.includes('position_histories')) {
    //   if (global_positions) {
    //     positions = await this.globalConfigPositionsRepository.findOne(
    //       {
    //         where: { id: global_positions },
    //         select: ['global_config_position_name_en', 'global_config_position_name_en']
    //       }
    //     )
    //   }
    // }

   

    // injuries
    if (keys.includes('injuries')) {
      const account_injuries = await this.accountInjuriesRepository.findOne({
        account_id,
      });
      finalData = { ...finalData, account_injuries };
    }

    // technical_skills
    if (keys.includes('technical_skills')) {
      const technical_skills = await this.accountTechnicalSkillsRepository.find(
        {
          where: {
            account_specification_id: account_specification_id,
          },
          relations:['global_technical_skill_id']
        },
      );
      finalData = { ...finalData, technical_skills };
    }

    if (keys.includes('physical_fitness')) {
      finalData = { ...finalData, global_config_physical_fitness:global_config_physical_fitness || 0 };
    }

    // specification_data
    if (keys.includes('specification_data')) {
      finalData = { ...finalData, specification_data };
    }

    return finalData;
  }

  paginateArray(array, page_size, page_number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
  }

  async queryAndSelectHonorsByKey(account_id, key, query: IMicroservicePaginateQuery, accessToken: string) {
    const ResponseMS = await getResponse(this.microservice.get(`account/${account_id}`,
      {
        headers: {
          Authorization: accessToken
        }
      }));
    const { data: { account: { account_specifications } } } = ResponseMS;
    const queryResult = await this.accountSpecificationsRepository.createQueryBuilder('specification').leftJoin('account_honors', 'honors', 'honors.id = specification.account_honors')
      .where('specification.id = :id', { id: account_specifications }).select([`honors.*`])
      .getRawOne();
    if (!queryResult || !queryResult.id) {
      return new PaginateResponse(
        query.page || 1,
        0,
        query.size || 10,
        [],
      );
    }
    let result: IHonor[];
    result = queryResult[key]
    const total = result.length;
    result = result.map((ele, idx) => ({ ...ele, id: idx }))
      .slice(
        (query.size || 10) * ((query.page || 1) - 1),
        (query.size || 10) * (query.page || 1),
      );
    result.sort((first, last) => {
      return last.honor_year - first.honor_year
    })
    return new PaginateResponse(
      query.page || 1,
      total,
      query.size || 10,
      result,
    );

  }

  // account
  async getAccountBanner(account_id: number, accessToken: string) {
    const responseFromMS = await getResponse(
      this.microservice.get(`/player/${account_id}/banner`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );

    const {
      data: { account },
    } = responseFromMS;
    if (!account) return responseFromMS.data || responseFromMS;
    if (!account.account_specifications) return responseFromMS.data.account || responseFromMS
    const { account_specifications: account_specifications_id } = account;
    const moreData = await this.getMoreDataFromSpecification(
      account_specifications_id,
      account_id,
      ['positions', 'injuries', 'technical_skills', 'physical_fitness'],
    );

    return {
      ...account,
      ...moreData,
    };
  }

  async getAccountInfo(account_id: number, accessToken: string) {
    const responseFromMS = await getResponse(
      this.microservice.get(`/player/${account_id}/information`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );

    return responseFromMS;
  }

  // team history
  async getHistoryOfNationTeam(
    account_id: number,
    accessToken: string,
    params: SearchPageDto,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`/player/${account_id}/team_history/nation`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );

    const {
      data: { account },
    } = responseFromMS;
    if (!account) return responseFromMS.data;
    if( account.data.length<0) return responseFromMS.data.account;
    const moreData = await this.genMoredata(
      responseFromMS
    );
    responseFromMS.data = responseFromMS.data.account
    responseFromMS.data.data = moreData;
    return {
      ...responseFromMS.data,
    };
  }

  async getHistoryOfClubTeam(
    account_id: number,
    accessToken: string,
    params: SearchPageDto,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`/player/${account_id}/team_history/club`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
      // console.log(responseFromMS);
    const {
      data: { account },
    } = responseFromMS;
    
    // return responseFromMS?.data;

    if (!account) return null;
    // const { account_specifications: account_specifications_id } = account;
    const moreData = await this.genMoredata(
      responseFromMS
    );
    // console.log(moreData);
    responseFromMS.data = responseFromMS.data.account
    responseFromMS.data.data = moreData;
    return {
      ...responseFromMS.data,
    };
  }

  // honor
  async getHonorsWithKey(
    account_id: number,
    key: string,
    query: IMicroservicePaginateQuery,
    accessToken: string
  ) {
    return this.queryAndSelectHonorsByKey(account_id, key, query, accessToken);
  }

  // injuries
  async getCurrentInjuries(account_id: number, params: SearchPageDto) {

    const result = await paginate<AccountInjuries>(
      this.accountInjuriesRepository,
      { limit: params.size, page: params.page },
      {
        where: {
          account_id,
          account_injury_present: true
        },
        relations: ['global_config_injury_types'],
      },
    );

    const { items, meta } = result;

    return { data: items, ...meta };
  }

  async getInjuries(account_id: number, params: SearchPageDto) {
    const result = await paginate<AccountInjuries>(
      this.accountInjuriesRepository,
      { limit: params.size, page: params.page },
      {
        where: {
          account_id,
          account_injury_present: false
        },
        relations: ['global_config_injury_types'],
      },
    );

    const { items, meta } = result;

    return { data: items, ...meta };
  }

  // penalties
  async getPenalties(
    account_id: number,
    params: SearchPageDto,
    account_penalty_present: boolean,
  ) {
    const result = await paginate<AccountPenalties>(
      this.accountPenaltiesRepository,
      { limit: params.size, page: params.page },
      {
        where: {
          account_id,
          account_penalty_present,
        },
        order: {
          account_penalty_date: 'DESC',
        },
      },
    );

    const { items, meta } = result;
    return { data: items, ...meta };
  }

  // images
  async getImages(
    account_id: number,
    params: SearchPageDto,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`/player/${account_id}/images`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }

  // videos
  async getVideos(
    account_id: number,
    params: SearchPageDto,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`/player/${account_id}/videos`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }
  
  async genMoredata(responseFromMS) {
    const {
      data: {
        account: { data },
      },
    } = responseFromMS;
    // console.log(data);
    if (!Array.isArray(data) || data.length === 0) {
      // responseFromMS.data.data = [];
      return [];
    }
    const newData = await Promise.all(
      data.map(async d => {
        const { account_specifications, account_id, global_config_positions } = d;
        
  
        let position = {};
       
        position = await this.globalConfigPositionsRepository.findOne({
            where:{ id: global_config_positions },
            select:['global_config_position_name_th','global_config_position_name_en']
        })

        return {
          ...d,
          ...position,
        };
      }),
    );
    return newData;
  }
}
