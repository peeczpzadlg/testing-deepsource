import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SearchPageDto } from 'src/microservices/microservices.dto';
import { IPaginateQuery, PaginateQueryDto } from 'src/shared/interfaces/api-interfaces';
import { ResponseFailed, ResponseSuccess } from 'src/shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from 'src/shared/utils/helper.utils';
import { StaffDetailService } from './staff-detail.service';

@ApiTags('User Panel - Staffs Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('staff')
export class StaffDetailController {
  constructor(private staffDetailService: StaffDetailService) {}
  @Get(':account_id/banner')
  async getStaffBanner(
    @Req() req: Request,
    @Param('account_id') account_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    try {
      const data = await this.staffDetailService.getStaffBanner(account_id, token);
      return new ResponseSuccess('Successfully', data);
    }catch (err){
      throw new ResponseFailed(err.message, err.error_code, '');
    }
    
  }

  @Get(':account_id/information')
  async getStaffInformation(
    @Req() req: Request,
    @Param('account_id') account_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await this.staffDetailService.getStaffInformation(account_id, token);
  }

  @Get(':account_id/team_history/nation')
  async getStaffTeamHistoryNation(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await this.staffDetailService.getStaffTeamHistoryNation(
      account_id,
      searchPageDto,
      token,
    );
  }

  @Get(':account_id/team_history/club')
  async getStaffTeamHistoryClub(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await this.staffDetailService.getStaffTeamHistoryClub(
      account_id,
      searchPageDto,
      token,
    );
  }

  @Get(':account_id/honors/nation')
  async getStaffHonorsNation(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: SearchPageDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.staffDetailService.getStaffHonors(
        account_id,
        'account_honor_nations',
        searchPageDto,
        token,
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err){
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/honors/club')
  async getStaffHonorsClub(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: PaginateQueryDto,
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.staffDetailService.getStaffHonors(
        account_id,
        'account_honor_clubs',
        searchPageDto,
        token,
      );
      return new ResponseSuccess('Successfully', data);
    }catch (err){
      throw new ResponseFailed(err.message, err.error_code, '');
    }
    
  }

  @Get(':account_id/honors/personal')
  async getStaffHonorsPersonal(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: PaginateQueryDto,
  ) {

    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.staffDetailService.getStaffHonors(
        account_id,
        'account_honor_personals',
        searchPageDto,
        token,
      );
      return new ResponseSuccess('Successfully', data);
    } catch (err){
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':account_id/penalties/current')
  async getStaffPenaltiesCurrent(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await this.staffDetailService.getStaffPenaltiesCurrent(
      account_id,
      searchPageDto,
      token,
    );
  }

  @Get(':account_id/penalties')
  async getStaffPenalties(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() searchPageDto: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await this.staffDetailService.getStaffPenalties(
      account_id,
      searchPageDto,
      token,
    );
  }

  @Get(':account_id/images')
  async getStaffImages(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() query:SearchPageDto 
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.staffDetailService.getStaffImages(account_id, query, token)
    );
   
  }

  @Get(':account_id/video')
  async getStaffVideo(
    @Req() req: Request,
    @Param('account_id') account_id: number,
    @Query() query:SearchPageDto 
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.staffDetailService.getStaffVideo(account_id, query, token)
    );
  }
}
