import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';
import { AccountHonors } from '../../../../../personals/account-honors/account-honors.entity';
import { AccountPenalties } from '../../../../../personals/account-penalties/account-penalties.entity';
import { AccountSpecifications } from '../../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from '../../../../../settings/primary/configs/entities/global-config-positions.entity';
import { StaffDetailController } from './staff-detail.controller';
import { StaffDetailService } from './staff-detail.service';

@Module({
    imports:[TypeOrmModule.forFeature([AccountSpecifications,AccountHonors,AccountPenalties,GlobalConfigPositions,GlobalConfigDepartments])],
    providers: [StaffDetailService],
    controllers: [StaffDetailController],
})
export class StaffDetailModule {}
