import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { AccountHonors } from '../../../../../personals/account-honors/account-honors.entity';
import { getResponse } from '../../../../../shared/utils/helper.utils';
import { SearchPageDto } from '../../../../../microservices/microservices.dto';
import { microserviceAxios } from '../../../../../shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import { IMicroservicePaginateQuery, IPaginateQuery, IResponseSuccess, PaginateResponse } from '../../../../../shared/interfaces/api-interfaces';
import { AccountPenalties } from '../../../../../personals/account-penalties/account-penalties.entity';
import { AccountSpecifications } from '../../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { response } from 'express';
import { IHonor } from 'src/personals/account-honors/account-honor.interface';
import { ResponseSuccess } from 'src/shared/interfaces/global-config.interface';
import { GlobalConfigDepartments } from 'src/settings/primary/configs/entities/global-config-departments.entity';

@Injectable()
export class StaffDetailService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(StaffDetailService.name);

  constructor(
    @InjectRepository(AccountHonors)
    private readonly accountHonors: Repository<AccountHonors>,

    @InjectRepository(AccountPenalties)
    private readonly accountPenalties: Repository<AccountPenalties>,

    @InjectRepository(AccountSpecifications)
    private readonly accountSpecifications: Repository<AccountSpecifications>,

    @InjectRepository(GlobalConfigPositions)
    private readonly gloabalConfigPositions: Repository<GlobalConfigPositions>,

    @InjectRepository(GlobalConfigDepartments)
    private readonly globalConfigDepartments: Repository<GlobalConfigDepartments>,

  ) {
    this.microservice = microserviceAxios();
  }

  async getStaffBanner(account_id: number, accessToken: string) {
    const responseFromMS = await getResponse(
      this.microservice.get(`staff/${account_id}/banner`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );

    if (responseFromMS.data && responseFromMS.data.account) {
      const newData = await this.genData(responseFromMS);
      return newData;
    }
    return {}


  }

  async getStaffInformation(account_id: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`staff/${account_id}/information`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async getStaffTeamHistoryNation(
    account_id: number,
    searchPageDto: SearchPageDto,
    accessToken,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`staff/${account_id}/team-history/nation`, {
        headers: {
          Authorization: accessToken,
        },
        params: searchPageDto,
      }),
    );
    const { data: { account } } = responseFromMS;
    if (!account) return responseFromMS.data;

    const newResponse = await this.genMoredata(responseFromMS);
    responseFromMS.data = responseFromMS.data.account;
    responseFromMS.data.data = newResponse;
    return {
      ...responseFromMS,
    };
  }

  async getStaffTeamHistoryClub(
    account_id: number,
    searchPageDto: SearchPageDto,
    accessToken,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`staff/${account_id}/team-history/club`, {
        headers: {
          Authorization: accessToken,
        },
        params: searchPageDto,
      }),
    );
    const { data: { account } } = responseFromMS;
    if (!account) return responseFromMS.data;

    const newResponse = await this.genMoredata(responseFromMS);
    responseFromMS.data = responseFromMS.data.account;
    responseFromMS.data.data = newResponse;
    return {
      ...responseFromMS,
    };

  }

  async getStaffHonors(
    account_id: number,
    key: string,
    query: IMicroservicePaginateQuery,
    accessToken,
  ) {

    const ResponseMS = await getResponse(this.microservice.get(`account/${account_id}`,
      {
        headers: {
          Authorization: accessToken
        }
      }));
    const { data: { account: { account_specifications } } } = ResponseMS;
    const _query = await this.accountSpecifications.createQueryBuilder('specification').leftJoin('account_honors', 'honors', 'honors.id = specification.account_honors')
      .where('specification.id = :id', { id: account_specifications }).select([`honors.*`])
      .getRawOne();
    if (!_query || !_query.id) {
      return new PaginateResponse(
        query.page || 1,
        0,
        query.size || 10,
        [],
      );
    }
    let result: IHonor[];
    result = _query[key]
    const total = result.length;
    result = result.map((ele, idx) => ({ ...ele, id: idx }))
      .slice(
        (query.size || 10) * ((query.page || 1) - 1),
        (query.size || 10) * (query.page || 1),
      );
    result.sort((first, last) => {
      return last.honor_year - first.honor_year
    })
    const newReponse = new PaginateResponse(
      query.page || 1,
      total,
      query.size || 10,
      result,
    );
    const newResult:any = {}
    newResult.honor = newReponse;
    return newResult
    // const results: IResponseSuccess = {
    //   success: true,
    //   message: 'get account_penalties successfully.',
    //   data: newResult,
    // };
  }




  async getStaffPenaltiesCurrent(
    account_id: number,
    searchPageDto: SearchPageDto,
    accessToken,
  ) {
    const page = searchPageDto.page ? searchPageDto.page : 1;
    const size = searchPageDto.size ? searchPageDto.size : 10;
    let skip = page - 1;
    const limit = size;
    skip = skip * limit;
    const _query = await this.accountPenalties
      .createQueryBuilder('account_penalties')
      .where('account_penalties.deleted = :delete', { delete: false })
      .andWhere(
        'account_penalties.account_penalty_present = :account_penalties',
        { account_penalties: true },
      )
      .andWhere('account_penalties.account_id = :account_id', {
        account_id: account_id,
      });
    _query.orderBy('account_penalties.account_penalty_date', 'DESC');

    const count = await _query.getCount();
    const account_penalties: any = await _query
      .take(limit)
      .skip(skip)
      .getMany();

    const result: any = {};
    result.currentPage = page;
    result.total = count;
    result.perPage = size;
    result.lastPage = Math.ceil(count / size);
    result.data = account_penalties;
    const newResult:any = {}
    newResult.penalty = result;
    const results: IResponseSuccess = {
      success: true,
      message: 'get account_penalties successfully.',
      data: newResult,
    };
    return results;
  }

  async getStaffPenalties(
    account_id: number,
    searchPageDto: SearchPageDto,
    accessToken,
  ) {
    const page = searchPageDto.page ? searchPageDto.page : 1;
    const size = searchPageDto.size ? searchPageDto.size : 10;
    let skip = page - 1;
    const limit = size;
    skip = skip * limit;
    const _query = await this.accountPenalties
      .createQueryBuilder('account_penalties')
      .where('account_penalties.deleted = :delete', { delete: false })
      .andWhere(
        'account_penalties.account_penalty_present = :account_penalties',
        { account_penalties: false },
      )
      .andWhere('account_penalties.account_id = :account_id', {
        account_id: account_id,
      });
    _query.orderBy('account_penalties.account_penalty_date', 'DESC');

    const count = await _query.getCount();
    const account_penalties: any = await _query
      .take(limit)
      .skip(skip)
      .getMany();

    const result: any = {};
    result.currentPage = page;
    result.total = count;
    result.perPage = size;
    result.lastPage = Math.ceil(count / size);
    result.data = account_penalties;

    const newResult:any = {}
    newResult.penalty = result;
    const results: IResponseSuccess = {
      success: true,
      message: 'get account_penalties successfully.',
      data: newResult,
    };
    return results;
  }

  async getStaffImages(account_id: number, query:SearchPageDto, accessToken: string) {
    return await getResponse(
      this.microservice.get(`staff/${account_id}/images`, {
        headers: {
          Authorization: accessToken,
        },
        params:query
      }),
    );
  }

  async getStaffVideo(account_id: number, query:SearchPageDto, accessToken: string) {
    return await getResponse(
      this.microservice.get(`staff/${account_id}/videos`, {
        headers: {
          Authorization: accessToken,
        },
        params:query
      }),
    );
  }

  async genData(responseFromMS) {
    // console.log(responseFromMS);
    if (Object.keys(responseFromMS.data).length === 0) {
      return responseFromMS;
    }
    const {
      data: {
        account: { account_specifications, account_id },
      },
    } = responseFromMS;
    if (!account_specifications) {
      return responseFromMS;
    }
    const accountResponse = await this.accountSpecifications.findOne({
      where: {
        id: account_specifications,
      },
    });
    // console.log(accountResponse);
    let global_config_position_ids =
      accountResponse.global_config_positions;
    if (global_config_position_ids.length > 0) {
      const global_config_positions = await this.gloabalConfigPositions
        .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
          select *
          from unnest(array[${global_config_position_ids}]) with ordinality
      ) as x (id, ordering) on a.id = x.id where a.id in (${global_config_position_ids}) order by x.ordering;`);
      responseFromMS.data.account.global_config_positions = global_config_positions
    } else {
      responseFromMS.data.account.global_config_positions = [];
    }

    return responseFromMS.data;
  }

  async genMoredata(responseFromMS) {
    const { data: { account: { data } } } = responseFromMS;
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS.data.account.data };
    }
    // console.log('aafdf',data);
    const newData = await Promise.all(
      data.map(async d => {
        const {  global_config_positions, global_config_department } = d;

        const position = await this.gloabalConfigPositions.findOne({
          where: { id: global_config_positions },
          select: ['global_config_position_name_th', 'global_config_position_name_en']
        })
        const department = await this.globalConfigDepartments.findOne({
          where: { id: global_config_department },
          select: ['global_config_department_name_th', 'global_config_department_name_en']
        })
        return {
          ...d,
          ...position,
          ...department
        };
      }),
    );
    return newData;
  }

  async genMoredataTimeline(responseFromMS) {
    const { data: { account: { data } } } = responseFromMS;
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS.data.account.data };
    }
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS.data.account.data };
    }
    // console.log('aafdf',data);
    const newData = await Promise.all(
      data.map(async d => {
        const {  timeline } = d;
        // console.log(timeline);
        if(!timeline || !timeline.team) return {...d};
        const position = await this.gloabalConfigPositions.findOne({
          where: { id: timeline.team.global_config_positions },
          select: ['global_config_position_name_th', 'global_config_position_name_en']
        })
        const department = await this.globalConfigDepartments.findOne({
          where: { id: timeline.team.global_config_department },
          select: ['global_config_department_name_th', 'global_config_department_name_en']
        })
        timeline.team = {
          ...timeline.team,
          ...position,
          ...department
        }
        return {
          ...d,
        };
      }),
    );
    return newData;
  }


  

}
