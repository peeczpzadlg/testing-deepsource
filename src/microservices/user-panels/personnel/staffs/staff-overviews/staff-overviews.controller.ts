import {
  Controller,
  Get,
  HttpStatus,
  Param,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
} from 'src/shared/utils/helper.utils';
import {
  GetStaffOverviewAllQueryDto,
  StaffOverviewTeamQueryDto,
} from './staff-overviews.dto';
import { StaffOverviewsService } from './staff-overviews.service';

@ApiTags('User Panel - Staffs Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('staff/overview')
export class StaffOverviewsController {
  constructor(private staffOverviewsService: StaffOverviewsService) {}

  @Get('all')
  async getStaffOverview(
    @Req() req: Request,
    @Query() query: GetStaffOverviewAllQueryDto,
  ) {
    try {
      // await checkAuthMicroservice(req.headers.authorization);
      const token = await getHeaderAccessToken(req.headers);

      const data = await this.staffOverviewsService.getStaffOverview(query, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('/team/:team_id')
  async getStaffoverviewTeamById(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() staffOverviewTeamQueryDto: StaffOverviewTeamQueryDto,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const data = await this.staffOverviewsService.getStaffOverviewTeamById(
        team_id,
        staffOverviewTeamQueryDto,
        token,
      );
      if (data) {
        return new ResponseSuccess('Successfully', data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }
}
