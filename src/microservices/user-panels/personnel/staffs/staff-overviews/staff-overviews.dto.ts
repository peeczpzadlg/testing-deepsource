import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from '../../../../../microservices/microservices.dto';
import { EGender } from '../../../../../shared/enum/gender.enum';

export class GetStaffOverviewAllQueryDto extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  minAge?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  maxAge?: number;

  @ApiPropertyOptional({
    enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER],
  })
  @IsOptional()
  @IsEnum(EGender)
  gender?: EGender;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  nationality?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_positions?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_departments?: number;
}

export class StaffOverviewTeamQueryDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({required:false})
  name?: string;

  @IsNumber()
  @ApiPropertyOptional()
  @IsOptional()
  minAge?: number;

  @IsNumber()
  @ApiPropertyOptional()
  @IsOptional()
  maxAge?: number;

  @ApiPropertyOptional({
    enum: [EGender.MALE, EGender.FEMALE, EGender.TRANSGENDER],
  })
  @IsOptional()
  @IsEnum(EGender)
  gender: EGender;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  nationality?: string;
}
