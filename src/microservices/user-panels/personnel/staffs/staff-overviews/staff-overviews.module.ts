import { Module } from '@nestjs/common';
import { StaffOverviewsController } from './staff-overviews.controller';
import { StaffOverviewsService } from './staff-overviews.service';

import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from '../../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigPositions } from '../../../../../settings/primary/configs/entities/global-config-positions.entity';
import { GlobalConfigDepartments } from '../../../../../settings/primary/configs/entities/global-config-departments.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AccountSpecifications,
      GlobalConfigPositions,
      GlobalConfigDepartments,
    ]),
  ],
  providers: [StaffOverviewsService],
  controllers: [StaffOverviewsController],
})
export class StaffOverviewsModule {}
