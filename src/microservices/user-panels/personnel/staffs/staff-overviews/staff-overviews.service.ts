import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { microserviceAxios } from '../../../../..//shared/utils/microservice-axios.utils';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountSpecifications } from '../../../../../personals/account-specifications/account-speciafications.entity';
import { GlobalConfigDepartments } from '../../../../../settings/primary/configs/entities/global-config-departments.entity';
import { GlobalConfigPositions } from '../../../../../settings/primary/configs/entities/global-config-positions.entity';
import { Any, In, Repository } from 'typeorm';
import { convertAge, getResponse } from '../../../../../shared/utils/helper.utils';
import { GetStaffOverviewAllQueryDto, StaffOverviewTeamQueryDto } from './staff-overviews.dto';
import { SearchPageDto } from '../../../../../microservices/microservices.dto';
import { iif } from 'rxjs';
import { query, response } from 'express';
import { filter } from 'rxjs/operators';

@Injectable()
export class StaffOverviewsService {
  private microservice: AxiosInstance;
  constructor(
    @InjectRepository(AccountSpecifications)
    private accountSpecificationsRepository: Repository<AccountSpecifications>,

    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositionsRepository: Repository<GlobalConfigPositions>,

    @InjectRepository(GlobalConfigDepartments)
    private globalConfigDepartmentsRepository: Repository<
      GlobalConfigDepartments
    >,
    
  ) {
    this.microservice = microserviceAxios();
  }

  async getStaffOverviewTeamById(
    team_id: number,
    staffOverviewTeamQueryDto: StaffOverviewTeamQueryDto,
    accessToken: string,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`staff/overview/team/${team_id}`, {
        headers: {
          Authorization: accessToken,
        },
        params: staffOverviewTeamQueryDto,
      }),
    );
    const {
      data: {
        account: { data },
      },
    } = responseFromMS;
    if(data.length==0){
        return responseFromMS;
    }
    const newData = await Promise.all(
      data.map(async d => {
        const { account_id,account_specifications } = d;
        let global_config_positions = []
        let global_config_departments = null;
        if(account_specifications){
          const getSpecification = await this.accountSpecificationsRepository.findOne(
            { id: account_specifications },
          );
          if(getSpecification &&getSpecification.global_config_positions && getSpecification.global_config_positions.length>0){
            global_config_positions = await this
            .globalConfigPositionsRepository
            .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
                  select *
                  from unnest(array[${getSpecification.global_config_positions}]) with ordinality
              ) as x (id, ordering) on a.id = x.id where a.id in (${getSpecification.global_config_positions}) order by x.ordering;`);
          }
        
          if(getSpecification.global_config_departments){
            global_config_departments = await this.globalConfigDepartmentsRepository.findOne(
              { id: getSpecification.global_config_departments },
            );
          }
        }

        return {
          ...d,
          global_config_positions,
          global_config_departments,
        };
      }),
    );
    return newData;
  }

  async getStaffOverview(params: GetStaffOverviewAllQueryDto, accessToken: string) {

    const getSpecification = this.accountSpecificationsRepository.createQueryBuilder('account_specification').relation('global_config_departments')
    .select(['account_specification.id']).where({active:true,deleted:false})

    if(params.global_config_positions){
      getSpecification.andWhere(`account_specification.global_config_positions::jsonb ? '${params.global_config_positions}'`);
    }
    if(params.global_config_departments){
      getSpecification.andWhere(`global_config_departments = ${params.global_config_departments}`);
    }
    
    const account_specification = await getSpecification.getMany();
    console.log(account_specification);
    // return false
    const specification_ids = account_specification.map(a => a.id * 1);

    const body = {
      "specifications_ids":specification_ids
    }
    const responseFromMS = await getResponse(this.microservice.post('/staff/overview/all', body, {
      headers: {
        Authorization: accessToken,
      },
      params
    }));
    const { data: { account: {data} } } = responseFromMS;
    if (!data || data.length < 1) {
        return responseFromMS.data
    }
    let new_data = [];
    await Promise.all(data.map(async d => {
      const { account_id, account_specifications } = d;
      let global_config_positions = [];
      let global_config_departments = null;
      if (account_specifications) {
        const Specification = await this.accountSpecificationsRepository.findOne({
          where: {id:account_specifications},
          relations:['global_config_departments']
        });
        console.log(Specification);

        if(Specification){
          if(Specification.global_config_positions && Specification.global_config_positions.length>0){
            global_config_positions = await this.globalConfigPositionsRepository
            .query(`select a.id,a.global_config_position_name_th,a.global_config_position_name_en,a.global_config_position_abbreviation,a.global_config_position_main_position from global_config_positions a join (
              select *
              from unnest(array[${Specification.global_config_positions}]) with ordinality
            ) as x (id, ordering) on a.id = x.id where a.id in (${Specification.global_config_positions}) order by x.ordering;`);
          }
          global_config_departments = Specification.global_config_departments
        }
      }

      new_data.push({
        ...d,
        global_config_departments,
        global_config_positions: global_config_positions || [],
      });
    }));
    
    const newResponse = responseFromMS;
    newResponse.data.account.data = new_data;
    return newResponse.data;

    
  }
}
