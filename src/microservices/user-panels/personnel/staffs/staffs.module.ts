import { Module } from '@nestjs/common';
import { StaffOverviewsModule } from './staff-overviews/staff-overviews.module';
import { StaffDetailModule } from './staff-detail/staff-detail.module';

@Module({
  imports: [StaffOverviewsModule, StaffDetailModule],
  controllers: [],
  providers: [],
})
export class StaffsModule {}
