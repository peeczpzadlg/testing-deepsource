import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SearchPageDto } from 'src/microservices/microservices.dto';
import { PaginationQuery } from 'src/shared/decorators/search-page-query.decorator';
import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from 'src/shared/utils/helper.utils';
import { GetMatchHistoriesQueryDto, GetStaffQueryDto } from './teams.dto';
import { TeamsService } from './teams.service';

@ApiTags('User Panel - Teams Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('teams')
export class TeamsController {
  constructor(private teamsService: TeamsService) {}

  // staff
  @Get(':team_id/staff/current')
  async getAccountBanner(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @Query() query: GetStaffQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamsService.getCurrentStaffs(team_id, query, token)
    );
  }

  @Get(':team_id/staff/history')
  async getStaffHistories(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @Query() query: GetStaffQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamsService.getStaffHistories(team_id, query, token)
    );
  }

  // match
  @Get(':team_id/match/history/important')
  async getMatchImportantHistories(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @Query() query: GetMatchHistoriesQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      await this.teamsService.getMatchImportantHistories(
        team_id,
        query,
        token,
      )
    );
    
  }

  @Get(':team_id/match/history')
  async getMatchHistories(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @Query() query: GetMatchHistoriesQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamsService.getMatchHistories(team_id, query, token)
    );

  }

  // media
  @Get(':team_id/media/images')
  async getTeamImages(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @PaginationQuery() query: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamsService.getTeamImages(team_id, query, token)
    );
    
  }

  @Get(':team_id/media/videos')
  async getTeamVideos(
    @Param('team_id') team_id: number,
    @Req() req: Request,
    @PaginationQuery() query: SearchPageDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamsService.getTeamVideos(team_id, query, token)
    );
  }
}
