import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, isString } from 'class-validator';

export class GetStaffQueryDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  minAge: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  maxAge: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  nationality?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_positions?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_department?: number;

  constructor() {
    this.page = this.page ?? 1;
    this.size = this.size ?? 8;
  }
}

export class GetMatchHistoriesQueryDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_competitions?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadiums?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_sub_competition_year?: number;

  constructor() {
    this.page = this.page ?? 1;
    this.size = this.size ?? 20;
  }
}
