import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { SearchPageDto } from '../../../../microservices/microservices.dto';
import { GlobalConfigDepartments } from '../../../../settings/primary/configs/entities/global-config-departments.entity';
import { GlobalConfigPositions } from '../../../../settings/primary/configs/entities/global-config-positions.entity';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import { GetMatchHistoriesQueryDto, GetStaffQueryDto } from './teams.dto';

@Injectable()
export class TeamsService {
  private microservice: AxiosInstance;
  @InjectRepository(GlobalConfigPositions)
  private readonly gloabalConfigPositions: Repository<GlobalConfigPositions>

  @InjectRepository(GlobalConfigDepartments)
  private readonly globalConfigDepartments: Repository<GlobalConfigDepartments>

  constructor() {
    this.microservice = microserviceAxios();
  }

  // staff

  async getCurrentStaffs(
    team_id: number,
    params: GetStaffQueryDto,
    accessToken,
  ) {
    const responseFromMS =  await getResponse(
      this.microservice.get(`/teams/${team_id}/staff/current`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
    const newResponse = await this.genMoredata(responseFromMS);
    return newResponse;
  }

  async getStaffHistories(
    team_id: number,
    params: GetStaffQueryDto,
    accessToken,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`/teams/${team_id}/staff/history`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
    const newResponse = await this.genMoredata(responseFromMS);
    return newResponse;
    
  }

  // match

  sortHistories(response) {
    const {
      data: { versus_match },
    } = response;
    const { data: items } = versus_match;

    return items.sort((a, b) => {
      // a
      const {
        versus_match_team_detail_one_id: a_one,
        versus_match_team_detail_two_id: a_two,
      } = a;
      const important_a =
        a_one?.versus_match_team_detail_important ||
        a_two?.versus_match_team_detail_important;
      // b;
      const {
        versus_match_team_detail_one_id: b_one,
        versus_match_team_detail_two_id: b_two,
      } = b;
      const important_b =
        b_one?.versus_match_team_detail_important ||
        b_two?.versus_match_team_detail_important;

      // compare
      return Number(important_b) - Number(important_a);
    });
  }

  async getMatchImportantHistories(
    team_id: number,
    params: GetMatchHistoriesQueryDto,
    accessToken,
  ) {
    return await getResponse(
      this.microservice.get(`/teams/${team_id}/match/history/important`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }

  async getMatchHistories(
    team_id: number,
    params: GetMatchHistoriesQueryDto,
    accessToken,
  ) {
    return await getResponse(
      this.microservice.get(`/teams/${team_id}/match/history`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }

  // media
  async getTeamImages(team_id: number, params: SearchPageDto, accessToken) {
    return await getResponse(
      this.microservice.get(`/teams/${team_id}/media/images`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }

  async getTeamVideos(team_id: number, params: SearchPageDto, accessToken) {
    return await getResponse(
      this.microservice.get(`/teams/${team_id}/media/videos`, {
        headers: {
          Authorization: accessToken,
        },
        params,
      }),
    );
  }

  async genMoredata(responseFromMS) {

    const { data: { team_staff_history: { data } } } = responseFromMS;
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS };
    }
    // console.log('aafdf',data);
    const newData = await Promise.all(
      data.map(async d => {
        const {  global_config_positions, global_config_department } = d;

        const position = await this.gloabalConfigPositions.findOne({
          where: { id: global_config_positions },
          select: ['global_config_position_name_th', 'global_config_position_name_en']
        })
        const department = await this.globalConfigDepartments.findOne({
          where: { id: global_config_department },
          select: ['global_config_department_name_th', 'global_config_department_name_en']
        })
        return {
          ...d,
          ...position,
          ...department
        };
      }),
    );
    responseFromMS.data.team_staff_history.data = newData;
    return responseFromMS;
  }
}
