import { Controller, Get, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Request } from 'express';

import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
  getMicroserviceResponse,
} from 'src/shared/utils/helper.utils';
import { StadiumOverviewsService } from './stadium-overviews.service';
import { GetStadiumOverviewQueryDto } from './stadium-overviews.dto';

@ApiTags('User Panel - Stadiums Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('stadiums/overview')
export class StadiumOverviewsController {
  constructor(private stadiumOverviewsService: StadiumOverviewsService) {}

  @Get()
  async getThaiTeamOverview(@Req() req: Request, @Query() query: GetStadiumOverviewQueryDto) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.stadiumOverviewsService.getStadiumOverview(query, token)
    );
  }
}
