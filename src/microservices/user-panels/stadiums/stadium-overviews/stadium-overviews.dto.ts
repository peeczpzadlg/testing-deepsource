import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { SearchPageDto } from '../../../../microservices/microservices.dto';

export class GetStadiumOverviewQueryDto extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  countries?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  provinces?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_capacity_min?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  setting_stadium_capacity_max?: number;

}
