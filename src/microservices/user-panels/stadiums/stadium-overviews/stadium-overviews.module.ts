import { Module } from '@nestjs/common';
import { StadiumOverviewsController } from './stadium-overviews.controller';
import { StadiumOverviewsService } from './stadium-overviews.service';

@Module({
  controllers: [StadiumOverviewsController],
  providers: [StadiumOverviewsService]
})
export class StadiumOverviewsModule {}
