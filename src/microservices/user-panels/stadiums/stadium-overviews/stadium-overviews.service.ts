import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { GetStadiumOverviewQueryDto } from './stadium-overviews.dto';

@Injectable()
export class StadiumOverviewsService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStadiumOverview(query: GetStadiumOverviewQueryDto, accessToken: string) {
    return await getResponse( this.microservice.get('/stadiums/overview', {
      headers: {
        Authorization: accessToken,
      },
      params: query
    }));
  }
}
