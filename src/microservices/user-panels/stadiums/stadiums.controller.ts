import { Controller, Get, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import {
  checkAuthMicroservice,
  getHeaderAccessToken,
} from 'src/shared/utils/helper.utils';
import { StadiumsService } from './stadiums.service';
import { Request } from 'express';
import {
  ResponseFailed,
  ResponseSuccess,
} from 'src/shared/interfaces/global-config.interface';

@ApiTags('User Panel - Stadiums Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('stadiums')
export class StadiumsController {
  constructor(private stadiumsService: StadiumsService) {}

  @Get(':stadium_id/information')
  async getStadiumInformation(
    @Param('stadium_id') stadium_id: number,
    @Req() req: Request,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.stadiumsService.getStadiumInformation(stadium_id, token);
      return new ResponseSuccess('Successfully', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }
}
