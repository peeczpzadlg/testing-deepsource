import { Module } from '@nestjs/common';
import { StadiumOverviewsModule } from './stadium-overviews/stadium-overviews.module';
import { StadiumDetailModule } from './stadium-detail/stadium-detail.module';
import { StadiumsService } from './stadiums.service';
import { StadiumsController } from './stadiums.controller';

@Module({
  imports: [StadiumOverviewsModule, StadiumDetailModule],
  providers: [StadiumsService],
  controllers: [StadiumsController]
})
export class StadiumsModule {}
