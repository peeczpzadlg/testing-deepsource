import { Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';

@Injectable()
export class StadiumsService {
  private microservice: AxiosInstance;

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getStadiumInformation(stadium_id: number, accessToken: string) {
    const res = await this.microservice.get(
      `/stadiums/${stadium_id}/information`,
      {
        headers: {
          Authorization: accessToken,
        },
      },
    );

    return res?.data?.data;
  }
}
