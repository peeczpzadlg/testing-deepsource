import { Controller, Get, HttpStatus, Param, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ExpectError, ResponseFailed, ResponseSuccess } from 'src/shared/interfaces/global-config.interface';
import {  getHeaderAccessToken, getMicroserviceResponse } from 'src/shared/utils/helper.utils';
import { TeamDetailPlayerQueryDto } from './team-detail.dto';
import { TeamDetailService } from './team-detail.service';

@ApiTags('User Panel - Team Detail')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('team')
export class TeamDetailController {

  constructor(private teamDetailService: TeamDetailService) { }

  @Get('/:team_id/banner')
  async getTeamBanner(@Req() req: Request, @Param('team_id') team_id: number) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamBanner(team_id, token)
    );
  }

  @Get('/:team_id/information')
  async getTeamInformation(
    @Req() req: Request,
    @Param('team_id') team_id: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamInformation(team_id, token)
    );
  }

  @Get(':team_id/uniform')
  async getTeamUniform(
    @Req() req: Request,
    @Param('team_id') team_id: number,

  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamUniform(team_id, token),
    );
  }

  @Get(':team_id/uniform/year/:year')
  async getTeamUniformByYear(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Param('year') year: number,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamUniformYear(team_id, year, token),
    );
  }

  @Get(':team_id/player/current')
  async getTeamPlayerCurrent(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamPlayerCurrent(team_id, teamDetailPlayerQueryDto, token)
    );
  }

  @Get(':team_id/player/current/goalkeeper')
  async get(@Req() req: Request, @Param('team_id') team_id: number, @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto) {

    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamPlayerCurrentGoalkeeper(team_id, teamDetailPlayerQueryDto, token)
    );
  }

  @Get(':team_id/player/current/defender')
  async getTeamPlayerCurrentDefender(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamPlayerCurrentDefender(team_id, teamDetailPlayerQueryDto, token)
    );
  }

  @Get(':team_id/player/current/midfielder')
  async getTeamPlayerCurrentMidfielder(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamPlayerCurrentMidfielder(team_id, teamDetailPlayerQueryDto, token)
    );
  }

  @Get(':team_id/player/current/forward')
  async getTeamPlayerCurrentForward(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamDetailService.getTeamPlayerCurrentForward(team_id, teamDetailPlayerQueryDto, token)
    );
  }

  @Get(':team_id/player/history')
  async getTeamPlayerHistory(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.teamDetailService.getTeamPlayerHistory(team_id, teamDetailPlayerQueryDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':team_id/player/history/goalkeeper')
  async getTeamPlayerHistoryGoalkeeper(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.teamDetailService.getTeamPlayerHistoryGoalkeeper(team_id, teamDetailPlayerQueryDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':team_id/player/history/defender')
  async getTeamPlayerHistoryDefender(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.teamDetailService.getTeamPlayerHistoryDefender(team_id, teamDetailPlayerQueryDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':team_id/player/history/midfielder')
  async getTeamPlayerHistoryMidfielder(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.teamDetailService.getTeamPlayerHistoryMidfielder(team_id, teamDetailPlayerQueryDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':team_id/player/history/forward')
  async getTeamPlayerHistoryForaward(
    @Req() req: Request,
    @Param('team_id') team_id: number,
    @Query() teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.teamDetailService.getTeamPlayerHistoryForward(team_id, teamDetailPlayerQueryDto, token);
      if (data && data.data) {
        return new ResponseSuccess('Successfully', data.data);
      }
      throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
    } catch (err) {
      console.log(err);
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }


}
