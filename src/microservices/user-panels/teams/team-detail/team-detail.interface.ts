export interface ITeamDetailPlayer {
  page?: number;
  size?: number;
  minAge?: number;
  maxAge?: number;
  nationality?: string;
  name?:string;
  team_player_history_loan?: string;
}
