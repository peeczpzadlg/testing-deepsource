import { Module } from '@nestjs/common';
import { TeamDetailService } from './team-detail.service';
import { TeamDetailController } from './team-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';

@Module({
  imports: [TypeOrmModule.forFeature([GlobalConfigPositions])],
  providers: [TeamDetailService],
  controllers: [TeamDetailController],
})
export class TeamDetailModule {}
