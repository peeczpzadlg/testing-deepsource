import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';
import { ExpectError } from 'src/shared/interfaces/global-config.interface';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import { TeamDetailPlayerQueryDto } from './team-detail.dto';

@Injectable()
export class TeamDetailService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(TeamDetailService.name);

  constructor(
    @InjectRepository(GlobalConfigPositions)
    private globalConfigPositions: Repository<GlobalConfigPositions>,
  ) {
    this.microservice = microserviceAxios();
  }

  async getTeamBanner(team_id: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`teams/${team_id}/banner`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async getTeamInformation(team_id: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`teams/${team_id}/information`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async getTeamUniform(team_id: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`teams/${team_id}/uniform`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async getTeamUniformYear(team_id: number, year: number, accessToken: string) {
    return await getResponse(
      this.microservice.get(`teams/${team_id}/uniform/year/${year}`, {
        headers: {
          Authorization: accessToken,
        },
      }),
    );
  }

  async getTeamPlayerCurrent(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const responseFromMS = await getResponse(
      this.microservice.get(`teams/${team_id}/player/current`, {
        headers: {
          Authorization: accessToken,
        },
        params: teamDetailPlayerQueryDto,
      }),
    );
    const newResponse = await this.genMoredata(responseFromMS);
    return newResponse;
  }

  async getTeamPlayerCurrentGoalkeeper(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'GOALKEEPER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      }
      // console.log(body);
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/current/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      console.log(teamDetailPlayerQueryDto);
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }

  async getTeamPlayerCurrentDefender(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'DEFENDER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/current/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }
  async getTeamPlayerCurrentMidfielder(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'MIDFIELDER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      // console.log(body);
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/current/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );

      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }


    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }

  async getTeamPlayerCurrentForward(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'FORWARD',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/current/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }

  async getTeamPlayerHistory(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`teams/${team_id}/player/history`, {
        headers: {
          Authorization: accessToken,
        },
        params: teamDetailPlayerQueryDto,
      }),
    );
  }
  async getTeamPlayerHistoryGoalkeeper(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'GOALKEEPER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };

      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/history/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }
  async getTeamPlayerHistoryDefender(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'DEFENDER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/history/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }
  async getTeamPlayerHistoryForward(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'FORWARD',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/history/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }

  async getTeamPlayerHistoryMidfielder(
    team_id: number,
    teamDetailPlayerQueryDto: TeamDetailPlayerQueryDto,
    accessToken: string,
  ) {
    const global_config_position = await this.globalConfigPositions.find({
      where: {
        global_config_position_main_position: 'MIDFIELDER',
        deleted: false,
      },
      select: ['id'],
    });
    if(global_config_position.length>0){
      const ids = [];
      global_config_position.map(x => {
        ids.push(x.id);
      });
      const body = {
        global_config_positions: ids,
      };
      const responseFromMS = await getResponse(
        this.microservice.post(`teams/${team_id}/player/history/position`,body, {
          headers: {
            Authorization: accessToken,
          },
          params: teamDetailPlayerQueryDto,
        }),
      );
      const newResponse = await this.genMoredata(responseFromMS);
      return newResponse;
    }
    throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  }


  async genMoredata(responseFromMS) {
    // console.log(responseFromMS);
    const { data: { team_player_history: { data } } } = responseFromMS;
    // console.log(data);
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS };
    }
    if (!Array.isArray(data) || data.length === 0) {
      return { ...responseFromMS};
    }
    // console.log('aafdf',data);
    const newData = await Promise.all(
      data.map(async d => {
        const {  global_config_positions } = d;
        // console.log(timeline);
        if(!global_config_positions) return {...d};
        const position = await this.globalConfigPositions.findOne({
          where: { id: global_config_positions },
          select: ['global_config_position_name_th', 'global_config_position_name_en']
        })
        return {
          ...d,
          ...position
        };
      }),
    );
    responseFromMS.data.team_player_history.data = newData;
    return {
        ...responseFromMS
    }
  }

  
}
