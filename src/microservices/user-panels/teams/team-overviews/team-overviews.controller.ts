import {
  Controller,
  Get,
  HttpStatus,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../../../auth/jwt-auth.guard';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../../../../shared/interfaces/global-config.interface';
import { checkAuthMicroservice, getHeaderAccessToken, getMicroserviceResponse } from '../../../../shared/utils/helper.utils';
import {
  GetNationalOverviewQueryDto,
  GetThaiTeamOverviewQueryDto,
  TeamOverviewsQueryDto,
  TeamOverviewsThaiClubQueryDto,
} from './team-overviews.dto';
import { TeamOverviewsService } from './team-overviews.service';

@ApiTags('User Panel - Teams Menu')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('teams')
export class TeamOverviewsController {
  constructor(private teamOverviewsService: TeamOverviewsService) {}

  @Get('/overview')
  async getDefender(
    @Req() req: Request,
    @Query() teamOverviewsQueryDto: TeamOverviewsQueryDto,
  ) {
    const token = await getHeaderAccessToken(req.headers);
    return await getMicroserviceResponse(
      this.teamOverviewsService.getTeamOverviews(
        teamOverviewsQueryDto,
        token,
      )
    );
  }

  // @Get('/thai-club')
  // async getTeamThaiClub(
  //   @Req() req: Request,
  //   @Query() teamOverviewsThaiClubQueryDto: TeamOverviewsThaiClubQueryDto,
  // ) {
  //   try {
  //     const token = req.headers.authorization;
  //     await checkAuthMicroservice(token);
  //     const data = await this.teamOverviewsService.getTeamOverviewThaiClub(
  //       teamOverviewsThaiClubQueryDto,
  //       token,
  //     );
  //     if (data && data.data) {
  //       return new ResponseSuccess('Successfully', data.data);
  //     }
  //     throw new ExpectError(HttpStatus.NOT_FOUND, 'Not found', {});
  //   } catch (err) {
  //     console.log(err);
  //     throw new ResponseFailed(err.message, err.error_code, '');
  //   }
  // }

  // @Get('thai-team')
  // async getThaiTeamOverview(
  //   @Req() req: Request,
  //   @Query() query: GetThaiTeamOverviewQueryDto,
  // ) {
  //   try {
  //     await checkAuthMicroservice(req.headers.authorization);
  //     const token = await getHeaderAccessToken(req.headers);
  //     const data = await this.teamOverviewsService.getThaiTeamOverview(query, token);
  //     return new ResponseSuccess('Successfully', data);
  //   } catch (err) {
  //     throw new ResponseFailed(err.message, err.error_code, '');
  //   }
  // }

  // @Get('national-team')
  // async getNationalTeamOverview(
  //   @Req() request: Request,
  //   @Query() query: GetNationalOverviewQueryDto,
  // ) {
  //   const token = await getHeaderAccessToken(request.headers);
  //   return await getMicroserviceResponse(
  //     this.teamOverviewsService.getNationalTeamOverview(query, token),
  //   );
  // }

  // @Get('national-club')
  // async getNationalClubOverview(
  //   @Req() request: Request,
  //   @Query() query: GetNationalOverviewQueryDto,
  // ) {
  //   const token = await getHeaderAccessToken(request.headers);
  //   return await getMicroserviceResponse(
  //     this.teamOverviewsService.getNationalClubOverview(query, token),
  //   );
  // }
}
