import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import {
  SearchPageDto,
  SearchPaginateDto,
} from '../../../../microservices/microservices.dto';
import { IMicroservicePaginateQuery } from '../../../../shared/interfaces/api-interfaces';
import { ITeamOverview } from '../../home/teams/teams.interface';

export class TeamOverviewsQueryDto extends SearchPaginateDto
  implements IMicroservicePaginateQuery {
  @IsNumber()
  @ApiPropertyOptional()
  @IsOptional()
  global_config_local_types: number;
}

export class TeamOverviewsThaiClubQueryDto extends SearchPageDto
  implements IMicroservicePaginateQuery {
  @IsNumber()
  @ApiPropertyOptional()
  @IsOptional()
  global_config_local_types: number;
}

export class GetThaiTeamOverviewQueryDto extends SearchPageDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_local_types?: number;
}

export class GetNationalOverviewQueryDto implements ITeamOverview {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_local_types?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  size?: number;
}
