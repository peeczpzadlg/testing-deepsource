import { Module } from '@nestjs/common';
import { TeamOverviewsController } from './team-overviews.controller';
import { TeamOverviewsService } from './team-overviews.service';

@Module({
  controllers: [TeamOverviewsController],
  providers: [TeamOverviewsService],
})
export class TeamOverviewsModule {}
