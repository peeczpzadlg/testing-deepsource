import { Injectable, Logger } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { getResponse } from '../../../../shared/utils/helper.utils';
import { microserviceAxios } from '../../../../shared/utils/microservice-axios.utils';
import { TeamOverviewsQueryDto } from './team-overviews.dto';

import { ISearchPaginate } from 'src/microservices/microservices.interface';
import { GetThaiTeamOverviewQueryDto } from './team-overviews.dto';

@Injectable()
export class TeamOverviewsService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(TeamOverviewsService.name);

  constructor() {
    this.microservice = microserviceAxios();
  }

  async getThaiTeamOverview(
    query: GetThaiTeamOverviewQueryDto,
    accessToken: string,
  ) {
    const res = await this.microservice.get('/teams/overview/thai-team', {
      headers: {
        Authorization: accessToken,
      },
      params: query,
    });

    return res.data;
  }

  async getNationalTeamOverview(params: ISearchPaginate, accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get('teams/overview/national-team', {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getNationalClubOverview(params: ISearchPaginate, accessToken: string) {
    try {
      return await getResponse(
        this.microservice.get('teams/overview/national-club', {
          headers: {
            Authorization: accessToken,
          },
          params,
        }),
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getTeamOverviews(
    teamOverviewsQueryDto: TeamOverviewsQueryDto,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`teams/overview`, {
        headers: {
          Authorization: accessToken,
        },
        params: teamOverviewsQueryDto,
      }),
    );
  }

  async getTeamOverviewThaiClub(
    teamOverviewsQueryDto: TeamOverviewsQueryDto,
    accessToken: string,
  ) {
    return await getResponse(
      this.microservice.get(`teams/overview/thai-club`, {
        headers: {
          Authorization: accessToken,
        },
        params: teamOverviewsQueryDto,
      }),
    );
  }
}
