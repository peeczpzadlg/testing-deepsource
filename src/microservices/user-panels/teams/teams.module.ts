import { Module } from '@nestjs/common';
import { TeamOverviewsModule } from './team-overviews/team-overviews.module';
import { TeamDetailModule } from './team-detail/team-detail.module';

@Module({
  imports: [TeamOverviewsModule, TeamDetailModule]
})
export class TeamsModule {}
