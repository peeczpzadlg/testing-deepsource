import { Module } from '@nestjs/common';
import { CompetitionsModule } from './competitions/competitions.module';
import { HomeModule } from './home/home.module';
import { PersonnelModule } from './personnel/personnel.module';
import { StadiumsModule } from './stadiums/stadiums.module';
import { TeamsModule } from './teams/teams.module';
import { GlobalModule } from './global/global.module';

@Module({
  imports: [
    CompetitionsModule,
    PersonnelModule,
    HomeModule,
    TeamsModule,
    StadiumsModule,
    GlobalModule
  ]
})
export class UserPanelsModule {}
