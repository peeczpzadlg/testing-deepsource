import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ExpectError, ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { SaveGlobalAwardDto, SearchAwardDto } from './account-awards.dto';
import { AccountAwardsService } from './account-awards.service';

@ApiTags('award')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('')
export class AccountAwardsController {

  constructor(
    private awardService: AccountAwardsService
  ) {}

  @Get('setting/global/award')
  async findAllAward(@Req() req: Request, @Query() query: SearchAwardDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.awardService.findAllConfigAwards(query);
      return new ResponseSuccess('successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('setting/global/award/:awardId')
  async findAward(@Req() req: Request, @Param('awardId') awardId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.awardService.findConfigAward(awardId);
      return new ResponseSuccess('successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('setting/global/award')
  async saveAward(@Req() req: Request, @Body() body: SaveGlobalAwardDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      if (!body.id) {
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      const checkDuplicate = await this.awardService.findByName(body);
      
      if(checkDuplicate){

        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of Award already exists.',
          {},
        );
      }
      const data = await this.awardService.saveGlobalAward(body);
      return new ResponseSuccess('successfully', data);
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code);
    }
  }

  @Post('setting/global/award/:awardId/delete')
  async deleteAward(@Req() req: Request, @Param('awardId') awardId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.awardService.deleteGlobalAward(awardId);
      return new ResponseSuccess('successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
