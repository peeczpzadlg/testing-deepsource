import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { PrimaryData } from "../../shared/entities/primary-data.entity";
import { PaginateQueryDto } from "../../shared/interfaces/api-interfaces";
import { ISaveGlobalAward, ISearchAward } from "./account-awards.interface";

export class SaveGlobalAwardDto extends PrimaryData implements ISaveGlobalAward {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_award_name_th: string;
  
  @ApiProperty()
  @IsString()
  global_config_award_name_en: string;
}

export class SearchAwardDto extends PaginateQueryDto implements ISearchAward {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active: string;
}
