import { PrimaryData } from "../../shared/entities/primary-data.entity";
import { IPaginateQuery } from "../../shared/interfaces/api-interfaces";

export interface ISaveGlobalAward extends PrimaryData {
  global_config_award_name_th: string;
  global_config_award_name_en: string;
}

export interface ISearchAward extends IPaginateQuery {
  name?: string;
  active?: string;
}