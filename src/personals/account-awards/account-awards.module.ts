import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigAwards } from '../../settings/primary/configs/entities/global-config-awards.entity';
import { AccountAwardsController } from './account-awards.controller';
import { AccountAwardsService } from './account-awards.service';

@Module({
  controllers: [AccountAwardsController],
  providers: [AccountAwardsService],
  imports: [
    TypeOrmModule.forFeature([GlobalConfigAwards])
  ]
})
export class AccountAwardsModule {}
