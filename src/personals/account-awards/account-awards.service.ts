import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalConfigAwards } from '../../settings/primary/configs/entities/global-config-awards.entity';
import { PaginateResponse } from '../../shared/interfaces/api-interfaces';
import { ExpectError } from '../../shared/interfaces/global-config.interface';
import { FindConditions, ILike, Not, Repository } from 'typeorm';
import { ISaveGlobalAward, ISearchAward } from './account-awards.interface';
import { AxiosInstance } from 'axios';
import { checkAuthMicroservice, getResponse } from '../../shared/utils/helper.utils';
import { microserviceAxios } from '../../shared/utils/microservice-axios.utils';

@Injectable()
export class AccountAwardsService {
  private microservice: AxiosInstance;
  private logger: Logger = new Logger(AccountAwardsService.name);

  constructor(
    @InjectRepository(GlobalConfigAwards) private globalConfigAwardRepository: Repository<GlobalConfigAwards>
  ) {
    this.microservice = microserviceAxios()
  }

  async findAllConfigAwards(query: ISearchAward) {
    try {
      let where: FindConditions<GlobalConfigAwards>[] = [ {deleted: false} ];
      if (query.name) {
        where = [
          { global_config_award_name_th: ILike(`%${query.name}%`), deleted: false },
          { global_config_award_name_en: ILike(`%${query.name}%`),deleted: false }
        ]
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true'
        }))
      }

      const result = await this.globalConfigAwardRepository.findAndCount({
        where,
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { updated_at: 'DESC' }
      })
      return new PaginateResponse(query.page || 1, result[1], query.perPage || 10, result[0]);
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findConfigAward(awardId: number) {
    try {
      const result = await this.globalConfigAwardRepository.findOne({
        id: awardId,
        deleted: false
      });
      if (!result) {
        throw new ExpectError(HttpStatus.NOT_FOUND, 'position id not correct', {});
      }
      return result;
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async saveGlobalAward(body: ISaveGlobalAward) {
    try {
      let award: GlobalConfigAwards;
      if (body.id) {
        award = await this.globalConfigAwardRepository.findOne(body.id);
        if (!award) {
          throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
        }
        delete body.id;
        award = {...award, ...body};
      } else {
        award = body;
      }

      return await this.globalConfigAwardRepository.save(award);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }


  async deleteGlobalAward(awardId: number) {
    try {
      const position = await this.globalConfigAwardRepository.findOne(awardId);
      if (!position) {
        return;
      }
      position.active = false;
      position.deleted = true;
      return this.globalConfigAwardRepository.save(position);
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findByName(body) {
    const {
      id,
      global_config_award_name_th,
    } = body;
    let res = false;
    const result = await this.globalConfigAwardRepository.findOne({
      id: Not(id || 0),
      global_config_award_name_th: global_config_award_name_th, deleted: false
    })
    if(result){
      res = true;
    }
    return res;


    

  }
  
}
