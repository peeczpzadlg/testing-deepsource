import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import { ExpectError, ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { CreateDepartmentDto, SearchDepartmentDto } from './account-departments.dto';
import { AccountDepartmentsService } from './account-departments.service';
import { AccountSpecificationsService } from '../account-specifications/account-specifications.service';
import { AccountTeamHistoryService } from 'src/microservices/account-team-history/account-team-history.service';

@ApiTags('Department')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('setting/global/departments')
export class AccountDepartmentsController {

  constructor(
    private departmentService: AccountDepartmentsService,
    private specificationService: AccountSpecificationsService,
    private accountTeamHistroyService: AccountTeamHistoryService
  ) {}

  @Post()
  async createDepartment(@Req() req: Request, @Body() body: CreateDepartmentDto) {
    try { 
      await checkAuthMicroservice(req.headers.authorization);
      const checkDuplicate = await this.departmentService.findByName(body);
      if(checkDuplicate){
        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of global config department already exists.',
          {},
        );
      }
      const data = await this.departmentService.createDepartment(body);
      return new ResponseSuccess('created successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code);
    }
  }

  @Get()
  async findDepartments(@Req() req: Request, @Query() query: SearchDepartmentDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.departmentService.findDepartments(query);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('all')
  async findAllDepartment(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.departmentService.findAllDepartment();
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get(':departmentId')
  async findDepartment(@Req() req: Request, @Param('departmentId') departmentId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.departmentService.findDepartment(departmentId);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post(':departmentId/delete')
  async deleteDepartment(@Req() req: Request, @Param('departmentId') departmentId: number) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const usedDepartment = await this.specificationService.findOneByDepartmentId(departmentId);
      const checkDepartmentUsed = await this.accountTeamHistroyService.checkDepartmentUsed(departmentId,token);
    
      if (checkDepartmentUsed.data.department || usedDepartment) {
        throw new ExpectError(HttpStatus.BAD_REQUEST, 'this department is used');
      }
     
      await this.departmentService.deleteDepartment(departmentId);
      return new ResponseSuccess('successful', undefined);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something went wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR, '');
    }
  }
}
