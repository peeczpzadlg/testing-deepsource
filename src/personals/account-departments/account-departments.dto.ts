import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { PaginateQueryDto } from "../../shared/interfaces/api-interfaces";
import { IGlobalConfigDepartment, ISearchDepartment } from "../../shared/interfaces/global-config.interface";

export class CreateDepartmentDto implements IGlobalConfigDepartment {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id: number

  @ApiProperty()
  @IsString()
  global_config_department_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_department_name_en: string;
}

export class SearchDepartmentDto extends PaginateQueryDto implements ISearchDepartment {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active?: string;
}
