import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountTeamHistoryModule } from 'src/microservices/account-team-history/account-team-history.module';
import { GlobalConfigDepartments } from '../../settings/primary/configs/entities/global-config-departments.entity';
import { AccountSpecificationsModule } from '../account-specifications/account-specifications.module';
import { AccountDepartmentsController } from './account-departments.controller';
import { AccountDepartmentsService } from './account-departments.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([GlobalConfigDepartments]),
    forwardRef(() => AccountSpecificationsModule),
    AccountTeamHistoryModule
  ],
  controllers: [AccountDepartmentsController],
  providers: [AccountDepartmentsService]
})
export class AccountDepartmentsModule {}
