import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalConfigDepartments } from '../../settings/primary/configs/entities/global-config-departments.entity';
import { PaginateResponse } from '../../shared/interfaces/api-interfaces';
import {
  ExpectError,
  IGlobalConfigDepartment,
  ISearchDepartment,
} from '../../shared/interfaces/global-config.interface';
import { FindConditions, ILike, Not, Repository } from 'typeorm';

@Injectable()
export class AccountDepartmentsService {
  private logger: Logger = new Logger(AccountDepartmentsService.name);
  constructor(
    @InjectRepository(GlobalConfigDepartments)
    private departmentRepository: Repository<GlobalConfigDepartments>,
  ) {}

  async createDepartment(body: IGlobalConfigDepartment) {
    try {
      let department = body;
      if (body.id) {
        department = await this.departmentRepository.findOne(body.id);
        delete body.id;
        department = {
          ...department,
          ...body,
        };
      } else {
        // TODO: change to account id
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      return this.departmentRepository.save(department);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findDepartments(query: ISearchDepartment) {
    try {
      let where: FindConditions<GlobalConfigDepartments>[] = [
        { deleted: false },
      ];
      if (query.name) {
        where = [
          {
            global_config_department_name_en: ILike(`%${query.name}%`),
            deleted: false,
          },
          {
            global_config_department_name_th: ILike(`%${query.name}%`),
            deleted: false,
          },
        ];
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true',
        }));
      }

      const result = await this.departmentRepository.findAndCount({
        where,
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { updated_at: 'DESC' },
      });
      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findAllDepartment() {
    try {
      const result = await this.departmentRepository.find({
        deleted: false,
      });
      if (!result) {
        throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
      }
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findDepartment(departmentId: number) {
    try {
      const result = await this.departmentRepository.findOne({
        id: departmentId,
        deleted: false,
      });
      if (!result) {
        throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
      }
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deleteDepartment(departmentId: number) {
    try {
      const department = await this.departmentRepository.findOne(departmentId);
      if (!department) {
        return;
      }
      department.active = false;
      department.deleted = true;
      return await this.departmentRepository.save(department);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findByName(body) {
    const { id,global_config_department_name_th } = body;
    let res = false;
    const result = await this.departmentRepository.findOne({
      id:Not(id || 0),
      global_config_department_name_th: global_config_department_name_th,
      deleted: false,
    });
    if (result) {
      res = true;
    }
    return res;
  }
}
