import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { IPaginateQuery } from "src/shared/interfaces/api-interfaces";
import { IHonor } from "./account-honor.interface";

export class HonorDto implements IHonor {
  @ApiProperty()
  @IsString()
  setting_sub_competition_name_en: string;
  
  @ApiProperty()
  @IsString()
  setting_sub_competition_name_th: string;

  @ApiProperty()
  @IsString()
  setting_team_name_en: string;

  @ApiProperty()
  @IsString()
  setting_team_name_th: string;

  @ApiProperty()
  @IsString()
  honor_name: string;

  @ApiProperty()
  @IsString()
  honor_name_en: string;

  @ApiProperty()
  @IsNumber()
  honor_year: number;
}
