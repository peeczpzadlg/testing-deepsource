export interface IHonor {
  setting_sub_competition_name_th: string;
  setting_sub_competition_name_en: string;
  setting_team_name_th: string;
  setting_team_name_en: string;
  honor_year: number;
  honor_name: string;
  honor_name_en: string;
}
