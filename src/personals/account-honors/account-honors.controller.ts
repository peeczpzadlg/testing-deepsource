import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express'
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PaginateDto } from 'src/microservices/media/media.dto';
import { EHonorType, PaginateQueryDto } from 'src/shared/interfaces/api-interfaces';
import { ResponseFailed, ResponseSuccess } from 'src/shared/interfaces/global-config.interface';
import { checkAuthMicroservice, getHeaderAccessToken } from 'src/shared/utils/helper.utils';
import { AccountSpecificationsService } from '../account-specifications/account-specifications.service';
import { HonorDto } from './account-honor.dto';
import { AccountHonorsService } from './account-honors.service';

@ApiTags('account-honors')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account/:accountId/honor')
export class AccountHonorsController {

  constructor(
    private readonly honorService: AccountHonorsService,
    private readonly specificationService: AccountSpecificationsService
  ) {}

  @Get('nation')
  async getNationHonor(@Param('accountId') accountId: number, @Req() req: Request, @Query() query: PaginateQueryDto) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.findHonor(accountId, EHonorType.NATION, query, token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('personal')
  async getPersonalHonor(@Param('accountId') accountId: number, @Req() req: Request, @Query() query: PaginateQueryDto) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.findHonor(accountId, EHonorType.PERSONAL,query, token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('club')
  async getClubHonor(@Param('accountId') accountId: number, @Req() req: Request, @Query() query: PaginateQueryDto) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.findHonor(accountId, EHonorType.CLUB, query,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('/nation')
  async createNationHonor(@Param('accountId') accountId: number, @Body() body: HonorDto, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.createHonor(accountId, EHonorType.NATION, body,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('/personal')
  async createPersonalHonor(@Param('accountId') accountId: number, @Body() body: HonorDto, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.createHonor(accountId, EHonorType.PERSONAL, body,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('/club')
  async createClubHonor(@Param('accountId') accountId: number, @Body() body: HonorDto, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.createHonor(accountId, EHonorType.CLUB, body,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('nation/:id/delete')
  async deleteNationHonor(@Param('accountId') accountId: number, @Param('id') idx: number, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.deleteHonor(accountId, EHonorType.NATION, idx,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('club/:id/delete')
  async deleteClubHonor(@Param('accountId') accountId: number, @Param('id') idx: number, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.deleteHonor(accountId, EHonorType.CLUB, idx,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('personal/:id/delete')
  async deletePersonalHonor(@Param('accountId') accountId: number, @Param('id') idx: number, @Req() req: Request) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.honorService.deleteHonor(accountId, EHonorType.PERSONAL, idx,token);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed(err.message || 'something wrong', err.error_code || HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
