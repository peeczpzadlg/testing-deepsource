import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../shared/entities/primary-data.entity';
import { IHonor } from './account-honor.interface';

@Entity()
export class AccountHonors extends PrimaryData {
  @Column({ type: 'jsonb', nullable: false })
  account_honor_nations: IHonor[];

  @Column({ type: 'jsonb', nullable: false })
  account_honor_clubs: IHonor[];

  @Column({ type: 'jsonb', nullable: false })
  account_honor_personals: IHonor[];

  @Column({ type: 'int8', nullable: true})
  account_id: number;
}