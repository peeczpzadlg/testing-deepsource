import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountSpecifications } from '../account-specifications/account-speciafications.entity';
import { AccountSpecificationsModule } from '../account-specifications/account-specifications.module';
import { AccountHonorsController } from './account-honors.controller';
import { AccountHonors } from './account-honors.entity';
import { AccountHonorsService } from './account-honors.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AccountHonors,
      AccountSpecifications
    ]),
    forwardRef(() => AccountSpecificationsModule)
  ],
  controllers: [AccountHonorsController],
  providers: [AccountHonorsService]
})
export class AccountHonorsModule {}
