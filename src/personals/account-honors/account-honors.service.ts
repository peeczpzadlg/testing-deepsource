import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosInstance } from 'axios';
import {
  EHonorType,
  IPaginateQuery,
  PaginateResponse,
} from 'src/shared/interfaces/api-interfaces';
import { getResponse } from 'src/shared/utils/helper.utils';
import { microserviceAxios } from 'src/shared/utils/microservice-axios.utils';
import { Repository } from 'typeorm';
import { AccountSpecifications } from '../account-specifications/account-speciafications.entity';
import { IHonor } from './account-honor.interface';
import { AccountHonors } from './account-honors.entity';

@Injectable()
export class AccountHonorsService {
  private logger: Logger = new Logger(AccountHonorsService.name);
  private microservice: AxiosInstance;
  constructor(
    @InjectRepository(AccountHonors)
    private honorRepository: Repository<AccountHonors>,

    @InjectRepository(AccountSpecifications)
    private accountSpecifications: Repository<AccountSpecifications>
  ) {
    this.microservice = microserviceAxios();
  }

  // get
  async findHonor(accountId: number, type: EHonorType ,query: IPaginateQuery,accessToken:string) {
    try {
      const ResponseMS = await getResponse(this.microservice.get(`account/${accountId}`, 
        {
          headers: {
            Authorization: accessToken
        }
      }));
      const { data: { account:{account_specifications} } } = ResponseMS;

      const data = await this.accountSpecifications.createQueryBuilder('specification').leftJoin('account_honors','honors','honors.id = specification.account_honors')
          .where('specification.id = :id', { id: account_specifications }).select(['honors.*'])
          .getRawOne();
          if (!data || !data.id) {
            return new PaginateResponse(
              query.page || 1,
              0,
              query.perPage || 10,
              [],
            );
          }
          let result: IHonor[];
          switch (type) {
            case EHonorType.NATION:
              result = data.account_honor_nations;
              break;
            case EHonorType.PERSONAL:
              result = data.account_honor_personals;
              break;
            case EHonorType.CLUB:
              result = data.account_honor_clubs;
              break;
            default:
              result = [];
          }
          const total = result.length;
          result = result
            .map((ele, idx) => ({ ...ele, id: idx }))
            .slice(
              (query.perPage || 10) * ((query.page || 1) - 1),
              (query.perPage || 10) * (query.page || 1),
            );
          result.sort((first, last) => {
            return last.honor_year-first.honor_year
          })
          
          return new PaginateResponse(
            query.page || 1,
            total,
            query.perPage || 10,
            result,
          );
  
    
      // return data;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // create
  async createHonor(accountId: number, type: EHonorType, body: IHonor,accessToken:string) {
    try {
      let existHonor = await this.honorRepository.findOne({
        account_id: accountId,
      });
      if (!existHonor) {
        existHonor = this.honorRepository.create({
          account_honor_nations: [],
          account_honor_clubs: [],
          account_honor_personals: [],
          account_id: accountId,
        });
      }
      switch (type) {
        case EHonorType.CLUB:
          existHonor.account_honor_clubs.push(body);
          break;
        case EHonorType.NATION:
          existHonor.account_honor_nations.push(body);
          break;
        case EHonorType.PERSONAL:
          existHonor.account_honor_personals.push(body);
          break;
        default:
          break;
      }
      const ResponseMS = await getResponse(this.microservice.get(`account/${accountId}`, 
        {
          headers: {
            Authorization: accessToken
        }
      }));
      const { data: { account:{account_specifications} } } = ResponseMS;
      if(account_specifications){
          const honorSave =  await this.honorRepository.save(existHonor);
          const { id } = honorSave;
          const getoldSpecification = await this.accountSpecifications.findOne({id:account_specifications});
          const account_specification_respose =  await this.accountSpecifications.save({...getoldSpecification,account_honors:honorSave.id})
          if(account_specification_respose){
            return honorSave;
          }
          return false;
      }else{
        return false;
      }
    } catch (err) {
      this.logger.error(err)
      throw err;
    }
  }

  // delete
  async deleteHonor(accountId: number, type: EHonorType, index: number,accessToken:string) {
    try {
      const ResponseMS = await getResponse(this.microservice.get(`account/${accountId}`, 
        {
          headers: {
            Authorization: accessToken
        }
      }));
      const { data: { account:{account_specifications} } } = ResponseMS;
      const specifications = await this.accountSpecifications.createQueryBuilder('specifications').select(['specifications.*']).where(`specifications.id = ${account_specifications}`).getRawOne();
      const result = await this.honorRepository.createQueryBuilder('honors').select(['honors.*']).where(`honors.id = ${specifications.account_honors}`).getRawOne();
      switch (type) {
        case EHonorType.NATION:
          result.account_honor_nations = result.account_honor_nations.filter((ele, idx) => idx !== index);
          break;
        case EHonorType.CLUB:
          result.account_honor_clubs = result.account_honor_clubs.filter((ele, idx) => idx !== index);
          break;
        case EHonorType.PERSONAL:
          result.account_honor_personals = result.account_honor_personals.filter((ele, idx) => idx !== index);
          break;
        default:
          break;
      }
      return await this.honorRepository.save(result);
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }
}

// {
//   "id": 1,
//   "account_id": 64,
//   "account_honor_nations": [
//       {
//           "setting_sub_competition_name_th": "FIFA's World Cup",
//           "setting_sub_competition_name_en": "FIFA's World Cup",
//           "setting_team_name_th": "England",
//           "setting_team_name_en": "England",
//           "honor_year": 2020,
//           "honor_name": "รางวัลชนะเลิศ"
//       }
//   ],
//   "account_honor_clubs": [
//       {
//           "setting_sub_competition_name_th": "Premier-League",
//           "setting_sub_competition_name_en": "Premier-League",
//           "setting_team_name_th": "Liverpool",
//           "setting_team_name_en": "Liverpool",
//           "honor_year": 2019,
//           "honor_name": "รางวัลชนะเลิศ"
//       }
//   ],
//   "account_honor_personals": [
//       {
//           "setting_sub_competition_name_th": "Premier-League",
//           "setting_sub_competition_name_en": "Premier-League",
//           "setting_team_name_th": "England",
//           "setting_team_name_en": "England",
//           "honor_year": 2020,
//           "honor_name": "ผู้เล่นยอดเยี่ยมแห่งปี"
//       }
//   ],
//   "created_by": "KookKai Kamonchanok"
// }
