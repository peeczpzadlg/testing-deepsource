import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { checkAuthMicroservice, getHeaderAccessToken } from '../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { PaginateQueryDto } from '../../shared/interfaces/api-interfaces';
import { ExpectError, ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { CreateInjuryDto, CreateInjuryTypeDto, SearchInjuryTypeDto } from './account-injuries.dto';
import { AccountInjuriesService } from './account-injuries.service';

@ApiTags('Injury')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('')
export class AccountInjuriesController {
  constructor(
    private injuryService: AccountInjuriesService
  ) {}

  @Get('setting/global/injury-types')
  async getInjuryTypes(@Req() req: Request, @Query() query: SearchInjuryTypeDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.getInjuryTypes(query);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('setting/global/injury-types/all')
  async getAllInjuryTypes(@Req() req: Request, @Query() query: SearchInjuryTypeDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.getAllInjuryTypes(query);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('setting/global/injury-types/:injuryTypeId')
  async getInjuryType(@Req() req: Request, @Param('injuryTypeId') injuryTypeId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.getInjuryType(injuryTypeId);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something wrong', HttpStatus.INTERNAL_SERVER_ERROR); 
    }
  }

  @Post('setting/global/injury-types')
  async createInjuryType(@Req() req: Request, @Body() body: CreateInjuryTypeDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const checkDuplicate = await this.injuryService.findTypeByName(body);
      if(checkDuplicate){
        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of global config injury-type already exists.',
          {},
        );
      }
      const data = await this.injuryService.createInjuryType(body);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code);
    }
  }

  @Post('setting/global/injury-types/:injuryTypeId/delete')
  async deleteInjuryType(@Req() req: Request, @Param('injuryTypeId') injuryTypeId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const usedType = await this.injuryService.checkInjuiryUsed(injuryTypeId);
      if (usedType && usedType.length>0) {
        throw new ExpectError(HttpStatus.BAD_REQUEST, 'this injury type is used');
      }
      await this.injuryService.deleteInjuryType(injuryTypeId);
      return new ResponseSuccess('successful', undefined);
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('accounts/:accountId/injuries')
  async findInjury(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Query() query: PaginateQueryDto
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.findInjury(accountId, query);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('accounts/:accountId/injuries/current')
  async getCurrentInjury(@Req() req: Request, @Param('accountId') accountId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.getCurrentInjury(accountId);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('accounts/:accountId/injuries/:injuryId')
  async changeInjuryStatus(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Param('injuryId') injuryId: number
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.injuryService.changeInjuryStatus(injuryId);
      return new ResponseSuccess('change status successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong',HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('accounts/:accountId/injuries')
  async createInjury(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Body() body: CreateInjuryDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.injuryService.createInjury(accountId, body, token);
      return new ResponseSuccess('created successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('accounts/:accountId/injuries/:injuryId/delete')
  async deleteInjury(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Param('injuryId') injuryId: number
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      await this.injuryService.deleteInjury(accountId, injuryId);
      return new ResponseSuccess('successful', undefined);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
