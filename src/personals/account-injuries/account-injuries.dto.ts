import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { type } from "os";
import { EDurationUnit, EInjuryCause, EInjuryLevel } from "../../shared/enum";
import { PaginateQueryDto } from "../../shared/interfaces/api-interfaces";
import { IGlobalConfigInjuries } from "../../shared/interfaces/global-config.interface";
import { ICreateInjuries, ISearchInjuryType } from "./account-injuries.interface";

export class CreateInjuryTypeDto implements IGlobalConfigInjuries {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_injury_type_name_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_injury_type_name_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_injury_type_description_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_injury_type_description_en?: string;
  

  @ApiPropertyOptional({default:true})
  @IsOptional()
  @IsBoolean()
  active:boolean
}

export class UpdateInjuryTypeDto extends CreateInjuryTypeDto {}

export class CreateInjuryDto implements ICreateInjuries {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_name_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_injury_date: Date;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EInjuryLevel)
  account_injury_level: EInjuryLevel;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EInjuryCause)
  account_injury_cause: EInjuryCause;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_injury_status: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  account_injury_present: boolean;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_medical: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_medical_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_doctor_name: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_doctor_name_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_remark: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_remark_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_injury_recovery_time: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EDurationUnit)
  account_injury_recovery_time_unit: EDurationUnit;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_injury_types: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_injury_admitted_begin_at: Date;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_injury_admitted_end_at: Date;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_injury_cost: number;
}


export class SearchInjuryTypeDto extends PaginateQueryDto implements ISearchInjuryType {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active: string;
}
