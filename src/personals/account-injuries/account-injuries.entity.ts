import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { PrimaryData } from '../../shared/entities/primary-data.entity';
import { EDurationUnit, EInjuryCause, EInjuryLevel } from '../../shared/enum';
import { GlobalConfigInjuryTypes } from '../../settings/primary/configs/entities/global-config-injury-types.entity';

@Entity()
export class AccountInjuries extends PrimaryData {
  @Column({ type: 'int8'})
  account_id: number

  @Column({ type: 'varchar', length: 255, nullable: false })
  account_injury_name_th: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  account_injury_name_en: string;

  @Column({ type: 'timestamp', nullable: false, comment: "วันที่ได้รับบาดเจ็บ" })
  account_injury_date: Date;

  @Column({ type: 'varchar', enum: EInjuryLevel, enumName: 'EInjuryLevel', nullable: false, default: EInjuryLevel.MINOR })
  account_injury_level: EInjuryLevel;

  @Column({ type: 'varchar', enum: EInjuryCause, enumName: 'EInjuryCause', nullable: false, default: EInjuryCause.OTHERS })
  account_injury_cause: EInjuryCause;

  @Column({ type: 'int2', nullable: false, comment: "1 = บาดเจ็บครั้งแรก, 2 = เคยบาดเจ็บ" })
  account_injury_status: number;

  @Column({ type: 'boolean', nullable: false, default: false, comment: "อาการบาดเจ็บปัจจุบัน" })
  account_injury_present: boolean;

  @Column({ type: 'varchar', length: 255, nullable: true, comment: "โรงพยาบาลที่เข้ารับการรักษา" })
  account_injury_medical: string;
  
  @Column({ type: 'varchar', length: 255, nullable: true, comment: "โรงพยาบาลที่เข้ารับการรักษา (en)" })
  account_injury_medical_en: string;

  @Column({ type: 'varchar', length: '150', nullable: true })
  account_injury_doctor_name: string;
  
  @Column({ type: 'varchar', length: '150', nullable: true })
  account_injury_doctor_name_en: string;

  @Column({ type: 'text', nullable: true, comment: "บันทึกการรักษาเพิ่มเติม" })
  account_injury_remark: string;
  
  @Column({ type: 'text', nullable: true, comment: "บันทึกการรักษาเพิ่มเติม (en)" })
  account_injury_remark_en: string;

  @Column({ type: 'varchar', length: '100', nullable: true, comment: "จำนวนวันที่เข้าพักรักษา" })
  account_injury_recovery_time: string;

  @Column({ type: 'varchar', enum: EDurationUnit, enumName: 'EDurationUnit', nullable: true, comment: "เข้าพักรักษาจำนวนกี่ วัน, เดือน, สัปดาห์, ปี" })
  account_injury_recovery_time_unit: EDurationUnit;

  @ManyToOne(() => GlobalConfigInjuryTypes, global_config_injury_types => global_config_injury_types.id)
  @JoinColumn({ name: 'global_config_injury_types' })
  global_config_injury_types: GlobalConfigInjuryTypes;

  @Column({ type: 'timestamp', nullable: true })
  account_injury_admitted_begin_at: Date;

  @Column({ type: 'timestamp', nullable: true })
  account_injury_admitted_end_at: Date;

  @Column({ type: 'decimal', nullable: true })
  account_injury_cost: number;
}