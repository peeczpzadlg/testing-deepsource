import { BasicData } from "../../shared/entities/basic-data.entity";
import { EDurationUnit, EInjuryCause, EInjuryLevel } from "../../shared/enum";
import { IPaginateQuery } from "../../shared/interfaces/api-interfaces";

interface IInjury extends BasicData {
  account_id?: number
  account_injury_name_th: string;
  account_injury_name_en: string;
  account_injury_date: Date;
  account_injury_level: EInjuryLevel;
  account_injury_cause: EInjuryCause;
  account_injury_status: number;
  account_injury_present: boolean;
  account_injury_medical: string;
  account_injury_medical_en: string;
  account_injury_doctor_name: string;
  account_injury_doctor_name_en: string;
  account_injury_remark: string;
  account_injury_remark_en: string;
  account_injury_recovery_time: string;
  account_injury_recovery_time_unit: EDurationUnit;
  global_config_injury_types: number;
  account_injury_admitted_begin_at: Date;
  account_injury_admitted_end_at: Date;
  account_injury_cost: number;
}

export type ICreateInjuries = IInjury

export interface ISearchInjuryType extends IPaginateQuery {
  name?: string;
  active?: string;
}