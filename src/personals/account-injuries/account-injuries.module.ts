import { forwardRef, Module } from '@nestjs/common';
import { AccountInjuriesService } from './account-injuries.service';
import { AccountInjuriesController } from './account-injuries.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigInjuryTypes } from '../../settings/primary/configs/entities/global-config-injury-types.entity';
import { AccountModule } from '../../microservices/account/account.module';
import { AccountInjuries } from './account-injuries.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GlobalConfigInjuryTypes,
      AccountInjuries
    ]),
    forwardRef(() => AccountModule)
  ],
  providers: [AccountInjuriesService],
  controllers: [AccountInjuriesController]
})
export class AccountInjuriesModule {}
