import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountService } from '../../microservices/account/account.service';
import { GlobalConfigInjuryTypes } from '../../settings/primary/configs/entities/global-config-injury-types.entity';
import {
  IPaginateQuery,
  PaginateResponse,
} from '../../shared/interfaces/api-interfaces';
import { IGlobalConfigInjuries } from '../../shared/interfaces/global-config.interface';
import { Connection, FindConditions, ILike, Not, Repository } from 'typeorm';
import { AccountInjuries } from './account-injuries.entity';
import {
  ICreateInjuries,
  ISearchInjuryType,
} from './account-injuries.interface';

@Injectable()
export class AccountInjuriesService {
  private logger: Logger = new Logger(AccountInjuriesService.name);

  constructor(
    @InjectRepository(GlobalConfigInjuryTypes)
    private injuryTypeRepository: Repository<GlobalConfigInjuryTypes>,
    @InjectRepository(AccountInjuries)
    private injuryRepository: Repository<AccountInjuries>,
    private connection: Connection,
    private accountService: AccountService,
  ) {}

  async getAllInjuryTypes(query: ISearchInjuryType) {
    try {
      let where: FindConditions<GlobalConfigInjuryTypes>[] = [
        { deleted: false },
      ];
      if (query.name) {
        where = [
          {
            global_config_injury_type_name_th: ILike(`%${query.name}%`),
            deleted: false,
          },
          {
            global_config_injury_type_name_en: ILike(`%${query.name}%`),
            deleted: false,
          },
        ];
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true',
        }));
      }

      return await this.injuryTypeRepository.find({
        where,
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getInjuryTypes(query: ISearchInjuryType) {
    try {
      let where: FindConditions<GlobalConfigInjuryTypes>[] = [
        { deleted: false },
      ];
      if (query.name) {
        where = [
          {
            global_config_injury_type_name_th: ILike(`%${query.name}%`),
            deleted: false,
          },
          {
            global_config_injury_type_name_en: ILike(`%${query.name}%`),
            deleted: false,
          },
        ];
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true',
        }));
      }

      const result = await this.injuryTypeRepository.findAndCount({
        where,
        order: {
          updated_at: 'DESC',
        },
        skip: (query.page - 1) * query.perPage,
        take: query.perPage,
      });

      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getInjuryType(injuryTypeId: number) {
    try {
      return await this.injuryTypeRepository.findOne(injuryTypeId);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deleteInjuryType(injuryTypeId: number) {
    try {
      const injuryType = await this.injuryTypeRepository.findOne({id:injuryTypeId});
      if (!injuryType) {
        return;
      }
      injuryType.deleted = true;
      injuryType.active = false;
      const result = await this.injuryTypeRepository.save(injuryType);
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async createInjuryType(body: IGlobalConfigInjuries) {
    try {
      let injury = body;
      if (body.id) {
        injury = await this.injuryTypeRepository.findOne(body.id);
        delete body.id;
        injury = {
          ...injury,
          ...body,
        };
      } else {
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      return await this.injuryTypeRepository.save(injury);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async updateInjuryType(injuryTypeId: number, body: IGlobalConfigInjuries) {
    try {
      body.updated_by = 'admin';
      return await this.injuryTypeRepository.update(injuryTypeId, body);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async createInjury(
    accountId: number,
    body: ICreateInjuries,
    accessToken: string,
  ) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const account = await this.accountService.getAccount(
        accountId,
        accessToken,
      );
      const injuryType = await this.injuryTypeRepository.findOne(
        body.global_config_injury_types as number,
      );
      if (!account || !injuryType) {
        throw 'Not Found';
      }
      body.account_id = accountId;
      body.created_by = 'admin';
      body.updated_by = 'admin';

      const injury = await queryRunner.manager.create(AccountInjuries, {
        ...body,
        global_config_injury_types: injuryType,
      });
      const result = await queryRunner.manager.save(injury);
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async findAccountByinjuryId(injuryId: number) {
    try {
      
      const injury = await this.injuryTypeRepository.findOne(injuryId);
      if (!injury) return null;
      const result = await this.injuryRepository.find({
        where: {
          global_config_injury_types: injury,
        },
        take: 1,
      });
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async checkInjuiryUsed(injuryId: number) {
    try {

      const result = await this.injuryRepository.find({
        where: {
          global_config_injury_types: injuryId,
          deleted:false
        },
        take: 1,
      });
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findInjury(accountId: number, query?: IPaginateQuery) {
    try {
      const where: FindConditions<AccountInjuries>[] = [
        {
          deleted: false,
          account_id: accountId,
        },
      ];

      const result = await this.injuryRepository.findAndCount({
        where,
        join: {
          alias: 'accountInjury',
          leftJoinAndSelect: {
            global_config_injury_types:
              'accountInjury.global_config_injury_types',
          },
        },
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { account_injury_date: 'DESC' },
      });
      return new PaginateResponse(
        query.page,
        result[1],
        query.perPage,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getCurrentInjury(accountId: number) {
    try {
      const result = await this.injuryRepository.find({
        where: {
          account_id: accountId,
          deleted: false,
          account_injury_present: true,
        },
        join: {
          alias: 'accountInjury',
          leftJoinAndSelect: {
            global_config_injury_types:
              'accountInjury.global_config_injury_types',
          },
        },
      });
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async changeInjuryStatus(injuryId: number) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const injury = await this.injuryRepository.findOne(injuryId);
      injury.account_injury_present = false;
      const result = await queryRunner.manager.save(injury);
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async deleteInjury(accountId: number, injuryId: number) {
    try {
      const injury = await this.injuryRepository.findOne(injuryId);
      if (injury.account_id != accountId) throw 'Not Found';

      injury.active = false;
      injury.deleted = true;
      return await this.injuryRepository.save(injury);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findTypeByName(body) {

    const { id, global_config_injury_type_name_th } = body;
    let res = false;
    const result = await this.injuryTypeRepository.findOne({
      id: Not(id || 0),
      global_config_injury_type_name_th: global_config_injury_type_name_th,
      deleted: false,
    })


    if (result) {
      res = true;
    }
    return res;
  }
}
