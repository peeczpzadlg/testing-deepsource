import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { EDurationUnit } from "src/shared/enum";
import { IPenalties } from "./account-penalties.interface";

export class AccountPenaltyDto implements IPenalties {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;
  
  @ApiProperty()
  @IsNumber()
  account_id: number;
  
  @ApiProperty()
  @IsString()
  account_penalty_title_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_penalty_title_en: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_penalty_description_th?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_penalty_description_en?: string;
  
  @ApiProperty()
  @IsString()
  account_penalty_competition_name: string;

  @ApiProperty()
  @IsString()
  account_penalty_competition_name_en: string;
  
  @ApiProperty()
  @IsString()
  account_penalty_competition_match: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_penalty_competition_match_en: string;
  
  @ApiProperty()
  @IsDate()
  account_penalty_date: Date;
  
  @ApiProperty()
  @IsString()
  account_penalty_duration: string;
  
  @ApiProperty({
    type: 'enum',
    enum: EDurationUnit
  })
  @IsEnum(EDurationUnit)
  account_penalty_duration_unit: EDurationUnit;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  account_penalty_appeal_begin: Date;
  
  @ApiProperty()
  @IsBoolean()
  account_penalty_appeal_conclusion: boolean;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_penalty_appeal_duration?: string;

  @ApiPropertyOptional({
    type: 'enum',
    enum: EDurationUnit
  })
  @IsOptional()
  @IsEnum(EDurationUnit)
  account_penalty_appeal_duration_unit?: EDurationUnit;
  
  @ApiProperty()
  @IsBoolean()
  account_penalty_present: boolean;
}