import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { PaginateQueryDto } from 'src/shared/interfaces/api-interfaces';
import { ResponseFailed, ResponseSuccess } from 'src/shared/interfaces/global-config.interface';
import { checkAuthMicroservice } from 'src/shared/utils/helper.utils';
import { AccountPenaltyDto } from './account penalties.dto';
import { AccountPenaltiesService } from './account-penalties.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@ApiTags('Penalties')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account/:accountId/penalties')
export class AccountPenaltiesController {

  constructor(
    private penaltiesService: AccountPenaltiesService
  ) {}

  // find current
  @Get('current')
  async findCurrentPenalties(@Param('accountId') accountId: number, @Req() req: Request, @Query() query: PaginateQueryDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.penaltiesService.findCurrentPenalties(accountId, query);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // find all
  @Get('')
  async findAllPenalties(@Param('accountId') accountId: number, @Req() req: Request, @Query() query: PaginateQueryDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.penaltiesService.findAllPenalties(accountId, query);
      return new ResponseSuccess('success', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  // save
  @Post('')
  async saveAccountPenalties(@Param('accountId') accountId: number, @Req() req: Request, @Body() body: AccountPenaltyDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.penaltiesService.savePenalties(body);
      return new ResponseSuccess('save success', data);
    } catch (err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete
  @Post(':penaltyId/delete')
  async deletePenalty(@Param('accountId') accountId: number, @Param('penaltyId') penaltyId: number, @Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.penaltiesService.deletePenalty(penaltyId);
      return new ResponseSuccess('delete success', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
