import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../shared/entities/primary-data.entity';
import { EDurationUnit } from '../../shared/enum';
import { IPenalties } from './account-penalties.interface';

@Entity()
export class AccountPenalties extends PrimaryData implements IPenalties {
  @Column({ type: 'int8' })
  account_id: number;

  @Column({ type: 'varchar', length: 255, nullable: false, comment: "หัวข้อการลงโทษ" })
  account_penalty_title_th: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  account_penalty_title_en: string;

  @Column({ type: 'text', nullable: true, comment: "รายละเอียดการลงโทษ" })
  account_penalty_description_th: string;

  @Column({ type: 'text', nullable: true })
  account_penalty_description_en: string;

  @Column( {type: 'varchar', length: 255, comment: 'รายการการแข่งขันที่ได้รับบทลงโทษ'})
  account_penalty_competition_name: string;

  @Column( {type: 'varchar', length: 255, nullable:true, comment: 'รายการการแข่งขันที่ได้รับบทลงโทษ (en)'})
  account_penalty_competition_name_en: string;

  @Column( {type: 'varchar', length: 255, comment: 'รอบการแข่งขันที่ได้รับบทลงโทษ'})
  account_penalty_competition_match: string;
  
  @Column( {type: 'varchar', nullable: true, length: 255, comment: 'รอบการแข่งขันที่ได้รับบทลงโทษ (en)'})
  account_penalty_competition_match_en: string;

  @Column({ type: 'date', comment: 'วันที่โดนบทลงโทษ'})
  account_penalty_date: Date;

  @Column({ type: 'varchar', length: '50', nullable: true, comment: "ระยะเวลาในการโดนลงโทษ" })
  account_penalty_duration: string;

  @Column({ type: 'varchar', enum: EDurationUnit, enumName: 'EDurationUnit', nullable: true, comment: "หน่วยของระยะเวลาในการโดนลงโทษ วัน/ สัปดาห์/ เดือน/ ปี/ แมทช์/ ฤดูกาล" })
  account_penalty_duration_unit: EDurationUnit;

  @Column({ type: 'date', nullable: true, comment: "วันที่ยื่นอุทธรณ์" })
  account_penalty_appeal_begin?: Date;
  
  @Column({ type: 'boolean', default: false, nullable: false, comment: "บทสรุปการยื่นอุทธรณ์ true=อนุมัติ, false=ไม่อนุมัติ" })
  account_penalty_appeal_conclusion: boolean;

  @Column({ type: 'varchar', length: '50', nullable: true, comment: "ระยะเวลาในการโดนลงโทษหลังการอุทธรณ์" })
  account_penalty_appeal_duration?: string;

  @Column({ type: 'varchar', enum: EDurationUnit, enumName: 'EDurationUnit', nullable: true, comment: "หน่วยของระยะเวลาในการโดนลงโทษ วัน/ สัปดาห์/ เดือน/ ปี/ แมทช์/ ฤดูกาล" })
  account_penalty_appeal_duration_unit?: EDurationUnit;

  @Column({ type: 'boolean', default: false, nullable: false})
  account_penalty_present: boolean;
}