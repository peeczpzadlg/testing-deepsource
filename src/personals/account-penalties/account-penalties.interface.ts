import { PrimaryData } from "src/shared/entities/primary-data.entity";
import { EDurationUnit } from "src/shared/enum";

export interface IPenalties extends PrimaryData {
  account_id: number;
  account_penalty_title_th: string;
  account_penalty_title_en: string;
  account_penalty_description_th?: string;
  account_penalty_description_en?: string;
  account_penalty_competition_name: string;
  account_penalty_competition_name_en?: string;
  account_penalty_competition_match: string;
  account_penalty_competition_match_en?: string;
  account_penalty_date: Date;
  account_penalty_duration: string;
  account_penalty_duration_unit: EDurationUnit;
  account_penalty_appeal_begin?: Date;
  account_penalty_appeal_conclusion: boolean;
  account_penalty_appeal_duration?: string;
  account_penalty_appeal_duration_unit?: EDurationUnit;
  account_penalty_present: boolean;
}
