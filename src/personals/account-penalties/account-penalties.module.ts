import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountPenaltiesController } from './account-penalties.controller';
import { AccountPenalties } from './account-penalties.entity';
import { AccountPenaltiesService } from './account-penalties.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AccountPenalties
    ])
  ],
  controllers: [AccountPenaltiesController],
  providers: [AccountPenaltiesService]
})
export class AccountPenaltiesModule {}
