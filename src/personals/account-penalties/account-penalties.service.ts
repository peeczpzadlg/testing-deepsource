import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginateQuery,
  PaginateResponse,
} from 'src/shared/interfaces/api-interfaces';
import { ExpectError } from 'src/shared/interfaces/global-config.interface';
import { Repository } from 'typeorm';
import { AccountPenalties } from './account-penalties.entity';
import { IPenalties } from './account-penalties.interface';

@Injectable()
export class AccountPenaltiesService {
  private logger: Logger = new Logger(AccountPenalties.name);

  constructor(
    @InjectRepository(AccountPenalties)
    private penaltiesRepository: Repository<AccountPenalties>,
  ) {}

  // find current
  async findCurrentPenalties(accountId: number, query?: IPaginateQuery) {
    try {
      const result = await this.penaltiesRepository.findAndCount({
        where: {
          account_penalty_present: true,
          account_id: accountId,
          deleted: false
        },
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { account_penalty_date: 'DESC' },
      });
      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // find all
  async findAllPenalties(accountId: number, query: IPaginateQuery) {
    try {
      const result = await this.penaltiesRepository.findAndCount({
        where: {
          account_id: accountId,
          deleted: false
        },
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { account_penalty_date: 'DESC' },
      });
      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // save
  async savePenalties(body: IPenalties) {
    try {
      let penalty: IPenalties;
      if (body.id) {
        penalty = await this.penaltiesRepository.findOne(body.id);
        delete body.id;
        penalty = { ...penalty, ...body };
      } else {
        penalty = body;
      }

      return await this.penaltiesRepository.save(penalty);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  // delete
  async deletePenalty(penaltyId: number) {
    try {
      return await this.penaltiesRepository.update(
        { id: penaltyId },
        { deleted: true, active: false },
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
