import { Body, Controller, Get, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import { ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { SavePhysicalFitnessDto } from './account-physical-fitness.dto';
import { AccountPhysicalFitnessService } from './account-physical-fitness.service';

@ApiTags('Physical fitness')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('setting/global/physical-fitness')
export class AccountPhysicalFitnessController {
  constructor(
    private physicalFitnessService: AccountPhysicalFitnessService
  ) {}

  @Get()
  async findPhysicalFitness(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.physicalFitnessService.findAllActivePhysicalFitness();
      return new ResponseSuccess('Successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post()
  @ApiBody({ type: [SavePhysicalFitnessDto] })
  async savePhysicalFitness(@Req() req: Request, @Body() body: SavePhysicalFitnessDto[]) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.physicalFitnessService.savePhysicalFitness(body);
      return new ResponseSuccess('created successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
