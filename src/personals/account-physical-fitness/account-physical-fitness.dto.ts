import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { IGlobalPhysicalFitness } from "./account-physical-fitness.interface";

export class SavePhysicalFitnessDto implements IGlobalPhysicalFitness {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_physical_fitness_name_en?: string;
  
  @ApiProperty()
  @IsString()
  global_config_physical_fitness_name_th: string;

  @ApiProperty()
  @IsString()
  global_config_physical_fitness_percentage: string;

  active = true;
}
