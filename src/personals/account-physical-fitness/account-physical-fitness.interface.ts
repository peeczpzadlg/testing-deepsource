import { PrimaryData } from "../../shared/entities/primary-data.entity";

export interface IGlobalPhysicalFitness extends PrimaryData{
  global_config_physical_fitness_percentage: string;
  global_config_physical_fitness_name_th: string;
  global_config_physical_fitness_name_en?: string;
}
