import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigPhysicalFitness } from '../../settings/primary/configs/entities/global-config-physical-fitness.entity';
import { AccountPhysicalFitnessController } from './account-physical-fitness.controller';
import { AccountPhysicalFitnessService } from './account-physical-fitness.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GlobalConfigPhysicalFitness
    ])
  ],
  controllers: [AccountPhysicalFitnessController],
  providers: [AccountPhysicalFitnessService]
})
export class AccountPhysicalFitnessModule {}
