import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalConfigPhysicalFitness } from '../../settings/primary/configs/entities/global-config-physical-fitness.entity';
import { Connection, Repository } from 'typeorm';
import { IGlobalPhysicalFitness } from './account-physical-fitness.interface';

@Injectable()
export class AccountPhysicalFitnessService {
  private logger: Logger = new Logger(AccountPhysicalFitnessService.name);

  constructor(
    private connection: Connection,
    @InjectRepository(GlobalConfigPhysicalFitness) private physicalFitnessRepository: Repository<GlobalConfigPhysicalFitness>
  ) {}

  async savePhysicalFitness(body: IGlobalPhysicalFitness[]) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const result = await Promise.all(body.map(async (ele) => {
        let fitness = ele;
        if (ele.id) {
          fitness = await this.physicalFitnessRepository.findOne({id: ele.id, deleted: false});
          delete ele.id;
          fitness = {
            ...fitness,
            ...ele
          }
        } else {
          fitness.created_by = 'admin';
        }
        fitness.updated_by = 'admin';
        return await queryRunner.manager.getRepository(GlobalConfigPhysicalFitness).save(fitness);
      }))
      await queryRunner.commitTransaction();
      return result;
    } catch(err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async updatePhysicalFitness(physicalFitnessId: number, body: IGlobalPhysicalFitness) {
    try {
      body.updated_by = 'admin';
      return await this.physicalFitnessRepository.update(physicalFitnessId, body);
    } catch(err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findAllActivePhysicalFitness() {
    try {
      const result = await this.physicalFitnessRepository.find({
        where: {active: true, deleted: false},
      });
      return result.sort((a, b) => Number(a.global_config_physical_fitness_percentage) - Number(b.global_config_physical_fitness_percentage))
    } catch(err) {
      this.logger.error(err);
      throw err
    }
  }
}
