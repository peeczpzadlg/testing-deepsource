import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { EMainPosition } from "../../shared/enum";
import { EPersonnelPositionType } from "../../shared/enum/position-type";
import { PaginateQueryDto } from "../../shared/interfaces/api-interfaces";
import { IGetPosition, ISavePlayerPosition, ISaveStaffPosition } from "./account-position.interface";

export class SavePlayerPositionDto implements ISavePlayerPosition {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_position_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_position_name_en?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_position_abbreviation?: string;

  global_config_position_type: EPersonnelPositionType;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_position_main_position: EMainPosition;

  created_by?: string
  updated_by?: string
}

export class SaveStaffPositionDto implements ISaveStaffPosition {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_position_name_th: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_position_name_en?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_position_abbreviation?: string;

  global_config_position_type: EPersonnelPositionType;

  created_by?: string
  updated_by?: string
}

export class GetPosition extends PaginateQueryDto implements IGetPosition {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name: string;

  @ApiPropertyOptional({
    type: Boolean
  })
  @IsOptional()
  @IsString()
  active: string;
}