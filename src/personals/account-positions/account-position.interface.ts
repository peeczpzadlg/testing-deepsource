import { PrimaryData } from "../../shared/entities/primary-data.entity";
import { EMainPosition } from "../../shared/enum";
import { EPersonnelPositionType } from "../../shared/enum/position-type";
import { IPaginateQuery } from "../../shared/interfaces/api-interfaces";

export interface ISavePlayerPosition extends PrimaryData {
  global_config_position_name_th: string;
  global_config_position_name_en?: string;
  global_config_position_abbreviation?: string;
  global_config_position_type: EPersonnelPositionType;
  global_config_position_main_position: EMainPosition;
}

export interface ISaveStaffPosition extends PrimaryData {
  global_config_position_name_th: string;
  global_config_position_name_en?: string;
  global_config_position_abbreviation?: string;
  global_config_position_type: EPersonnelPositionType;
}

export interface IGetPosition extends IPaginateQuery {
  name?: string;
  active?: string;
}
