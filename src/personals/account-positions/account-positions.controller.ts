import { Body, Controller, Get, HttpStatus, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import { EPersonnelPositionType } from '../../shared/enum/position-type';
import { ExpectError, ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { GetPosition, SavePlayerPositionDto, SaveStaffPositionDto } from './account-position.dto';
import { AccountPositionsService } from './account-positions.service';
import { AccountSpecificationsService } from '../account-specifications/account-specifications.service';
import { AccountTeamHistoryService } from 'src/microservices/account-team-history/account-team-history.service';

@ApiTags('Position')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('setting/global/positions')
export class AccountPositionsController {
  
  constructor(
    private positionService: AccountPositionsService,
    private specificationService: AccountSpecificationsService,
    private accountTeamHistroyService: AccountTeamHistoryService
  ) {}

  @Get('player')
  async getPlayerPosition(@Req() req: Request, @Query() query: GetPosition) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findPositionTypes(EPersonnelPositionType.Player, query);
      return new ResponseSuccess('Successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('player/all')
  async findAllPlayerPosition(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findAllPosition(EPersonnelPositionType.Player);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('player/:positionId')
  async getPlayerPositionById(@Req() req: Request, @Param('positionId') positionId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findPositionType(EPersonnelPositionType.Player, positionId);
      
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('player')
  async createPlayerPosition(@Req() req: Request, @Body() body: SavePlayerPositionDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      if (!body.id) {
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      body.global_config_position_type = EPersonnelPositionType.Player
      const checkDuplicate = await this.positionService.findByNameAndType(body);
      if(checkDuplicate){
        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of global config player positon already exists.',
          {},
        );
      }
      const data = await this.positionService.savePlayerPosition(body);
      return new ResponseSuccess('Successfully', data);
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code);
    }
  }

  @Get('staff')
  async getStaffPosition(@Req() req: Request, @Query() query: GetPosition) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findPositionTypes(EPersonnelPositionType.Staff, query);
      return new ResponseSuccess('Successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('staff/all')
  async findAllStaffPosition(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findAllPosition(EPersonnelPositionType.Staff);
      return new ResponseSuccess('Successfully', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('staff/:positionId')
  async getStaffPositionById(@Req() req: Request, @Param('positionId') positionId: number) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.positionService.findPositionType(EPersonnelPositionType.Staff, positionId);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('staff')
  async createStaffPosition(@Req() req: Request, @Body() body: SaveStaffPositionDto) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      if (!body.id) {
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      body.global_config_position_type = EPersonnelPositionType.Staff;
      const checkDuplicate = await this.positionService.findByNameAndType(body);

      if(checkDuplicate){
        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of global config staff positon already exists.',
          {},
        );
      }
      const data = await this.positionService.saveStaffPosition(body);
      return new ResponseSuccess('Successfully', data);
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code);
    }
  }

  @Post(':positionId/delete')
  async deletePosition(@Req() req: Request, @Param('positionId') positionId: number) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const usePosition = await this.specificationService.findOneByPosition(positionId);
      const checkPositionUsed = await this.accountTeamHistroyService.checkPositionUsed(positionId,token);
    
      if (checkPositionUsed.data.position || usePosition) {
        throw new ExpectError(HttpStatus.BAD_REQUEST, 'this position is used');
      }
      await this.positionService.deletePosition(positionId);
      return new ResponseSuccess('Deleted Successfully', {})
    } catch(err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

}
