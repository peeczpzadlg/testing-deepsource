import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountTeamHistoryModule } from 'src/microservices/account-team-history/account-team-history.module';
import { GlobalConfigPositions } from '../../settings/primary/configs/entities/global-config-positions.entity';
import { AccountSpecificationsModule } from '../account-specifications/account-specifications.module';
import { AccountPositionsController } from './account-positions.controller';
import { AccountPositionsService } from './account-positions.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([GlobalConfigPositions]),
    forwardRef(() => AccountSpecificationsModule),
    AccountTeamHistoryModule
  ],
  controllers: [AccountPositionsController],
  providers: [AccountPositionsService],
  exports: [AccountPositionsService]
})
export class AccountPositionsModule {}
