import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalConfigPositions } from '../../settings/primary/configs/entities/global-config-positions.entity';
import { EPersonnelPositionType } from '../../shared/enum/position-type';
import { PaginateResponse } from '../../shared/interfaces/api-interfaces';
import { ExpectError } from '../../shared/interfaces/global-config.interface';
import { FindConditions, ILike, Not, Repository } from 'typeorm';
import {
  IGetPosition,
  ISavePlayerPosition,
  ISaveStaffPosition,
} from './account-position.interface';
@Injectable()
export class AccountPositionsService {
  private logger: Logger = new Logger(AccountPositionsService.name);

  constructor(
    @InjectRepository(GlobalConfigPositions)
    private positionRepository: Repository<GlobalConfigPositions>,
  ) {}

  async savePlayerPosition(body: ISavePlayerPosition) {
    try {
      let position: GlobalConfigPositions;
      if (body.id) {
        position = await this.positionRepository.findOne(body.id);
        if (!position) {
          throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
        } else if (
          position.global_config_position_type !==
          body.global_config_position_type
        ) {
          throw new ExpectError(
            HttpStatus.BAD_REQUEST,
            'use the correct api for type',
            {},
          );
        }
        delete body.id;
        position = { ...position, ...body };
      } else {
        position = body;
      }
      return await this.positionRepository.save(position);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async saveStaffPosition(body: ISaveStaffPosition) {
    try {
      let position: GlobalConfigPositions;
      if (body.id) {
        position = await this.positionRepository.findOne(body.id);
        if (!position) {
          throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
        } else if (
          position.global_config_position_type !==
          body.global_config_position_type
        ) {
          throw new ExpectError(
            HttpStatus.BAD_REQUEST,
            'use the correct api for type',
            {},
          );
        }
        delete body.id;
        position = { ...position, ...body };
      } else {
        position = body;
      }

      return await this.positionRepository.save(position);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findAllPosition(positionType: EPersonnelPositionType) {
    try {
      const result = await this.positionRepository.find({
        where: {
          active: true,
          deleted: false,
          global_config_position_type: positionType,
        },
      });
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findPositionType(
    positionType: EPersonnelPositionType,
    positionId: number,
  ) {
    try {
      const result = await this.positionRepository.findOne({
        global_config_position_type: positionType,
        id: positionId,
        deleted: false,
      });
      if (!result) {
        throw new ExpectError(
          HttpStatus.NOT_FOUND,
          'position id not correct',
          {},
        );
      }
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findPositionTypes(
    positionType: EPersonnelPositionType,
    query: IGetPosition,
  ) {
    try {
      let where: FindConditions<GlobalConfigPositions>[] = [
        {
          global_config_position_type: positionType,
          deleted: false,
        },
      ];
      if (query.name) {
        where = [
          {
            global_config_position_name_en: ILike(`%${query.name}%`),
            global_config_position_type: positionType,
            deleted: false,
          },
          {
            global_config_position_name_th: ILike(`%${query.name}%`),
            global_config_position_type: positionType,
            deleted: false,
          },
        ];
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true',
        }));
      }

      const result = await this.positionRepository.findAndCount({
        where,
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
        order: { updated_at: 'DESC' },
      });
      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deletePosition(positionId: number) {
    try {
      const position = await this.positionRepository.findOne(positionId);
      if (!position) {
        return;
      }
      position.active = false;
      position.deleted = true;
      return this.positionRepository.save(position);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findByNameAndType(body) {
    const {
      id,
      global_config_position_name_th,
      global_config_position_type,
    } = body;

    let res = false;
    const result = await this.positionRepository.findOne({
      id: Not(id || 0),
      global_config_position_name_th: global_config_position_name_th,
      global_config_position_type: global_config_position_type,
      deleted: false,
    });
    if (result) {
      res = true;
    }
    return res;
  }
}
