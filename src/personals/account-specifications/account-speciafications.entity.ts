import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { PrimaryData } from '../../shared/entities/primary-data.entity';
import { GlobalConfigDepartments } from '../../settings/primary/configs/entities/global-config-departments.entity';
import { AccountHonors } from '../account-honors/account-honors.entity';
import { GlobalConfigPhysicalFitness } from '../../settings/primary/configs/entities/global-config-physical-fitness.entity';
import { IAccountSpecification } from './account-specifications.interface';
import { EMainPosition } from '../../shared/enum';
import { GlobalConfigPositions } from 'src/settings/primary/configs/entities/global-config-positions.entity';

@Entity()
export class AccountSpecifications extends PrimaryData
  implements IAccountSpecification {
  @Column({ type: 'jsonb', nullable: true })
  global_config_positions: number[]; // Array Object

  @ManyToOne(
    type => GlobalConfigDepartments,
    globalDepartment => globalDepartment.id,
  )
  @Column({ type: 'int8', nullable: true })
  @JoinColumn({ name: 'global_config_departments' })
  global_config_departments: number;

  @Column({ type: 'jsonb', nullable: true })
  global_config_sport_categories: number[]; // Array Object

  @Column({ type: 'varchar', enum: EMainPosition, nullable: true })
  account_specification_main_position: EMainPosition;

  @ManyToOne(
    type => GlobalConfigPhysicalFitness,
    accountPhysicalFitness => accountPhysicalFitness.id,
  )
  @JoinColumn({ name: 'account_physical_fitness' })
  account_physical_fitness: number;

  @ManyToOne(
    type => AccountHonors,
    accountHonor => accountHonor.id,
  )
  @JoinColumn({ name: 'account_honors' })
  account_honors: number; // Array Object
}
