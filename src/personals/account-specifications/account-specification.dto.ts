import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";
import { UpdateMedicalConditionDto } from "../../microservices/account/dto/medical-condition.dto";
import { EBloodType } from "../../shared/enum";
import { AccountHonors } from "../account-honors/account-honors.entity";
import { TechnicalSkillDto } from "../account-technical-skills/account-technical-skills.dto";
import { ICreateSpecification, ISaveAccountInformationAndMedical } from "./account-specifications.interface";

export class CreateSpecificationDto implements ICreateSpecification {
  @ApiProperty({
    type: [Number]
  })
  @IsArray()
  global_config_positions: number[];

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  global_config_departments?: number;
  
  @ApiPropertyOptional({
    type: [Number]
  })
  @IsOptional()
  @IsArray()
  global_config_sport_categories?: number[];
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_physical_fitness?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_honors?: number;
  
  @ApiPropertyOptional({
    type: [TechnicalSkillDto]
  })
  @IsOptional()
  @IsArray()
  account_technical_skills?: TechnicalSkillDto[];
}

export class SaveAccountInformationAndMedicalDto implements ISaveAccountInformationAndMedical {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_information_height: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  account_information_weight: number;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_biography_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_biography_en?: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_hand_skill: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  account_information_feet_skill: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(EBloodType)
  account_information_blood_type: EBloodType;
  
  @ApiProperty({
    type: [UpdateMedicalConditionDto]
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateMedicalConditionDto)
  medical_conditions: UpdateMedicalConditionDto[];
  
  updated_by: string;
}


