import { Body, Controller, Get, HttpStatus, Param, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { getHeaderAccessToken } from '../../shared/utils/helper.utils';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { EBloodType } from '../../shared/enum';
import { ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { CreateSpecificationDto, SaveAccountInformationAndMedicalDto } from './account-specification.dto';
import { AccountSpecificationsService } from './account-specifications.service';

@ApiTags('Specification')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('account')
export class AccountSpecificationsController {
  constructor(
    private specificationService: AccountSpecificationsService
  ){}

  @Get('blood-type')
  returnBloodType() {
    return new ResponseSuccess('successful', EBloodType);
  }

  @Get(':accountId/specifications')
  async findAccountSpecification(@Req() req: Request, @Param('accountId') accountId: number) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.specificationService.findAcountSpecification(accountId, token);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      if(err.error_code == 404) {
        throw new ResponseFailed('account has not found', HttpStatus.NOT_FOUND);
      }
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post(':accountId/specifications')
  async saveSpecification(
    @Req() req: Request, 
    @Param('accountId') accountId: number, 
    @Body() body: CreateSpecificationDto
  ) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.specificationService.savePlayerSpecification(accountId, body, token);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get(':accountId/informations')
  async getInformationAndMedicalCondition(@Req() req: Request, @Param('accountId') accountId: number) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.specificationService.getInformationAndMedical(accountId, token);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post(':accountId/informations')
  async saveInformationAndMedicalCondition(@Req() req: Request, @Param('accountId') accountId: number, @Body() body: SaveAccountInformationAndMedicalDto) {
    try {
      const token = await getHeaderAccessToken(req.headers);
      const data = await this.specificationService.saveAccountInformationAndMedical(accountId, body, token);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
