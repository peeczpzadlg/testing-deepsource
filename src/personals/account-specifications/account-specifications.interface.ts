import { IUpdateInformations, IUpdateMedicalCondition } from "../../microservices/account/inteface";
import { PrimaryData } from "../../shared/entities/primary-data.entity";
import { AccountHonors } from "../account-honors/account-honors.entity";
import { ITechnicalSkill } from "../account-technical-skills/account-technical-skills.interface";


export interface IAccountSpecification extends PrimaryData {
  global_config_positions: number[];
  global_config_departments?: number;
  global_config_sport_categories?: number[];
  account_physical_fitness?: string | number;
  account_honors?: number;
}


export interface ICreateSpecification extends IAccountSpecification {
  account_technical_skills?: ITechnicalSkill[];
}

export interface ISaveAccountInformationAndMedical extends IUpdateInformations {
  medical_conditions: IUpdateMedicalCondition[];
}

