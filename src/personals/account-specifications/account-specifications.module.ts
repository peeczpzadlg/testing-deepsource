import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigDepartments } from '../../settings/primary/configs/entities/global-config-departments.entity';
import { AccountTechnicalSkills } from '../account-technical-skills/account-technical-skills.entity';
import { GlobalConfigPositions } from '../../settings/primary/configs/entities/global-config-positions.entity';
import { GlobalConfigSportCategories } from '../../settings/primary/configs/entities/global-config-sport-categories.entity';
import { AccountHonors } from '../account-honors/account-honors.entity';
import { AccountInjuries } from '../account-injuries/account-injuries.entity';
import { AccountPenalties } from '../account-penalties/account-penalties.entity';
import { AccountSpecificationsController } from './account-specifications.controller';
import { AccountSpecificationsService } from './account-specifications.service';
import { AccountModule } from '../../microservices/account/account.module';
import { AccountTechnicalSkillsModule } from '../account-technical-skills/account-technical-skills.module';
import { AccountSpecifications } from './account-speciafications.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GlobalConfigPositions,
      GlobalConfigDepartments,
      AccountTechnicalSkills,
      AccountSpecifications,
      GlobalConfigSportCategories,
      AccountHonors,
      AccountPenalties,
      AccountInjuries,
    ]),
    forwardRef(() => AccountModule),
    forwardRef(() => AccountTechnicalSkillsModule)
  ],
  controllers: [AccountSpecificationsController],
  providers: [AccountSpecificationsService],
  exports: [AccountSpecificationsService]
})
export class AccountSpecificationsModule {}
