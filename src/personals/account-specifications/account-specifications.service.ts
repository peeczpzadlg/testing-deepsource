import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountService } from '../../microservices/account/account.service';
import { GlobalConfigPhysicalFitness } from '../../settings/primary/configs/entities/global-config-physical-fitness.entity';
import { ExpectError } from '../../shared/interfaces/global-config.interface';
import { Connection, Repository } from 'typeorm';
import { AccountTechnicalSkillsService } from '../account-technical-skills/account-technical-skills.service';
import { AccountSpecifications } from './account-speciafications.entity';
import {
  ICreateSpecification,
  ISaveAccountInformationAndMedical,
} from './account-specifications.interface';

@Injectable()
export class AccountSpecificationsService {
  private logger: Logger = new Logger(AccountSpecificationsService.name);

  constructor(
    private connect: Connection,
    private technicalSkillService: AccountTechnicalSkillsService,
    @InjectRepository(AccountSpecifications)
    private specificationRepository: Repository<AccountSpecifications>,
    private accountService: AccountService,
  ) { }

  async savePlayerSpecification(
    accountId: number,
    body: ICreateSpecification,
    accessToken: string,
  ) {
    try {
      const result = await this.saveSpecification(accountId, body, accessToken);
      if (body.account_technical_skills) {
        const technicalSkills = body.account_technical_skills.map(ele => ({
          ...ele,
          account_specification_id: result.id,
        }));

        result.account_technical_skills = await this.technicalSkillService.updateTechnicalSkills(
          technicalSkills,
        );
      }
      await this.accountService.saveSpecificationId(
        accountId,
        result.id,
        accessToken,
      );

      return await this.findAcountSpecification(accountId, accessToken);
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  private async saveSpecification(
    accountId: number,
    body: ICreateSpecification,
    accessToken: string,
  ) {
    const queryRunner = this.connect.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      if (body.account_technical_skills.length === 0) { body.account_technical_skills = null }
      let accountSpecification;
      const account = await this.accountService
        .getAccount(accountId, accessToken)
        .then(res => res.data?.account);
      if (!account) {
        throw new ExpectError(
          HttpStatus.BAD_REQUEST,
          'account not active or deleted',
          {},
        );
      }
      const fitness = await queryRunner.manager
        .getRepository(GlobalConfigPhysicalFitness)
        .findOne({
          global_config_physical_fitness_percentage: body.account_physical_fitness as any,
        });
      let fitness_id = null;
      if (fitness) {
        fitness_id = fitness;
      }
      if (account.account_specifications) {
        accountSpecification = await queryRunner.manager
          .getRepository(AccountSpecifications)
          .findOne(account.account_specifications);
        accountSpecification = await queryRunner.manager.create(
          AccountSpecifications,
          {
            ...accountSpecification,
            ...body,
            account_physical_fitness: fitness_id,
          },
        );
      } else {
        accountSpecification = await queryRunner.manager.create(
          AccountSpecifications,
          {
            ...body,
            account_physical_fitness: fitness_id,
          },
        );
      }
      const result = await queryRunner.manager.save(accountSpecification);
      if (!result.id) {
        throw 'Cant Create Specification';
      }
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async findAcountSpecification(accountId: number, accessToken: string) {
    const account = await this.accountService.getAccount(
      accountId,
      accessToken,
    );
    try {
      if (!account.data.account) {
        throw new ExpectError(
          HttpStatus.NOT_FOUND,
          'account id has not found',
          {},
        );
      }
      const specificationId = account.data.account.account_specifications;
      const specification = await this.specificationRepository
        .createQueryBuilder('specification')
        .where('specification.id = :id', { id: specificationId })
        .leftJoinAndSelect(
          'specification.account_physical_fitness',
          'account_physical_fitness',
        )
        .leftJoinAndSelect('specification.account_honors', 'account_honors')
        .getOne();
      const technicalSkills = await this.technicalSkillService.findTechnicalSkillBySpecificationId(
        specificationId,
      );
      return { ...specification, account_technical_skills: technicalSkills };
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async saveAccountInformationAndMedical(
    accountId: number,
    body: ISaveAccountInformationAndMedical,
    accessToken: string,
  ) {
    try {
      // create medical
      const medicalConditionList = body.medical_conditions;
      const saveMedicalResult = await this.accountService
        .updateMedicalCondition(accountId, medicalConditionList, accessToken)
        .then(res => res.data?.medical_conditions);
      // save information
      delete body.medical_conditions;
      const { account } = await this.accountService
        .updateInformation(accountId, body, accessToken)
        .then(res => res.data);
      account.medical_conditions = saveMedicalResult;
      return account;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async getInformationAndMedical(accountId: number, accessToken: string) {
    try {
      const accountInformation = await this.accountService
        .getAccount(accountId, accessToken)
        .then(res => {
          if (res.data.account) {
            return res.data.account.account_informations;
          } else {
            throw new ExpectError(
              HttpStatus.OK,
              'account has not found informations',
              {},
            );
          }
        });
      const medicalConditions = await this.accountService
        .getMedicalCondition(accountId, accessToken)
        .then(res => {
          if (res.data.medical_conditions) {
            return res.data.medical_conditions;
          } else {
            return [];
          }
        });
      accountInformation.medical_conditions = medicalConditions;
      return accountInformation;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findOneByPosition(positionId: number) {
    try {
      const result = await this.specificationRepository
        .createQueryBuilder()
        .where("global_config_positions::jsonb ? :positionId", { positionId })
        .andWhere('deleted = :deleted', { deleted: false })
        .getOne();
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async deleteSpecification(specificationId: number) {
    try {
      const specification = await this.specificationRepository.findOne(specificationId);
      specification.deleted = true;
      specification.active = false;
      await this.specificationRepository.save(specification);
      return specification;
    } catch (err) {
      throw err;
    }
  }

  async findOneBycategory(categoryId: number) {
    try {
      const result = await this.specificationRepository
        .createQueryBuilder()
        .where("global_config_sport_categories::jsonb ? :categoryId", { categoryId })
        .getOne();
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async findOneByDepartmentId(departmentId: number) {
    try {
      const result = await this.specificationRepository.findOne({
        global_config_departments: departmentId, deleted: false
      })
      return result;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
}
