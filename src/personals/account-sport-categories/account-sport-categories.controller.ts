import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../../shared/interfaces/global-config.interface';
import {
  CreateSportCategoryDto,
  SearchCategoryDto,
} from './account-sport-categories.dto';
import { AccountSportCategoriesService } from './account-sport-categories.service';
import { AccountSpecificationsService } from '../account-specifications/account-specifications.service';
import { ApiRequestContext } from 'src/shared/api-request-context';
import { SubCompetitionsService } from 'src/microservices/competitions/sub-competitions/sub-competitions.service';

@ApiTags('Sport category')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('setting/global/sport-categories')
export class AccountSportCategoriesController {
  logContext: ApiRequestContext;

  constructor(
    private sportCategoryService: AccountSportCategoriesService,
    private specificationService: AccountSpecificationsService,
    private subCompetitionsService: SubCompetitionsService
  ) {}

  @Post()
  async createSportCategory(
    @Req() req: Request,
    @Body() body: CreateSportCategoryDto,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const checkDuplicate = await this.sportCategoryService.findByName(body);
      if(checkDuplicate){
        throw new ExpectError(
          HttpStatus.CONFLICT,
          'The name of global config sport category already exists.',
          {},
        );
      }
      const data = await this.sportCategoryService.createSportCategory(body);
      return new ResponseSuccess('successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get('all')
  async findAllSportCategory(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.sportCategoryService.findAllCategory();
      return new ResponseSuccess('successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':id/test')
  async test(@Param('id') id: number, @Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.specificationService.findOneByPosition(id);
      return new ResponseSuccess('successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get()
  async findSportCategories(
    @Req() req: Request,
    @Query() query: SearchCategoryDto,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.sportCategoryService.findSportCategories(query);
      return new ResponseSuccess('successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Get(':categoryId')
  async findSportCategory(
    @Req() req: Request,
    @Param('categoryId') categoryId: number,
  ) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.sportCategoryService.findSportCategory(
        categoryId,
      );

      return new ResponseSuccess('successful', data);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }

  @Post(':categoryId/delete')
  async deleteSportCategory(
    @Req() req: Request,
    @Param('categoryId') categoryId: number,
  ) {
    try {
      const token = req.headers.authorization;
      await checkAuthMicroservice(token);
      const useCategory = await this.specificationService.findOneBycategory(categoryId);
      const checkSportCategoryUsed = await this.subCompetitionsService.checkSportCategoryUsed(categoryId,token);
    
      if (checkSportCategoryUsed.data.sport_category || useCategory) {
        throw new ExpectError(HttpStatus.BAD_REQUEST, 'this category is used');
      }

      await this.sportCategoryService.deleteSportCategory(categoryId);

      return new ResponseSuccess('successful', undefined);
    } catch (err) {
      throw new ResponseFailed(err.message, err.error_code, '');
    }
  }
}
