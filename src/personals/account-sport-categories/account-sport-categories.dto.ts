import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";
import { PaginateQueryDto } from "../../shared/interfaces/api-interfaces";
import { IGlobalconfigSportCategory, ISearchCategory } from "../../shared/interfaces/global-config.interface";

export class CreateSportCategoryDto implements IGlobalconfigSportCategory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiProperty()
  @IsString()
  global_config_sport_category_name_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_sport_category_name_en?: string;
  
  @ApiProperty()
  @IsString()
  global_config_sport_category_description_th: string;
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_sport_category_description_en?: string;
}

export class SearchCategoryDto extends PaginateQueryDto implements ISearchCategory {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional(
    {
      type: Boolean
    }
  )
  @IsOptional()
  @IsString()
  active?: string;
}
