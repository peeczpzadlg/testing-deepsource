import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubCompetitionsModule } from 'src/microservices/competitions/sub-competitions/sub-competitions.module';
import { GlobalConfigSportCategories } from '../../settings/primary/configs/entities/global-config-sport-categories.entity';
import { AccountSpecificationsModule } from '../account-specifications/account-specifications.module';
import { AccountSportCategoriesController } from './account-sport-categories.controller';
import { AccountSportCategoriesService } from './account-sport-categories.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GlobalConfigSportCategories
    ]),
    forwardRef(() => AccountSpecificationsModule),
    SubCompetitionsModule
  ],
  controllers: [AccountSportCategoriesController],
  providers: [AccountSportCategoriesService]
})
export class AccountSportCategoriesModule {}
