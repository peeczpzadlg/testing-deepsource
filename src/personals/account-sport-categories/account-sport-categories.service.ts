import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { GlobalConfigSportCategories } from '../../settings/primary/configs/entities/global-config-sport-categories.entity';
import { PaginateResponse } from '../../shared/interfaces/api-interfaces';
import {
  ExpectError,
  IGlobalconfigSportCategory,
  ISearchCategory,
} from '../../shared/interfaces/global-config.interface';
import { FindConditions, ILike, Not, Repository } from 'typeorm';
import { logData } from '../../shared/utils/logger';

@Injectable()
export class AccountSportCategoriesService {
  constructor(
    @InjectRepository(GlobalConfigSportCategories)
    private sportCategoryRepository: Repository<GlobalConfigSportCategories>,
  ) {}

  async createSportCategory(body: IGlobalconfigSportCategory) {
    try {
      let sportCategory = body;

      if (body.id) {
        sportCategory = await this.sportCategoryRepository.findOne(body.id);
        delete body.id;
        sportCategory = {
          ...sportCategory,
          ...body,
        };
      } else {
        // TODO: change to account id
        body.created_by = 'admin';
      }
      body.updated_by = 'admin';
      return this.sportCategoryRepository.save(sportCategory);
    } catch (err) {
      throw new ExpectError(HttpStatus.BAD_REQUEST, `Bad Request from api`, {});
    }
  }

  async findAllCategory() {
    try {
      const result = await this.sportCategoryRepository.find({
        where: {
          active: true,
          deleted: false,
        },
      });
      return result;
    } catch (err) {
      throw new ExpectError(HttpStatus.BAD_REQUEST, `Bad Request from api`, {});
    }
  }

  async findSportCategories(query: ISearchCategory) {
    try {
      let where: FindConditions<GlobalConfigSportCategories>[] = [
        { deleted: false },
      ];
      if (query.name) {
        where = [
          {
            global_config_sport_category_name_en: ILike(`%${query.name}%`),
            deleted: false,
          },
          {
            global_config_sport_category_name_th: ILike(`%${query.name}%`),
            deleted: false,
          },
        ];
      }

      if (query.active) {
        where = where.map(ele => ({
          ...ele,
          active: query.active.toLowerCase() === 'true',
        }));
      }

      const result = await this.sportCategoryRepository.findAndCount({
        where,
        order: { updated_at: 'DESC' },
        take: query.perPage,
        skip: (query.page - 1) * query.perPage,
      });
      if (!result[0].length) {
        logData(AccountSportCategoriesService.name, {
          path: '/account-sport-categories/findSportCategories',
          message: 'Sport category has not found data',
          status: HttpStatus.NOT_FOUND,
          id: null,
          parameterName: null,
          query: {
            name: 'name',
            active: query.active,
          },
        });
      }

      return new PaginateResponse(
        query.page || 1,
        result[1],
        query.perPage || 10,
        result[0],
      );
    } catch (err) {
      throw new ExpectError(HttpStatus.BAD_REQUEST, `Bad Request from api`, {});
    }
  }

  async findSportCategory(categoryId: number) {
    try {
      const result = await this.sportCategoryRepository.findOne({
        id: categoryId,
        deleted: false,
      });
      if (!result) {
        logData(AccountSportCategoriesService.name, {
          path: 'account-sport-categories/findSportCategory',
          message: 'Sport category has not found',
          status: HttpStatus.NOT_FOUND,
          id: categoryId,
          parameterName: 'sportCategoryId',
          query: null,
        });
      }
      return result;
    } catch (err) {
      throw new ExpectError(
        HttpStatus.BAD_REQUEST,
        `Parameter id must be a number.`,
        {},
      );
    }
  }

  async deleteSportCategory(categoryId: number) {
    try {
      const category = await this.sportCategoryRepository.findOne(categoryId);
      if (!category) {
        logData(AccountSportCategoriesService.name, {
          path: 'account-sport-categories/deleteSportCategory',
          message: 'Sport category has not found',
          status: HttpStatus.NOT_FOUND,
          id: categoryId,
          parameterName: 'sportCategoryId',
          query: null,
        });
        return;
      }
      category.deleted = true;
      category.active = false;
      return await this.sportCategoryRepository.save(category);
    } catch (err) {
      throw new ExpectError(
        HttpStatus.BAD_REQUEST,
        `Parameter id must be a number.`,
        {},
      );
    }
  }

  async findByName(body) {
    let res = false;
    const { id,global_config_sport_category_name_th } = body
    const result = await this.sportCategoryRepository.findOne({
      id:Not(id || 0),
      global_config_sport_category_name_th: global_config_sport_category_name_th,
      deleted: false,
    });
    if (result) {
      res = true;
    }
    return res;
  }
}
