import { Body, Controller, Get, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { checkAuthMicroservice } from '../../shared/utils/helper.utils';
import { ResponseFailed, ResponseSuccess } from '../../shared/interfaces/global-config.interface';
import { GlobalConfigSkillsDto } from './account-technical-skills.dto';
import { AccountTechnicalSkillsService } from './account-technical-skills.service';

@ApiTags('Technical skill')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('')
export class AccountTechnicalSkillsController {

  constructor(
    private technicalSkillService: AccountTechnicalSkillsService
  ) {}

  @Get('setting/global/skill')
  async findGlobalSkills(@Req() req: Request) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.technicalSkillService.getGlobalService();
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('setting/global/skill')
  @ApiBody({type: [GlobalConfigSkillsDto]})
  async setGlobalSkill(@Req() req: Request, @Body() body: GlobalConfigSkillsDto[]) {
    try {
      await checkAuthMicroservice(req.headers.authorization);
      const data = await this.technicalSkillService.saveGlobalSkill(body);
      return new ResponseSuccess('successful', data);
    } catch(err) {
      throw new ResponseFailed('something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
