import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsArray, IsNumber, IsOptional, IsString } from "class-validator";
import { IGlobalTechnicalSkill, ITechnicalSkill } from "./account-technical-skills.interface";

export class TechnicalSkillDto implements ITechnicalSkill {
  account_specification_id: number;
  
  @ApiProperty()
  @IsNumber()
  global_technical_skill_id: number;
  
  @ApiProperty()
  @IsString()
  account_technical_skill_percentage: string;
}

export class UpdateTechnicalSkillDto extends TechnicalSkillDto {
  @ApiProperty()
  @IsNumber()
  id: number;
}

export class GetTechnicalSkillsDto {
  @ApiProperty({
    type: [Number]
  })
  @IsArray()
  ids: number[];
}

export class GlobalConfigSkillsDto implements IGlobalTechnicalSkill {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  id?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  global_config_skill_name_en?: string;
  
  @ApiProperty()
  @IsString()
  global_config_skill_name_th: string;

  created_by?: string;
  updated_by?: string;
}
