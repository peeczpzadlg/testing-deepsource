import { GlobalConfigSkills } from '../../settings/primary/configs/entities/global-config-skills.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { PrimaryData } from '../../shared/entities/primary-data.entity';
import { AccountSpecifications } from '../account-specifications/account-speciafications.entity';
import { ITechnicalSkill } from './account-technical-skills.interface';

@Entity()
export class AccountTechnicalSkills extends PrimaryData implements ITechnicalSkill {
    @ManyToOne(() => AccountSpecifications, specification => specification.id)
    @JoinColumn({ name: 'account_specification_id'})
    account_specification_id: number;

    @ManyToOne(() => GlobalConfigSkills, skill => skill.id)
    @JoinColumn({ name: 'global_technical_skill_id' })
    global_technical_skill_id: number;

    @Column({ type: 'varchar', length: 20, nullable: false, comment: "เปอร์เซนต์ทักษะความสามารถทางกีฬา" })
    account_technical_skill_percentage: string;
  
}