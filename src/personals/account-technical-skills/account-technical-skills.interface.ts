import { PrimaryData } from "../../shared/entities/primary-data.entity";

export interface ITechnicalSkill extends PrimaryData {
  account_specification_id: number;
  global_technical_skill_id: number;
  account_technical_skill_percentage: string;
}

export interface ICreateTechnicalSkill {
  technicalSkills: ITechnicalSkill[];
}

export type IUpdateTechnicalSkill = ICreateTechnicalSkill

export interface IGlobalTechnicalSkill extends PrimaryData {
  global_config_skill_name_th: string;
  global_config_skill_name_en?: string;
}
