import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalConfigSkills } from '../../settings/primary/configs/entities/global-config-skills.entity';
import { AccountTechnicalSkillsController } from './account-technical-skills.controller';
import { AccountTechnicalSkills } from './account-technical-skills.entity';
import { AccountTechnicalSkillsService } from './account-technical-skills.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AccountTechnicalSkills,
      GlobalConfigSkills
    ])
  ],
  controllers: [AccountTechnicalSkillsController],
  providers: [AccountTechnicalSkillsService],
  exports: [AccountTechnicalSkillsService]
})
export class AccountTechnicalSkillsModule {}
