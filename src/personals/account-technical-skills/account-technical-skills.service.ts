import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalConfigSkills } from '../../settings/primary/configs/entities/global-config-skills.entity';
import { ExpectError } from '../../shared/interfaces/global-config.interface';
import { Connection, Repository } from 'typeorm';
import { AccountTechnicalSkills } from './account-technical-skills.entity';
import {
  IGlobalTechnicalSkill,
  ITechnicalSkill,
} from './account-technical-skills.interface';

@Injectable()
export class AccountTechnicalSkillsService {
  private logger: Logger = new Logger(AccountTechnicalSkillsService.name);

  constructor(
    private connection: Connection,
    @InjectRepository(AccountTechnicalSkills)
    private technicalSkillRepository: Repository<AccountTechnicalSkills>,
    @InjectRepository(GlobalConfigSkills)
    private globalSkillRepository: Repository<GlobalConfigSkills>,
  ) {}

  async findTechnicalSkillBySpecificationId(specificationId: number) {
    try {
      return await this.technicalSkillRepository
        .createQueryBuilder('skill')
        .leftJoinAndSelect('skill.global_technical_skill_id', 'global_config_skills')
        .where('skill.account_specification_id = :id', { id: specificationId })
        .andWhere('global_config_skills.active = true AND global_config_skills.deleted = false')
        .orderBy('skill.id', 'ASC')
        .getMany();
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async updateTechnicalSkills(body: ITechnicalSkill[]) {
    const queryRunner = this.connection.createQueryRunner();

    queryRunner.connect();
    queryRunner.startTransaction();
    try {
      const saveTechnicalSkills = body.map(async ele => {
        const existSkill = await queryRunner.manager.createQueryBuilder(AccountTechnicalSkills, 'skill')
          .where('skill.account_specification_id = :specificationId AND skill.global_technical_skill_id = :skillId', {specificationId: ele.account_specification_id, skillId: ele.global_technical_skill_id})
          .getOne();

        if (!existSkill) {
          ele.created_by = 'admin';
        }
        ele.updated_by = 'admin';

        const technicalSkill = queryRunner.manager.create(
          AccountTechnicalSkills,
          {
            ...existSkill, 
            ...ele
          },
        );
        
        return await queryRunner.manager.save(technicalSkill);
      });

      const result = await Promise.all(saveTechnicalSkills);
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async saveGlobalSkill(body: IGlobalTechnicalSkill[]) {
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const result = await Promise.all(body.map(async (ele) => {
        let skill = ele;
        if (ele.id) {
          const existSkill = await this.globalSkillRepository.findOne(ele.id);
          if (!existSkill) {
            throw new ExpectError(HttpStatus.NOT_FOUND, 'not found', {});
          }
          delete ele.id;
          skill = { ...existSkill, ...ele };
        }

        return await queryRunner.manager.getRepository(GlobalConfigSkills).save(skill);
      }))
      await queryRunner.commitTransaction();
      return result;
    } catch (err) {
      this.logger.error(err);
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  async getGlobalService() {
    try {
      return await this.globalSkillRepository.find();
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

}
