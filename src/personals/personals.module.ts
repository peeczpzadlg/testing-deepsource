import { Module } from '@nestjs/common';
import { AccountSpecificationsModule } from './account-specifications/account-specifications.module';
import { AccountPositionsModule } from './account-positions/account-positions.module';
import { AccountDepartmentsModule } from './account-departments/account-departments.module';
import { AccountTechnicalSkillsModule } from './account-technical-skills/account-technical-skills.module';
import { AccountSportCategoriesModule } from './account-sport-categories/account-sport-categories.module';
import { AccountPhysicalFitnessModule } from './account-physical-fitness/account-physical-fitness.module';
import { AccountHonorsModule } from './account-honors/account-honors.module';
import { AccountPenaltiesModule } from './account-penalties/account-penalties.module';
import { AccountInjuriesModule } from './account-injuries/account-injuries.module';
import { AccountAwardsModule } from './account-awards/account-awards.module';

@Module({
  imports: [
    AccountSpecificationsModule,
    AccountPositionsModule,
    AccountDepartmentsModule,
    AccountTechnicalSkillsModule,
    AccountSportCategoriesModule,
    AccountPhysicalFitnessModule,
    AccountHonorsModule,
    AccountPenaltiesModule,
    AccountInjuriesModule,
    AccountAwardsModule,
  ],
  controllers: [],
})
export class PersonalsModule {}
