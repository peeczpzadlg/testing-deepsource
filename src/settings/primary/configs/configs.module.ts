import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigsController } from './configs.controller';
import { ConfigsService } from './configs.service';
import { GlobalConfigAwards } from './entities/global-config-awards.entity';
import { GlobalConfigDepartments } from './entities/global-config-departments.entity';
import { GlobalConfigInjuryTypes } from './entities/global-config-injury-types.entity';
import { GlobalConfigPositions } from './entities/global-config-positions.entity';
import { GlobalConfigSkills } from './entities/global-config-skills.entity';
import { GlobalConfigSportCategories } from './entities/global-config-sport-categories.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GlobalConfigAwards,
      GlobalConfigDepartments,
      GlobalConfigInjuryTypes,
      GlobalConfigPositions,
      GlobalConfigSkills,
      GlobalConfigSportCategories
    ])
  ],
  controllers: [ConfigsController],
  providers: [ConfigsService]
})
export class ConfigsModule {}
