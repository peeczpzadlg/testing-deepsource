import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';

@Entity()
export class GlobalConfigAwards extends PrimaryData {
  @Column({ type: 'varchar', length: 150, nullable: false })
  global_config_award_name_th: string;

  @Column({ type: 'varchar', length: 150, nullable: true })
  global_config_award_name_en: string;
}