import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';
import { IGlobalConfigInjuries } from '../../../../shared/interfaces/global-config.interface';

@Entity()
export class GlobalConfigInjuryTypes extends PrimaryData implements IGlobalConfigInjuries{
  @Column({ type: 'varchar', length: 200, nullable: false })
  global_config_injury_type_name_th: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  global_config_injury_type_name_en: string;

  @Column({ type: 'text', nullable: true })
  global_config_injury_type_description_th: string;

  @Column({ type: 'text', nullable: true })
  global_config_injury_type_description_en: string;  
}