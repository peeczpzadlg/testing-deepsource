import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';

@Entity()
export class GlobalConfigPhysicalFitness extends PrimaryData {
  @Column({ type: 'varchar', length: 10, nullable: false, comment: 'เปอร์เซนต์ความพร้อมทางกาย' })
  global_config_physical_fitness_percentage: string;

  @Column({ type: 'varchar', length: 255, nullable: false, comment: 'ชื่อระดับความพร้อมทางกาย' })
  global_config_physical_fitness_name_th: string;

  @Column({ type: 'varchar', length: 255, nullable: true, comment: 'ชื่อระดับความพร้อมทางกาย' })
  global_config_physical_fitness_name_en: string;
}