import { EMainPosition } from '../../../../shared/enum';
import { EPersonnelPositionType } from '../../../../shared/enum/position-type';
import { Column, Entity, ManyToOne } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';
import { AccountSpecifications } from 'src/personals/account-specifications/account-speciafications.entity';

@Entity()
export class GlobalConfigPositions extends PrimaryData {
  @Column({ type: 'varchar', length: 100, nullable: false })
  global_config_position_name_th: string;

  @Column({ type: 'varchar', length: 100, nullable: true })
  global_config_position_name_en?: string;

  @Column({ type: 'varchar', length: 10, nullable: true })
  global_config_position_abbreviation?: string;

  @Column({ type: 'int2', nullable: false })
  global_config_position_type: EPersonnelPositionType;

  @Column({ type: 'varchar', enum: EMainPosition, nullable: true })
  global_config_position_main_position?: EMainPosition;
}
