import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';

@Entity()
export class GlobalConfigSkills extends PrimaryData {
  @Column({ type: 'varchar', length: 255, nullable: false })
  global_config_skill_name_th: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  global_config_skill_name_en: string;
}