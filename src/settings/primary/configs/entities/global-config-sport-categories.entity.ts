import { IGlobalconfigSportCategory } from '../../../../shared/interfaces/global-config.interface';
import { Column, Entity } from 'typeorm';
import { PrimaryData } from '../../../../shared/entities/primary-data.entity';

@Entity()
export class GlobalConfigSportCategories extends PrimaryData implements IGlobalconfigSportCategory {

  @Column({ type: 'varchar', length: 100, nullable: false })
  global_config_sport_category_name_th: string;

  @Column({ type: 'varchar', length: 100, nullable: true })
  global_config_sport_category_name_en: string;

  @Column({ type: 'text', nullable: false })
  global_config_sport_category_description_th: string;

  @Column({ type: 'text', nullable: false })
  global_config_sport_category_description_en: string;
}