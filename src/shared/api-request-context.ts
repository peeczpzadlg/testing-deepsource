import { RequestContext } from '@medibloc/nestjs-request-context';
import shortUUID from 'short-uuid';
import firebase from 'firebase-admin';

export class ApiRequestContext extends RequestContext {
  id: string;
  timestamp: number;
  method: string;
  path: string;
  eventData: Array<any>;

  constructor() {
    super();
    this.id = shortUUID().new();
    this.timestamp = Date.now();
    this.eventData = [];
  }

  addEventData(eventData: any) {
    this.eventData.push({ ...eventData, timestamp: Date.now() });
  }

  async submit() {
    const validEvents = this.eventData.filter(
      event => event?.status >= 400 && event?.status < 500,
    );

    if (validEvents.length > 0) {
      firebase
        .database()
        .ref(`logs/${this.id}`)
        .set({ ...this });
    }
  }
}
