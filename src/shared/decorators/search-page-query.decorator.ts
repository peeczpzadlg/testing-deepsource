import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { SearchPageDto } from 'src/microservices/microservices.dto';

const DEFAULT_PAGE_SIZE = 15;
const DEFAULT_PAGE = 1;

export const PaginationQuery = createParamDecorator(
  (data: any, context: ExecutionContext): SearchPageDto => {
    const request = context.switchToHttp().getRequest();
    const { query } = request;
    const size = parseInt(query?.size ?? data?.size ?? DEFAULT_PAGE_SIZE, 10);
    const page = parseInt(query?.page ?? data?.page ?? DEFAULT_PAGE, 10);
    return new SearchPageDto({ size, page });
  },
  [
    (target: any, key: string) => {
      ApiQuery({
        name: 'page',
        required: false,
      })(target, key, Object.getOwnPropertyDescriptor(target, key));
      ApiQuery({
        name: 'size',
        required: false,
      })(target, key, Object.getOwnPropertyDescriptor(target, key));
    },
  ],
);
