import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export abstract class BasicData {

  @Column({ nullable: false, default: false })
  deleted?: boolean;

  @Column({ nullable: false, default: true })
  active?: boolean;

  @CreateDateColumn({ type: 'timestamptz' })
  created_at?: Date;

  @Column({ type: 'varchar', length: '50', nullable: true })
  created_by?: string;

  @UpdateDateColumn({ type: 'timestamptz' })
  updated_at?: Date;

  @Column({ type: 'varchar', length: '50', nullable: true })
  updated_by?: string;
}
