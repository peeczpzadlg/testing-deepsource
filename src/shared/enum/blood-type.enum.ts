export enum EBloodType {
  O = 'O',
  A = 'A',
  B = 'B',
  AB = 'AB'
}