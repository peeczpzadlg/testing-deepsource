export enum EDurationUnit {
  DAYS = 'DAYS',
  WEEKS = 'WEEKS',
  MONTHS = 'MONTHS',
  YEARS = 'YEARS',
  MATCHES = 'MATCHES',
  SEASONS = 'SEASONS'
}