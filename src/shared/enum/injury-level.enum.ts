export enum EInjuryLevel {
  MINOR = 'MINOR',
  MODERATE = 'MODERATE',
  SERIOUSLY = 'SERIOUSLY'
}