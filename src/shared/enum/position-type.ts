export enum EPositionType {
  Staff = 2,
  Player = 1
}

export enum EPersonnelPositionType {
  Staff = 2,
  Player = 1
}