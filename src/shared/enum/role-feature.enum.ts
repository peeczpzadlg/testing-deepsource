export enum ERolesFeature {
  BIGDATA = 'BIGDATA',
  ACADEMY = 'ACADEMY',
  ELEARNING = 'ELEARNING'
}