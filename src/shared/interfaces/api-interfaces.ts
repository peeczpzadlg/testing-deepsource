import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export interface IResponseSuccess<T = TResponseSuccess> {
  success: boolean;
  message: string;
  data: T;
}

export type TResponseSuccess = {
  success: boolean;
  message: string;
  data: any;
};

export interface IResponseFailed<T = TResponseFailed> {
  success: boolean;
  message: string;
  error_code: number;
  data?: T;
}

export type TResponseFailed = {
  success: boolean;
  message: string;
  error_code: number;
  data?: any;
};

/* Account */
export interface ICreateAccountHonor {
  account_honor_nations: [
    {
      setting_competition?: string;
      setting_team?: string;
      honor_year?: number;
      honor_name?: string;
    },
  ];
  account_honor_clubs: [
    {
      setting_competition?: string;
      setting_team?: string;
      honor_year?: number;
      honor_name?: string;
    },
  ];
  account_honor_personals: [
    {
      setting_competition?: string;
      setting_team?: string;
      honor_year?: number;
      honor_name?: string;
    },
  ];
  created_by?: string;
}

export interface IPaginateQuery {
  page?: number;
  perPage?: number;
}

export interface IMicroservicePaginateQuery {
  page?: number;
  size?: number;
}

export class PaginateQueryDto implements IPaginateQuery {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  page?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumber()
  perPage?: number;
}

export interface IPaginateResponse<RT> {
  currentPage: number;
  total: number;
  perPage: number;
  lastPage: number;
  data: RT;
}

export class PaginateResponse<RT> implements IPaginateResponse<RT> {
  currentPage: number;
  total: number;
  perPage: number;
  lastPage: number;
  data: RT;

  constructor(currentPage: number, total: number, perPage: number, data: RT) {
    this.currentPage = currentPage;
    this.total = total;
    this.perPage = perPage;
    this.lastPage = Math.ceil(total / perPage);
    this.data = data;
  }
}

export enum EHonorType {
  NATION = 'account_honor_nations',
  CLUB = 'account_honor_clubs',
  PERSONAL = 'account_honor_personals',
}