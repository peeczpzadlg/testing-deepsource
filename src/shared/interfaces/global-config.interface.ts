import { RequestContext } from '@medibloc/nestjs-request-context';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ApiRequestContext } from '../api-request-context';
import { PrimaryData } from '../entities/primary-data.entity';
import {
  IPaginateQuery,
  IResponseFailed,
  IResponseSuccess,
} from './api-interfaces';

export interface IGlobalConfigInjuries extends PrimaryData {
  global_config_injury_type_name_th: string;
  global_config_injury_type_name_en?: string;
  global_config_injury_type_description_th: string;
  global_config_injury_type_description_en?: string;
}

export interface IGlobalConfigDepartment extends PrimaryData {
  global_config_department_name_th: string;
  global_config_department_name_en?: string;
}

export interface ISearchDepartment extends IPaginateQuery {
  name?: string;
  active?: string;
}

export interface IGlobalconfigSportCategory extends PrimaryData {
  global_config_sport_category_name_th: string;
  global_config_sport_category_name_en?: string;
  global_config_sport_category_description_th: string;
  global_config_sport_category_description_en?: string;
}

export interface ISearchCategory extends IPaginateQuery {
  name?: string;
  active?: string;
}

@Injectable()
export class ResponseSuccess<T = any> implements IResponseSuccess<T> {
  message: string;
  success: boolean;
  data: T;

  constructor(message: string, data: T) {
    this.success = true;
    this.message = message;
    this.data = data;

    this.submitLog();
  }

  submitLog() {
    const logCtx: ApiRequestContext = RequestContext.get();
    logCtx.submit();
  }
}

export class ResponseFailed<T = any> extends HttpException {
  constructor(message: string, status: HttpStatus, data?: T) {
    const response: IResponseFailed<T> = {
      message,
      data,
      error_code: status,
      success: false,
    };
    super(response, status);
  }
}

export class ExpectError {
  error_code: HttpStatus;
  message: string;
  data: any;

  constructor(httpStatus: HttpStatus, message: string, data?: any) {
    this.error_code = httpStatus;
    this.data = data;
    this.message = message;
  }
}
