import { HttpStatus } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { IncomingHttpHeaders } from 'http';
import {
  ExpectError,
  ResponseFailed,
  ResponseSuccess,
} from '../interfaces/global-config.interface';
import { microserviceAxios } from './microservice-axios.utils';

export const randomStr = (length: number): string => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const checkNumberOrDigit = (value: any) => {
  const regex = new RegExp(
    /^(([0-9]*([.][0-9]{1,2}?)?))$|^(0[.](0[1-9]|[1-9][0-9]|[1-9])?)$/,
  );
  if (regex.test(value)) {
    return value;
  }
};

export const checkString = (value: any) => {
  const regex = new RegExp(
    /^(((?=.*[a-z])(?=.*[A-Z]))|([^u4E00-u9FA5])|((?=.*[a-z])(?=.*[0-9]))|((?=.*[a-z]))|(?=.*[a-z])|(?=.*[A-Z])|((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{0,})/,
  );
  if (regex.test(value)) {
    return value;
  }
};

export const convertFileSize = (bytes: number) => {
  if (bytes == 0) {
    return '0.00 B';
  }
  const e = Math.floor(Math.log(bytes) / Math.log(1024));
  return (
    (bytes / Math.pow(1024, e)).toFixed(2) + ' ' + ' KMGTP'.charAt(e) + 'B'
  );
};

export async function getMicroserviceResponse(callback: Promise<any>) {
  try {
    const result = await callback;
    if (!result.success) {
      throw result;
    }
    return new ResponseSuccess('successful', result.data);
  } catch (err) {
    if (err.error_code || err.statusCode) {
      throw new ResponseFailed(err.message, err.error_code || err.statusCode);
    } else {
      throw new ResponseFailed(
        'something wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
        err,
      );
    }
  }
}

export async function getResponse<RT = any>(
  callback: Promise<AxiosResponse<any>>,
  checkCallback?: Promise<AxiosResponse<any>>,
  expectError?: ExpectError,
): Promise<RT> {
  try {
    if (checkCallback) {
      await checkCallback.catch(() => {
        throw expectError;
      });
    }
    const response: RT = await callback
      .then(res => res.data)
      .catch(err => {
        const errorResponse = err.response ? {
          url: err.response.config.url,
          data: err.response.data,
        } : err;
        throw errorResponse;
      });
    return response;
  } catch (err) {
    throw err.data;
  }
}

export async function getHeaderAccessToken(
  headerCallback: IncomingHttpHeaders,
  expectError?: ExpectError,
): Promise<string>{
  if(!headerCallback.authorization) {
    throw expectError;
  }

  return headerCallback.authorization;
}

export async function checkAuthMicroservice(accessToken: string): Promise<boolean>{
  const microservice = microserviceAxios();
  return await this.getResponse(microservice.get('authentication/me', {
    headers: {
      Authorization: accessToken
    }
  })
  .then(response => response.data.statusCode)
  .catch((err) => { throw err.data }));
}

export const convertAge = (datetime: string) => {
  if (!datetime) {
    return 0
  }
  const today = new Date();
  const birthDate = new Date(datetime);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
  {
     age--;
  }
  return age;
};
