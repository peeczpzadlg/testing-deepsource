import { randomStr } from "./helper.utils";

export const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|pdf)$/)) {
      return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};

export const editFileName = (req, file, callback) => {
    const name = randomStr(12).substring(2,10) + '-' + Date.now() + '.jpg'
    callback(null, name);
};