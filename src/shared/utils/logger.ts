import { RequestContext } from '@medibloc/nestjs-request-context';
import { Logger } from '@nestjs/common';
import { ApiRequestContext } from '../api-request-context';

type TOptionLogger = {
  message?: string;
  status?: number;
  parameterName?: string;
  id?: number;
  query?: TQueryString;
  path?: string;
};

type TQueryString = {
  name?: string;
  active?: string;
};

export const logData = (className: string, options: TOptionLogger) => {
  const logger: Logger = new Logger(className);

  (options.message || options.status || options.id) &&
    logger.warn(
      `${options.parameterName}: ${JSON.stringify(
        options.id,
      )} / statusCode: ${JSON.stringify(
        options.status,
      )} / message: ${JSON.stringify(
        options.message,
      )} / query: ${JSON.stringify(options.query)} }`,
    );
  const ctx: ApiRequestContext = RequestContext.get();
  ctx.addEventData(options);
};
