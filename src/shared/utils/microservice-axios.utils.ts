import axios, { AxiosInstance } from "axios";

export function microserviceAxios(prefix = ''): AxiosInstance {
  const url = process.env.MICROSERVICE_URL || 'http://localhost:9001/api/v1';
  const token = process.env.RSA_CODE;
  return axios.create({
    baseURL: `${url}${prefix}`,
    headers: {
      'Content-Type': 'application/json',
      'rsa-ssl': token
    }
  })
}
